package com.example.barbershop.modules.ClientsForChoice.Interactor;

import com.example.barbershop.Logic.ClientService.ClientServiceInterface;
import com.example.barbershop.Logic.ClientService.Objects.Callback.ClientsCallback;
import com.example.barbershop.Logic.ClientService.Objects.Client;
import com.example.barbershop.modules.Client.Interactor.ClientViewItem;

import java.util.ArrayList;


public class ClientsForChoiceInteractor implements ClientsForChoiceInteractorInput {

    private ClientsForChoiceInteractorOutput output;

    private String idToken;

    private ClientServiceInterface clientService;

    public ClientsForChoiceInteractor(String idToken, ClientServiceInterface clientService) {
        this.idToken = idToken;
        this.clientService = clientService;
    }

    public void setOutput(ClientsForChoiceInteractorOutput output) {
        this.output = output;
    }

    @Override
    public void loadClients(String query) {
        clientService.getClientsByName(query, idToken, new ClientsCallback() {
            @Override
            public void done(Error error, ArrayList<Client> clients) {
                if (error == null && clients.size() != 0) {
                    ArrayList<ClientViewItem> viewItems = new ArrayList<>();

                    for (Client client : clients) {
                        ClientViewItem item = new ClientViewItem();

                        item.id = client.getId();
                        item.name = client.getName();
                        item.patronymic = client.getPatronymic();
                        item.surname = client.getSurname();
                        item.src = client.getPhotoSrc();

                        viewItems.add(item);
                    }

                    output.interactorDidLoadViewItems(viewItems);

                } else {
                    output.interactorDidErrorLoadClients();
                }
            }
        });
    }
}
