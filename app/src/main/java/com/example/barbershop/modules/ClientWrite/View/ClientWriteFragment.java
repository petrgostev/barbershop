package com.example.barbershop.modules.ClientWrite.View;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.support.v7.widget.RecyclerView;

import com.example.barbershop.R;
import com.example.barbershop.modules.ClientWrite.Assembly.ClientWriteAssembly;
import com.example.barbershop.modules.ClientWrite.Interactor.WriteTimeItem;
import com.example.barbershop.modules.Master.Interactor.MasterViewItem;
import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberViewItem;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;


public class ClientWriteFragment extends Fragment implements ClientWriteViewInput {

    private ClientWriteViewOutput viewOutput;

    private Toolbar toolbar;
    MenuItem itemEdit;
    MenuItem itemSave;
    MenuItem itemDelete;

    private TextView dateET;
    private TextView priceET;
    private TextView timePlaceholder;
    private RecyclerView timeList;

    private LinearLayout serviceContainer;
    private LinearLayout dateContainer;

    private SearchableSpinner mastersSpinner;
    private SearchableSpinner servicesSpinner;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ClientWriteAssembly.configureModule(this);
        setHasOptionsMenu(true);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client_write, container, false);

        toolbar = Objects.requireNonNull(getActivity()).findViewById(R.id.toolbar);

        TextView title = Objects.requireNonNull(getActivity()).findViewById(R.id.titleView);

        toolbar.setVisibility(View.VISIBLE);

        dateET = view.findViewById(R.id.date);

        priceET = view.findViewById(R.id.price);
        timePlaceholder = view.findViewById(R.id.timePlaceholder);
        mastersSpinner = view.findViewById(R.id.masters_spinner);
        servicesSpinner = view.findViewById(R.id.services_spinner);
        timeList = view.findViewById(R.id.time_list);

        serviceContainer = view.findViewById(R.id.service_container);
        dateContainer = view.findViewById(R.id.date_container);

        dateET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewOutput.viewDidTapDateEdit();
            }
        });


        title.setText("Новая запись");

        return view;
    }

    public void setViewOutput(ClientWriteViewOutput viewOutput) {
        this.viewOutput = viewOutput;
    }

    @Override
    public void onStart() {
        super.onStart();
        viewOutput.viewDidStarted();
    }

    @Override
    public void fillMasters(final ArrayList<MasterViewItem> masters) {

        ArrayList<String> arrayList = new ArrayList<>();

        arrayList.add("Не выбран");

        for (MasterViewItem master : masters) {
            arrayList.add(master.fullName);
        }

        ArrayAdapter adapter = new ArrayAdapter(getContext(), R.layout.spinner_item, arrayList);
        mastersSpinner.setAdapter(adapter);

        mastersSpinner.setTitle("Выберите мастера");
        mastersSpinner.setPositiveButton(null);

        mastersSpinner.setSelection(0);

        mastersSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    viewOutput.deselectMaster();
                    serviceContainer.setVisibility(View.GONE);
                    dateContainer.setVisibility(View.GONE);
                    timePlaceholder.setVisibility(View.GONE);
                    priceET.setVisibility(View.GONE);
                } else {
                    viewOutput.selectMaster(masters.get(position - 1));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
                mastersSpinner.setSelection(0);
            }
        });

        serviceContainer.setVisibility(View.GONE);
        dateContainer.setVisibility(View.GONE);
        timePlaceholder.setVisibility(View.GONE);
        priceET.setVisibility(View.GONE);
        timeList.setVisibility(View.GONE);
    }

    @Override
    public void fillServices(final ArrayList<ServiceBarberViewItem> services) {

        ArrayList<String> arrayList = new ArrayList<>();

        arrayList.add("Не выбрана");

        for (ServiceBarberViewItem service : services) {
            arrayList.add(service.name);
        }

        ArrayAdapter adapter = new ArrayAdapter(getContext(), R.layout.spinner_item, arrayList);
        servicesSpinner.setAdapter(adapter);

        servicesSpinner.setTitle("Выберите услугу");
        servicesSpinner.setPositiveButton(null);

        servicesSpinner.setSelection(0);

        servicesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    dateContainer.setVisibility(View.GONE);
                    timePlaceholder.setVisibility(View.GONE);
                    priceET.setVisibility(View.GONE);
                    timeList.setVisibility(View.GONE);
                } else {
                    viewOutput.selectService(services.get(position - 1));
                    priceET.setVisibility(View.VISIBLE);
                    priceET.setText(services.get(position - 1).price);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
                servicesSpinner.setSelection(0);
            }
        });

        serviceContainer.setVisibility(View.VISIBLE);
        dateContainer.setVisibility(View.GONE);
        timePlaceholder.setVisibility(View.GONE);
        priceET.setVisibility(View.GONE);
        timeList.setVisibility(View.GONE);
    }


    @Override
    public void setDate(String dateString) {
        dateContainer.setVisibility(View.VISIBLE);
        dateET.setText(dateString);
    }

    @Override
    public void fillDate(Date date, Date currentDate, Date maximumDate) {
        dateContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void fillTimes(final ArrayList<WriteTimeItem> times) {
        if (times == null) {
            timeList.setVisibility(View.GONE);
            return;
        }

        if (times.isEmpty()) {
            //        timePlaceholder.setVisibility(View.VISIBLE);
            //show error
            timeList.setVisibility(View.GONE);

            return;
        }

        TimeListAdapter adapter = new TimeListAdapter(getContext(), times);

        timeList.setAdapter(adapter);
        timeList.setVisibility(View.VISIBLE);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        timeList.setLayoutManager(layoutManager);

        adapter.setSelectListener(new TimeListAdapter.Listener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(int position) {
                WriteTimeItem item = times.get(position);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        inflater.inflate(R.menu.menu_main, menu);
        itemDelete = menu.findItem(R.id.delete);
        itemSave = menu.findItem(R.id.save);
        itemEdit = menu.findItem(R.id.edit);

        MenuItem itemAdd = menu.findItem(R.id.add);
        itemDelete.setVisible(false);
        itemEdit.setVisible(false);
        itemAdd.setVisible(false);
        itemSave.setVisible(true);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.save) {
            itemSave.setVisible(false);
            viewOutput.viewDidSaveButton();
        }

        return super.onOptionsItemSelected(item);
    }
}
