package com.example.barbershop.modules.TimeIntervalsList.View;

import com.example.barbershop.modules.TimeIntervalsList.Interactor.TimeIntervalViewItem;


public interface TimeIntervalsListViewOutput {

    void viewDidStarted();

    void viewDidSelectItem(TimeIntervalViewItem viewItem);
}
