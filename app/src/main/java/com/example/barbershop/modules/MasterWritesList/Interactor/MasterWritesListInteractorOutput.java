package com.example.barbershop.modules.MasterWritesList.Interactor;

import com.example.barbershop.modules.MasterWrite.Interactor.MasterWriteViewItem;

import java.util.ArrayList;

public interface MasterWritesListInteractorOutput {
    void interactorDidLoadViewItems(ArrayList<MasterWriteViewItem> viewItems);
}
