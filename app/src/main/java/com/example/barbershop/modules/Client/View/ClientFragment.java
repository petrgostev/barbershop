package com.example.barbershop.modules.Client.View;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.bottomappbar.BottomAppBar;
import android.support.design.button.MaterialButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.barbershop.R;
import com.example.barbershop.modules.Client.Assembly.ClientAssembly;
import com.example.barbershop.modules.Client.Interactor.ClientViewItem;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;


public class ClientFragment extends Fragment implements ClientViewInterface {
    private static String FOR_EDIT_KEY = "FOR_EDIT_KEY";
    private Bundle bundle;

    ClientViewOutput viewOutput;

    private EditText surnameET;
    private EditText nameET;
    private EditText phoneET;
    private EditText emailET;
    private EditText passwordET;

    private MaterialButton registrationButton;
    private MaterialButton exitButton;
    private MaterialButton addPhotoButton;

    private CircleImageView photoIV;

    private ProgressBar progressBar;

    private BottomAppBar bottomAppBar;
    private Toolbar toolbar;
    private TextView title;

    private ImageButton myProfileButton;
    private TextView myProfileTv;

    MenuItem itemAdd;
    MenuItem itemEdit;
    MenuItem itemSave;
    MenuItem itemDelete;

    private boolean isEdit = false;
    private boolean isForCreate = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ClientAssembly.configureModule(this);
        setHasOptionsMenu(true);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client, container, false);

        toolbar = Objects.requireNonNull(getActivity()).findViewById(R.id.toolbar);

        title = Objects.requireNonNull(getActivity()).findViewById(R.id.titleView);

        surnameET = view.findViewById(R.id.surname);
        nameET = view.findViewById(R.id.name);
        phoneET = view.findViewById(R.id.phone);
        emailET = view.findViewById(R.id.email);
        passwordET = view.findViewById(R.id.password);

        myProfileButton = Objects.requireNonNull(getActivity()).findViewById(R.id.my_profile_button);
        myProfileTv = Objects.requireNonNull(getActivity()).findViewById(R.id.my_profile_tv);

        photoIV = view.findViewById(R.id.photo);
        progressBar = view.findViewById(R.id.progress_bar);

        progressBar.setVisibility(View.GONE);

        registrationButton = view.findViewById(R.id.registration_button);
        exitButton = view.findViewById(R.id.exit_button);
        addPhotoButton = view.findViewById(R.id.add_photo_button);

        bottomAppBar = Objects.requireNonNull(getActivity()).findViewById(R.id.bottom_app_bar);

        registrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewOutput.viewDidTapRegistrationButton(surnameET.getText().toString(),
                        nameET.getText().toString(),
                        phoneET.getText().toString(),
                        emailET.getText().toString(),
                        passwordET.getText().toString());
            }
        });

        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewOutput.viewDidTapExitButton();
            }
        });

        addPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isForCreate)
                viewOutput.viewDidTapAddPhotoButtonButton();
            }
        });

        photoIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEdit) {
                    viewOutput.viewDidTapReplacePhotoButton();
                }
            }
        });


        title.setText("Регистрация");

        return view;
    }

    public void setViewOutput(ClientViewOutput viewOutput) {
        this.viewOutput = viewOutput;
    }

    @Override
    public void onStart() {
        super.onStart();
        viewOutput.viewDidStarted();
        enableIcon(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        enableIcon(false);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        bundle = outState;
        bundle.putBoolean(FOR_EDIT_KEY, isEdit);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (bundle != null) {
            isEdit = bundle.getBoolean(FOR_EDIT_KEY);
            setEditMode(isEdit, isForCreate);
        }
    }

    @Override
    public void showClientViewItem(ClientViewItem viewItem) {

        title.setText("Профиль");

        if (!viewItem.src.equals("")) progressBar.setVisibility(View.VISIBLE);

        toolbar.setVisibility(View.VISIBLE);
        registrationButton.setVisibility(View.GONE);

        if (!viewItem.src.equals("")) {
            photoIV.setVisibility(View.VISIBLE);
            addPhotoButton.setVisibility(View.GONE);

            Picasso.with(photoIV.getContext())
                    .load(viewItem.src)
                    .into(photoIV);
        }

        surnameET.setText(viewItem.surname);
        nameET.setText(viewItem.name);
        phoneET.setText(viewItem.phone);
        emailET.setText(viewItem.email);
        passwordET.setText(viewItem.password);
    }

    @Override
    public void setEditMode(boolean edit, boolean isForCreate) {
        isEdit = edit;
        this.isForCreate = isForCreate;

        surnameET.setEnabled(isEdit);
        nameET.setEnabled(isEdit);
        phoneET.setEnabled(isEdit);
        emailET.setEnabled(isEdit);
        passwordET.setEnabled(isEdit);
        addPhotoButton.setEnabled(isEdit);

        addPhotoButton.setText(isEdit ? "Добавить фото" : "Нет фото");

        exitButton.setVisibility(isForCreate ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        inflater.inflate(R.menu.menu_main, menu);
        itemDelete = menu.findItem(R.id.delete);
        itemSave = menu.findItem(R.id.save);
        itemEdit = menu.findItem(R.id.edit);
        itemAdd = menu.findItem(R.id.add);

        itemSave.setVisible(false);
        itemDelete.setVisible(false);
        itemAdd.setVisible(false);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.edit) {
            itemSave.setVisible(true);
            itemDelete.setVisible(true);
            itemEdit.setVisible(false);

            setEditMode(true, isForCreate);
        }

        if (id == R.id.save) {
            itemSave.setVisible(false);
            itemDelete.setVisible(false);
            itemEdit.setVisible(true);

            setEditMode(false, isForCreate);

            viewOutput.viewDidTapSaveButton(surnameET.getText().toString(),
                    nameET.getText().toString(),
                    phoneET.getText().toString(),
                    emailET.getText().toString(),
                    passwordET.getText().toString());
        }

        if (id == R.id.delete) {
            itemSave.setVisible(false);
            itemDelete.setVisible(false);
            itemEdit.setVisible(true);

            setEditMode(false, isForCreate);

            viewOutput.viewDidTapDeleteButton();
        }

        return super.onOptionsItemSelected(item);
    }

    //Private

    private void enableIcon(boolean enable) {
        if (enable) {
            myProfileButton.setColorFilter(0xffff0000);
            myProfileTv.setTextColor(0xffff0000);

        } else {
            myProfileButton.setColorFilter(0xffffffff);
            myProfileTv.setTextColor(0xffffffff);
        }
    }
}
