package com.example.barbershop.modules.Loader.Interactor;


public interface LoaderInteractorOutput {
    void interactorNeedsPresentLogin();

    void interactorDidAuthenticationMaster(String id, String idToken, boolean isAdmin, boolean isMaster);

    void interactorDidAuthenticationClient(String id, String idToken);
}
