package com.example.barbershop.modules.ClientsForChoice.View;

import com.example.barbershop.modules.Client.Interactor.ClientViewItem;

public interface ClientsForChoiceViewOutput {
    void viewDidTapSearch(String query);

    void viewDidSelectItem(ClientViewItem viewItem);
}
