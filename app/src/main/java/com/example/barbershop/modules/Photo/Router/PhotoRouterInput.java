package com.example.barbershop.modules.Photo.Router;

import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;


public interface PhotoRouterInput {

    void showEditAlert(String title, EditCallBack editCallBack);

    void closeModule();
}
