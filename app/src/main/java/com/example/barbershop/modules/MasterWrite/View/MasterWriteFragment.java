package com.example.barbershop.modules.MasterWrite.View;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;
import com.example.barbershop.R;
import com.example.barbershop.modules.MasterWrite.Assembly.MasterWriteAssembly;
import com.example.barbershop.modules.MasterWrite.Interactor.MasterWriteViewItem;

import java.util.Objects;


public class MasterWriteFragment extends Fragment implements MasterWriteViewInterface {

    public EditCallBack callBack;
    private MasterWriteViewOutput viewOutput;

    private MenuItem itemSave;

    private EditText dateET;
    private EditText timeET;
    private EditText clientET;
    private EditText serviceET;
    private EditText priceET;

    private MasterWriteViewItem viewItem;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MasterWriteAssembly.configureModule(this);
        setHasOptionsMenu(true);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_master_write, container, false);

        Toolbar toolbar = Objects.requireNonNull(getActivity()).findViewById(R.id.toolbar);

        TextView title = Objects.requireNonNull(getActivity()).findViewById(R.id.titleView);

        toolbar.setVisibility(View.VISIBLE);

        dateET = view.findViewById(R.id.date);
        timeET = view.findViewById(R.id.time);
        clientET = view.findViewById(R.id.client);
        serviceET = view.findViewById(R.id.service);
        priceET = view.findViewById(R.id.price);

        dateET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewOutput.viewDidTapDateEdit(viewItem);
            }
        });

        timeET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewOutput.viewDidTapTimeEdit(viewItem);
            }
        });

        clientET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewOutput.viewDidTapClientEdit();
            }
        });

        serviceET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewOutput.viewDidTapServiceEdit(viewItem);
            }
        });

        title.setText("Новая запись");

        return view;
    }

    public void setViewOutput(MasterWriteViewOutput viewOutput) {
        this.viewOutput = viewOutput;
    }

    @Override
    public void onStart() {
        super.onStart();
        viewOutput.viewDidStarted();
    }

    @Override
    public void loadViewItem(MasterWriteViewItem viewItem) {

        this.viewItem = viewItem;

        clientET.setText(viewItem.client);
        dateET.setText(viewItem.date);
        timeET.setText(viewItem.time);
        serviceET.setText(viewItem.service);
        priceET.setText(viewItem.price);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        inflater.inflate(R.menu.menu_main, menu);

        MenuItem itemAdd = menu.findItem(R.id.add);
        MenuItem itemDelete = menu.findItem(R.id.delete);
        itemSave = menu.findItem(R.id.save);
        MenuItem itemEdit = menu.findItem(R.id.edit);

        itemDelete.setVisible(false);
        itemEdit.setVisible(false);
        itemAdd.setVisible(false);
        itemSave.setVisible(true);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.save) {
            itemSave.setVisible(false);
            viewOutput.viewDidTapSaveButton(viewItem);
        }

        return super.onOptionsItemSelected(item);
    }
}
