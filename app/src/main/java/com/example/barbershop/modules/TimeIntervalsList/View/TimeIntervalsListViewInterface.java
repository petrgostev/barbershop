package com.example.barbershop.modules.TimeIntervalsList.View;

import com.example.barbershop.modules.TimeIntervalsList.Interactor.TimeIntervalViewItem;

import java.util.ArrayList;


public interface TimeIntervalsListViewInterface {

    void showViewItems(ArrayList<TimeIntervalViewItem> viewItems, String date);
}
