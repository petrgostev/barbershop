package com.example.barbershop.modules.ServicesBarberList.View;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.barbershop.R;
import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberViewItem;

import java.util.ArrayList;


public class ServiceBarberAdapter extends RecyclerView.Adapter<ServiceBarberAdapter.ViewHolder>{

    private ArrayList<ServiceBarberViewItem> viewItems;

    private ServiceBarberAdapter.Listener selectListener;

    public ServiceBarberAdapter(Context context, ArrayList<ServiceBarberViewItem> viewItems) {
        this.viewItems = viewItems;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        ViewHolder(CardView cardView) {
            super(cardView);
            this.cardView = cardView;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        CardView cv = (CardView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.service_barber_card, viewGroup, false);

        return new ViewHolder(cv);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceBarberAdapter.ViewHolder viewHolder, int position) {
        CardView cardView = viewHolder.cardView;

        configureView(cardView, position, viewHolder);
    }

    @SuppressLint("SetTextI18n")
    private void configureView(CardView cardView, int position, final ViewHolder viewHolder) {
        final ServiceBarberViewItem item = viewItems.get(position);

        final TextView nameTV = cardView.findViewById(R.id.name);
        final TextView timeIntervalTV = cardView.findViewById(R.id.time_interval);
        final TextView priceTV = cardView.findViewById(R.id.price);

        nameTV.setText(item.name);
        timeIntervalTV.setText(String.valueOf(item.timeInterval) + " час.");
        priceTV.setText(String.valueOf(item.price) + " руб.");

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectListener != null) {
                    selectListener.onClick(viewHolder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return viewItems.size();
    }

    interface Listener {
        void onClick(int position);
    }

    public void setSelectListener(Listener selectListener) {
        this.selectListener = selectListener;
    }
}
