package com.example.barbershop.modules.MastersList.View;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.barbershop.Logic.DataService.Objects.Callback.SelectedObjectCallback;
import com.example.barbershop.R;
import com.example.barbershop.modules.Master.Interactor.MasterViewItem;
import com.example.barbershop.modules.MastersList.Assembly.MastersListAssembly;

import java.util.ArrayList;
import java.util.Objects;


public class MastersListFragment extends Fragment implements MastersListViewInterface {

    public SelectedObjectCallback callback;

    private MastersListViewOutput viewOutput;

    private MasterViewAdapter adapter;

    private RecyclerView mastersRecycler;

    private Toolbar toolbar;

    private MenuItem itemAdd;
    private MenuItem itemEdit;
    private MenuItem itemSave;
    private MenuItem itemDelete;

    private ImageButton mastersButton;
    private TextView mastersTv;

    private boolean isAdmin = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        MastersListAssembly.configureModule(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        mastersRecycler = view.findViewById(R.id.recycler);

        mastersButton = Objects.requireNonNull(getActivity()).findViewById(R.id.masters_button);
        mastersTv = Objects.requireNonNull(getActivity()).findViewById(R.id.masters_tv);

        TextView title = Objects.requireNonNull(getActivity()).findViewById(R.id.titleView);

        toolbar = Objects.requireNonNull(getActivity()).findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);

        title.setText("Мастера");
        return view;
    }

    public void setViewOutput(MastersListViewOutput viewOutput) {
        this.viewOutput = viewOutput;
    }

    @Override
    public void onStart() {
        super.onStart();
        enableIcon(true);
        viewOutput.viewDidStarted();
    }

    @Override
    public void onPause() {
        super.onPause();
        enableIcon(false);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void showViewItems(final ArrayList<MasterViewItem> viewItems, final boolean isAdmin, final boolean isMaster, boolean isClient) {

        this.isAdmin = isAdmin;

        adapter = new MasterViewAdapter(getContext(), viewItems);

        adapter.setSelectListener(new MasterViewAdapter.Listener() {
            @Override
            public void onClick(int position) {
                MasterViewItem viewItem = viewItems.get(position);
                itemAdd.setVisible(false);

                if (isAdmin || isMaster) {
                    viewOutput.viewDidSelectItem(viewItem);
                }
            }
        });

        mastersRecycler.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mastersRecycler.setLayoutManager(layoutManager);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        inflater.inflate(R.menu.menu_main, menu);
        itemDelete = menu.findItem(R.id.delete);
        itemSave = menu.findItem(R.id.save);
        itemEdit = menu.findItem(R.id.edit);
        itemAdd = menu.findItem(R.id.add);

        itemAdd.setVisible(isAdmin);
        itemSave.setVisible(false);
        itemDelete.setVisible(false);
        itemEdit.setVisible(false);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.add) {
            viewOutput.viewDidTapCreateNewMaster();
        }

        return super.onOptionsItemSelected(item);
    }

    //Private

    private void enableIcon(boolean enable) {
        if (enable) {
            mastersButton.setColorFilter(0xffff0000);
            mastersTv.setTextColor(0xffff0000);

        } else {
            mastersButton.setColorFilter(0xffffffff);
            mastersTv.setTextColor(0xffffffff);
        }
    }
}
