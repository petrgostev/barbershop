package com.example.barbershop.modules.ClientWritesList.View;

import com.example.barbershop.modules.ClientWrite.Interactor.ClientWriteViewItem;

import java.util.ArrayList;

public interface ClientWritesListViewInterface {
    void showViewItems(ArrayList<ClientWriteViewItem> viewItems);
}
