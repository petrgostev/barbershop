package com.example.barbershop.modules.ServicesBarberList.Router;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;
import com.example.barbershop.Logic.DataService.Objects.Callback.SelectedObjectCallback;
import com.example.barbershop.R;
import com.example.barbershop.modules.ClientWrite.Assembly.ClientWriteAssembly;
import com.example.barbershop.modules.ClientWrite.View.ClientWriteFragment;
import com.example.barbershop.modules.ServiceBarber.Assembly.ServiceBarberAssembly;
import com.example.barbershop.modules.ServicesBarberList.Assembly.ServicesBarberListAssembly;


public class ServicesBarberListRouter implements ServicesBarberListRouterInput {

    private FragmentActivity getActivity;

    public ServicesBarberListRouter(FragmentActivity getActivity) {
        this.getActivity = getActivity;
    }

    @Override
    public void showInfoAlert(String title) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity, R.style.MyAlertDialogTheme);

        builder.setMessage(title).setPositiveButton("Ок", null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void showEditAlert(String title, final EditCallBack callBack) {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity);

        builder.setTitle(title)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callBack.done(true);
                    }
                })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                callBack.done(false);
                                dialog.cancel();
                            }
                        });

        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void showServiceBarberModule(String id, String masterId, String idToken, boolean isAdmin, boolean isMaster,
                                        boolean isForCreate, boolean isBindToMaster) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = ServiceBarberAssembly.serviceBarberModule(id, masterId, idToken, isAdmin, isMaster, isForCreate, isBindToMaster);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void presentClientWriteFragment(String ownerId, String serviceId, String idToken) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        ClientWriteFragment fragment = ClientWriteAssembly.clientWriteModule(ownerId, serviceId, idToken);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void presentServicesBarberForChoice(String masterId, boolean isBindToMaster, SelectedObjectCallback callback) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = ServicesBarberListAssembly.servicesBarberListModuleForSelectService(masterId, isBindToMaster, callback);

        fragmentTransaction.replace(R.id.frame, fragment, null)
                .addToBackStack(fragment.getTag())
                .commit();
    }

    @Override
    public void closeModule() {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();

        if (fragmentManager != null) {
            fragmentManager.popBackStack();
        }
    }
}
