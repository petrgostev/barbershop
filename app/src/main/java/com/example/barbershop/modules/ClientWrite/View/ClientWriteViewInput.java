package com.example.barbershop.modules.ClientWrite.View;

import com.example.barbershop.modules.ClientWrite.Interactor.WriteTimeItem;
import com.example.barbershop.modules.Master.Interactor.MasterViewItem;
import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberViewItem;

import java.util.ArrayList;
import java.util.Date;


public interface ClientWriteViewInput {

    void fillMasters(ArrayList<MasterViewItem> masters);

    void fillServices(ArrayList<ServiceBarberViewItem> masters);

    void setDate(String dateString);

    void fillDate(Date date, Date currentDate, Date maximumDate);

    void fillTimes(ArrayList<WriteTimeItem> times);
}
