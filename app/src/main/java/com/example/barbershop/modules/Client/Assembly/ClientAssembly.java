package com.example.barbershop.modules.Client.Assembly;

import android.os.Bundle;

import com.example.barbershop.Logic.ClientService.ClientService;
import com.example.barbershop.Logic.ClientService.ClientServiceInterface;
import com.example.barbershop.Logic.FileService.FileService;
import com.example.barbershop.Logic.FileService.FileServiceInterface;
import com.example.barbershop.Logic.UserService.UserService;
import com.example.barbershop.Logic.UserService.UserServiceInterface;
import com.example.barbershop.modules.Client.Interactor.ClientInteractor;
import com.example.barbershop.modules.Client.Presenter.ClientPresenter;
import com.example.barbershop.modules.Client.Router.ClientRouter;
import com.example.barbershop.modules.Client.View.ClientFragment;


public class ClientAssembly {
    private static String ID_KEY = "ID_KEY";
    private static String ID_TOKEN_KEY = "ID_TOKEN_KEY";
    private static String FOR_CREATE_KEY = "FOR_CREATE_KEY";
    private static String MY_PROFILE_KEY = "MY_PROFILE_KEY";

    public static ClientFragment clientModuleMyProfile(String id, String idToken) {
        ClientFragment fragment = new ClientFragment();

        Bundle args = new Bundle();

        args.putBoolean(MY_PROFILE_KEY, true);
        args.putString(ID_KEY, id);
        args.putString(ID_TOKEN_KEY, idToken);

        fragment.setArguments(args);

        return fragment;
    }

    public static ClientFragment clientModule(String id) {
        ClientFragment fragment = new ClientFragment();

        Bundle args = new Bundle();

        if (id != null) {
            args.putString(ID_KEY, id);
        }

        args.putBoolean(FOR_CREATE_KEY, false);

        fragment.setArguments(args);

        return fragment;
    }

    public static ClientFragment clientModuleForCreate() {
        ClientFragment fragment = new ClientFragment();

        Bundle args = new Bundle();

        args.putBoolean(FOR_CREATE_KEY, true);

        fragment.setArguments(args);

        return fragment;
    }

    public static void configureModule(ClientFragment fragment) {
        Bundle args = fragment.getArguments();

        String id = null;
        String idToken = null;
        boolean isForCreate = false;
        boolean isMyProfile = false;

        if (args != null) {
            id = args.getString(ID_KEY);
            idToken = args.getString(ID_TOKEN_KEY);
            isForCreate = args.getBoolean(FOR_CREATE_KEY);
            isMyProfile = args.getBoolean(MY_PROFILE_KEY);
        }

        FileServiceInterface fileService = FileService.getInstance(fragment.getContext());
        ClientServiceInterface clientService = ClientService.getInstance(fragment.getContext());
        UserServiceInterface userService = UserService.getInstance(fragment.getContext());

        ClientInteractor interactor = new ClientInteractor(id, idToken, clientService, userService, fileService);
        ClientPresenter presenter = new ClientPresenter();
        ClientRouter router = new ClientRouter(fragment.getActivity());

        fragment.setViewOutput(presenter);

        presenter.setRouter(router);
        presenter.setInteractor(interactor);
        presenter.setViewInterface(fragment);

        interactor.setOutput(presenter);

        if (isForCreate) {
            presenter.configureForCreate();
        } else if (isMyProfile) {
            presenter.configureForMyProfile();
        } else {
            presenter.configureForEdit();
        }
    }
}
