package com.example.barbershop.modules.ServicesBarberList.Interactor;

import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberViewItem;

import java.util.ArrayList;


public interface ServicesBarberListInteractorOutput {

    void interactorDidLoadViewItems(ArrayList<ServiceBarberViewItem> viewItems);

    void interactorDidAddServiceBarberToMaster();

    void interactorDidError(String error);
}
