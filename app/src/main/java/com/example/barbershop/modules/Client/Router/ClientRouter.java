package com.example.barbershop.modules.Client.Router;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;
import com.example.barbershop.MainActivity;
import com.example.barbershop.R;
import com.example.barbershop.modules.login.Assembly.LoginAssembly;
import com.example.barbershop.modules.login.View.LoginFragment;

import java.util.Calendar;
import java.util.Date;

import static com.example.barbershop.modules.Master.Router.MasterRouter.MY_PERMISSIONS_REQUEST_CAMERA;


public class ClientRouter implements ClientRouterInput {
    private FragmentActivity getActivity;

    public ClientRouter(FragmentActivity getActivity) {
        this.getActivity = getActivity;
    }

    @Override
    public void showEditAlert(String title, final EditCallBack callBack) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity);

        builder.setTitle(title)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callBack.done(true);
                    }
                })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                callBack.done(false);
                                dialog.cancel();
                            }
                        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void showInfoAlert(String title) {
        @SuppressLint("InlinedApi") final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);

        builder.setTitle("Внимание!")
                .setMessage(title)
                .setCancelable(false)
                .setNegativeButton("ОК",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void presentCreatePhoto(String id, String idToken) {
        if (ContextCompat.checkSelfPermission(getActivity,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity,
                    new String[]{Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST_CAMERA);

        } else {
            if (ContextCompat.checkSelfPermission(getActivity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(getActivity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_CAMERA);

            } else {
                ContentValues values = new ContentValues();

                Date currentTime = Calendar.getInstance().getTime();
                String photoId = String.valueOf(currentTime.getTime());


                values.put(MediaStore.Images.Media.TITLE, "BarbershopPhoto" + photoId);
                values.put(MediaStore.Images.Media.DESCRIPTION, "FromyourCamera");
                Uri imageUri = getActivity.getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

                ((MainActivity) getActivity).ownerId = id;
                ((MainActivity) getActivity).idToken = idToken;
                ((MainActivity) getActivity).lastImageUri = imageUri;
                ((MainActivity) getActivity).isMaster = false;

                if (intent.resolveActivity(getActivity.getPackageManager()) != null) {

                    if (ContextCompat.checkSelfPermission(getActivity, android.Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_DENIED) {
                        getActivity.startActivityForResult(intent, MainActivity.REQUEST_IMAGE_CAPTURE);
                    }
                }
            }
        }
    }

    @Override
    public void presentLoginModule() {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        LoginFragment fragment = LoginAssembly.loginModule();

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.commit();
    }
}
