package com.example.barbershop.modules.ClientWrite.Router;

import com.example.barbershop.Logic.DataService.Objects.Callback.DateCallback;
import com.example.barbershop.Logic.DataService.Objects.Callback.SelectedObjectCallback;
import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;

import java.util.Date;


public interface ClientWriteRouterInput {

    void presentAlertDatePickerDialog(Date selectedDay, DateCallback dateCallback);

    void presentMastersForChoice(SelectedObjectCallback callback);

    void showServicesForChoice(String masterId, SelectedObjectCallback callback);

    void closeModule();

    void showEditAlert(String title, EditCallBack callBack);

    void showInfoAlert(String title);

    void showTimesForChoice(String date, String masterId, String idToken, SelectedObjectCallback callback);
}
