package com.example.barbershop.modules.MasterWrite.Interactor;

import java.util.Date;

public interface MasterWriteInteractorInput {
    void createWrite();

    String getIdToken();

    void updateFieldClient(String objectId);

    void saveWrite();

    void updateFieldDate(Date date);

    void updateFieldTime(String timeIntervalId);

    void updateFieldService(String serviceId);
}
