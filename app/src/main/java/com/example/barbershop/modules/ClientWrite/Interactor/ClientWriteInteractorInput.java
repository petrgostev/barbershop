package com.example.barbershop.modules.ClientWrite.Interactor;

import com.example.barbershop.modules.Master.Interactor.MasterViewItem;
import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberViewItem;

import java.util.ArrayList;
import java.util.Date;


public interface ClientWriteInteractorInput {

    void updateFieldDate(Date date);

    void updateFieldMaster(String masterId);

    void saveWrite();

    ArrayList<MasterViewItem> getMasters();

    void setSelectedMaster(MasterViewItem item);

    void setSelectedService(ServiceBarberViewItem item);

    void setDay(Date currentDate);

    Date getSelectedDay();

    void getTimes(TimesCallback callback);

    public interface TimesCallback {
        void loadTimeItems(ArrayList<WriteTimeItem> items);
    }
}
