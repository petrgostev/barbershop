package com.example.barbershop.modules.Photo.Interactor;

public interface PhotoInteractorInput {
    void createPhoto();

    void load();

    void loadForEdit();

    void savePhoto();

    void deletePhoto();
}
