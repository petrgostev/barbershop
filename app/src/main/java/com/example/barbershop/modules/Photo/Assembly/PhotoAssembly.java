package com.example.barbershop.modules.Photo.Assembly;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.example.barbershop.Logic.ClientService.ClientService;
import com.example.barbershop.Logic.ClientService.ClientServiceInterface;
import com.example.barbershop.Logic.FileService.FileService;
import com.example.barbershop.Logic.FileService.FileServiceInterface;
import com.example.barbershop.Logic.MasterService.MasterService;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.modules.Photo.Interactor.PhotoInteractor;
import com.example.barbershop.modules.Photo.Presenter.PhotoPresenter;
import com.example.barbershop.modules.Photo.Router.PhotoRouter;
import com.example.barbershop.modules.Photo.View.PhotoFragment;

public class PhotoAssembly {

    private static String ID_KEY = "ID_KEY";
    private static String FOR_CREATE_KEY = "FOR_CREATE_KEY";
    private static String OWNER_ID_KEY = "OWNER_ID_KEY";
    private static String ID_TOKEN_KEY = "ID_TOKEN_KEY";
    private static String IS_MASTER_KEY = "IS_MASTER_KEY";

    private static Bitmap bitmap;

    public static Fragment photoModuleForCreate(String ownerId, String idToken, Bitmap bitmap, boolean isMaster) {
        PhotoFragment fragment = new PhotoFragment();

        Bundle args = new Bundle();

        if (ownerId != null) {
            args.putString(OWNER_ID_KEY, ownerId);
            args.putString(ID_TOKEN_KEY, idToken);
            args.putBoolean(FOR_CREATE_KEY, true);
            args.putBoolean(IS_MASTER_KEY, isMaster);

            PhotoAssembly.bitmap = bitmap;
        }

        fragment.setArguments(args);

        return fragment;
    }

    public static void configureModule(PhotoFragment fragment) {
        Bundle args = fragment.getArguments();

        String id = null;
        String idToken = null;
        boolean isForCreate = false;
        boolean isMaster = false;
        String ownerId = null;

        if (args != null) {
            id = args.getString(ID_KEY);
            isForCreate = args.getBoolean(FOR_CREATE_KEY);
            isMaster = args.getBoolean(IS_MASTER_KEY);
            ownerId = args.getString(OWNER_ID_KEY);
            idToken = args.getString(ID_TOKEN_KEY);
        }

        FileServiceInterface fileService = FileService.getInstance(fragment.getContext());
        MasterServiceInterface masterService = MasterService.getInstance(fragment.getContext());
        ClientServiceInterface clientService = ClientService.getInstance(fragment.getContext());

        PhotoInteractor interactor = new PhotoInteractor(ownerId, idToken, id, bitmap, fileService, masterService, clientService, isMaster);
        PhotoPresenter presenter = new PhotoPresenter();
        PhotoRouter router = new PhotoRouter(fragment.getActivity());

        fragment.setViewOutput(presenter);

        presenter.setRouter(router);
        presenter.setInteractor(interactor);
        presenter.setViewInterface(fragment);

        interactor.setOutput(presenter);

        if (isForCreate) {
            presenter.configureForCreate();
        } else {
            presenter.configureForEdit();
        }
    }
}
