package com.example.barbershop.modules.WorkMode.Interactor;

import android.text.Editable;

public interface WorkModeInteractorInput {
    void createWorkMode();

    void saveWorkMode(Editable startWork, Editable endWork, Editable startTimeout,
                      Editable endTimeout, Editable weekend);

    String getOwnerId();

    void loadWorkMode();
}
