package com.example.barbershop.modules.Master.Interactor;

import com.example.barbershop.Logic.DataService.DataServiceInterface;
import com.example.barbershop.Logic.DataService.Objects.WorkMode;
import com.example.barbershop.Logic.FileService.FileCallback;
import com.example.barbershop.Logic.FileService.FileServiceInterface;
import com.example.barbershop.Logic.FileService.Objects.Photo;
import com.example.barbershop.Logic.MasterService.Objects.Master;
import com.example.barbershop.Logic.MasterService.MasterCallback;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.Logic.UserService.UserCallback;
import com.example.barbershop.Logic.UserService.UserServiceInterface;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;


public class MasterInteractor implements MasterInteractorInput {

    private String id;
    private String idToken;
    private boolean isAdmin;
    private boolean isMaster;

    private MasterInteractorOutput output;
    private Master master;
    private MasterViewItem viewItem;

    private MasterServiceInterface masterService;
    private UserServiceInterface userService;
    private FileServiceInterface fileService;
    private DataServiceInterface dataService;

    public MasterInteractor(String id, String idToken, boolean isAdmin, boolean isMaster, MasterServiceInterface masterService,
                            UserServiceInterface userService, FileServiceInterface fileService, DataServiceInterface dataService) {
        this.id = id;
        this.idToken = idToken;
        this.isAdmin = isAdmin;
        this.isMaster = isMaster;
        this.masterService = masterService;
        this.userService = userService;
        this.fileService = fileService;
        this.dataService = dataService;
    }

    public void setOutput(MasterInteractorOutput output) {
        this.output = output;
    }

    @Override
    public void createMaster() {
        id = UUID.randomUUID().toString();

        master = new Master(id, null, null, null, null,
                null, null, null, null,
                null, true);
    }

    @Override
    public void registrationMaster(final String surname, final String name,
                                   final String patronymic, final String phone,
                                   final String email, final String password, String workingHours) {

        userService.registrationNewUser(isAdmin, email, password, new UserCallback() {
            @Override
            public void done(Error error, final String id, final String idToken, final boolean isAdmin, final boolean isMaster) {
                if (error == null) {
                    masterService.registrationNewMaster(idToken, id, surname, name, patronymic, phone,
                            email, password, new MasterCallback() {
                                @Override
                                public void done(Error error, Master master) {
                                    if (error == null && master != null) {

                                        if (isAdmin) {
                                            output.adminDidRegistrationNewMaster();

                                        } else {
                                            output.interactorDidRegistrationNewMaster(id, idToken, isAdmin, isMaster);
                                        }

                                    } else {
                                        output.interactorDidReceivedError(error);
                                    }
                                }
                            });
                }
            }
        });
    }

    @Override
    public void updateMaster(String surname, String name, String patronymic, String phone,
                             String email, String password, String workingHours) {
        master.setSurname(surname);
        master.setName(name);
        master.setPhone(phone);
        master.setEmail(email);
        master.setPassword(password);

        masterService.updateMaster(master, new MasterCallback() {
            @Override
            public void done(Error error, Master master) {
                if (error == null && master != null) {
                    loadMasterViewItem(master);

                    output.interactorDidUpdateViewItem(viewItem);
                }
            }
        });
    }

    @Override
    public void deleteMaster() {

        if (isAdmin) {
            userService.authenticationUserForAdmin(master.getEmail(), master.getPassword(), new UserCallback() {
                @Override
                public void done(Error error, final String id, final String idTokenMaster, boolean admin, boolean master) {
                    if (error == null && idTokenMaster != null) {
                        userService.deleteUser(idTokenMaster, new UserCallback() {
                            @Override
                            public void done(Error error, String userId, String token, final boolean admin, boolean master) {

                                if (error == null) {
                                    masterService.deleteMaster(id, idToken, new MasterCallback() {
                                        @Override
                                        public void done(Error error, Master master) {
                                            if (error == null) {

                                                output.interactorDidDeleteMaster();

                                            } else {
                                                output.interactorDidReceivedError(error);
                                            }
                                        }
                                    });
                                } else {
                                    output.interactorDidReceivedError(error);
                                }
                            }
                        });

                    } else {
                        output.interactorDidReceivedError(error);
                    }
                }
            });
        } else {
            removeMaster(idToken);
        }
    }

    @Override
    public void loadMaster() {
        masterService.getMasterById(id, idToken, new MasterCallback() {
            @Override
            public void done(Error error, Master master) {
                MasterInteractor.this.master = master;

                loadMasterViewItem(master);

                output.interactorDidLoadViewItem(viewItem);
            }
        });
    }

    @Override
    public void logoutMaster() {
        userService.logoutUser(new UserCallback() {
            @Override
            public void done(Error error, String id, String idToken, boolean isAdmin, boolean isMaster) {

                if (error == null) {
                    output.interactorDidLogoutMaster();

                } else {
                    //
                }
            }
        });
    }

    @Override
    public void updateData(String surname, String name, String patronymic, String phone, String email, String password) {
        master.setSurname(surname);
        master.setName(name);
        master.setPhone(phone);
        master.setEmail(email);
        master.setPassword(password);
    }

    @Override
    public void updateWorkMode(WorkMode workMode) {
//        master.setWorkMode(workMode);

        loadMasterViewItem(master);
        output.interactorDidUpdateViewItem(viewItem);
    }

    @Override
    public void removePhoto() {
        fileService.deletePhoto(id, master.getPhotoName(), new FileCallback() {
            @Override
            public void done(Error error, Photo photo) {
                if (error == null) {
                    master.removePhoto();

                    masterService.updateMaster(master, new MasterCallback() {
                        @Override
                        public void done(Error error, Master master) {
                            if (error == null && master != null) {
                                output.interactorDidDeletePhoto();

                            } else {
                                output.interactorDidReceivedError(error);
                            }
                        }
                    });
                } else {
                    output.interactorDidReceivedError(error);
                }
            }
        });
    }

    //Private

    private void removeMaster(final String idTokenMaster) {

        userService.deleteUser(idTokenMaster, new UserCallback() {
            @Override
            public void done(Error error, String userId, String token, boolean admin, boolean master) {

                if (error == null) {
                    masterService.deleteMaster(userId, idTokenMaster, new MasterCallback() {
                        @Override
                        public void done(Error error, Master master) {
                            if (error == null) {
                                boolean isMaster = true;

                                output.interactorDidDeleteMaster();

                            } else {
                                output.interactorDidReceivedError(error);
                            }
                        }
                    });
                } else {
                    output.interactorDidReceivedError(error);
                }
            }
        });
    }

    private void loadMasterViewItem(Master master) {
        viewItem = new MasterViewItem();

        viewItem.surname = master.getSurname();
        viewItem.name = master.getName();
        viewItem.fullName = master.getFullName();
        viewItem.email = master.getEmail();
        viewItem.password = master.getPassword();
        viewItem.phone = master.getPhone();
        viewItem.isAdmin = isAdmin;
        viewItem.isMaster = isMaster;
        viewItem.src = master.getPhotoSrc();

//        if (master.getWorkMode() != null) {
//
//            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
//            String pattern = "#0.00";
//            DecimalFormat decimalFormat = new DecimalFormat(pattern, otherSymbols);
//
//            String startWork = decimalFormat.format(master.getWorkMode().getStartWork());
//            String endWork = decimalFormat.format(master.getWorkMode().getEndWork());
//            String startTimeout = decimalFormat.format(master.getWorkMode().getStartTimeout());
//            String endTimeout = decimalFormat.format(master.getWorkMode().getEndTimeout());
//            String weekend = master.getWorkMode().getWeekend();
//
//            viewItem.workMode = "с " + startWork + " до " + endWork + "\n" + "перерыв: с " + startTimeout + " до " + endTimeout + "\n" + "выходной: " + weekend;
//        }
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getIdToken() {
        return idToken;
    }

    @Override
    public boolean isAdmin() {
        return isAdmin;
    }

    @Override
    public boolean isMaster() {
        return isMaster;
    }
}
