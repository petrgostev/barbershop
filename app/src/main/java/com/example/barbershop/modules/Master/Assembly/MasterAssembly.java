package com.example.barbershop.modules.Master.Assembly;

import android.os.Bundle;

import com.example.barbershop.Logic.DataService.DataService;
import com.example.barbershop.Logic.DataService.DataServiceInterface;
import com.example.barbershop.Logic.FileService.FileService;
import com.example.barbershop.Logic.FileService.FileServiceInterface;
import com.example.barbershop.Logic.MasterService.MasterService;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.Logic.UserService.UserService;
import com.example.barbershop.Logic.UserService.UserServiceInterface;
import com.example.barbershop.modules.Master.Interactor.MasterInteractor;
import com.example.barbershop.modules.Master.Presenter.MasterPresenter;
import com.example.barbershop.modules.Master.Router.MasterRouter;
import com.example.barbershop.modules.Master.View.MasterFragment;


public class MasterAssembly {

    private static String ID_KEY = "ID_KEY";
    private static String ID_TOKEN_KEY = "ID_TOKEN_KEY";
    private static String FOR_CREATE_KEY = "FOR_CREATE_KEY";
    private static String FOR_EDIT_KEY = "FOR_EDIT_KEY";
    private static String MY_PROFILE_KEY = "MY_PROFILE_KEY";
    private static String IS_WRITE_KEY = "IS_WRITE_KEY";
    private static String IS_ADMIN_KEY = "IS_ADMIN_KEY";
    private static String IS_MASTER_KEY = "IS_MASTER_KEY";

    public static MasterFragment masterModuleMyProfile(String id, String idToken, boolean isAdmin) {
        MasterFragment fragment = new MasterFragment();

        Bundle args = new Bundle();

        args.putBoolean(IS_ADMIN_KEY, isAdmin);
        args.putBoolean(MY_PROFILE_KEY, true);
        args.putString(ID_KEY, id);
        args.putString(ID_TOKEN_KEY, idToken);

        fragment.setArguments(args);

        return fragment;
    }

    public static MasterFragment masterModuleForEdit(String id) {
        MasterFragment fragment = new MasterFragment();

        Bundle args = new Bundle();

        args.putBoolean(FOR_EDIT_KEY, true);
        args.putString(ID_KEY, id);

        fragment.setArguments(args);

        return fragment;
    }

    public static MasterFragment masterModuleForBrowse(String id, String idToken, boolean isAdmin, boolean isMaster, boolean isWrite) {
        MasterFragment fragment = new MasterFragment();

        Bundle args = new Bundle();

        if (id != null) {
            args.putString(ID_KEY, id);
        }

        args.putString(ID_TOKEN_KEY, idToken);
        args.putBoolean(IS_WRITE_KEY, isWrite);
        args.putBoolean(IS_ADMIN_KEY, isAdmin);
        args.putBoolean(IS_MASTER_KEY, isMaster);

        fragment.setArguments(args);

        return fragment;
    }

    public static MasterFragment masterModuleForCreate(boolean isAdmin) {
        MasterFragment fragment = new MasterFragment();

        Bundle args = new Bundle();

        args.putBoolean(FOR_CREATE_KEY, true);
        args.putBoolean(IS_ADMIN_KEY, isAdmin);

        fragment.setArguments(args);

        return fragment;
    }

    public static void configureModule(MasterFragment fragment) {
        Bundle args = fragment.getArguments();

        String id = null;
        String idToken = null;
        boolean isForCreate = false;
        boolean isMyProfile = false;
        boolean isForEdit = false;
        boolean isAdmin = false;
        boolean isMaster = false;

        if (args != null) {
            id = args.getString(ID_KEY);
            idToken = args.getString(ID_TOKEN_KEY);
            isForCreate = args.getBoolean(FOR_CREATE_KEY);
            isMyProfile = args.getBoolean(MY_PROFILE_KEY);
            isForEdit = args.getBoolean(FOR_EDIT_KEY);
            isAdmin = args.getBoolean(IS_ADMIN_KEY);
            isMaster = args.getBoolean(IS_MASTER_KEY);
        }

        MasterServiceInterface masterService = MasterService.getInstance(fragment.getContext());
        UserServiceInterface userService = UserService.getInstance(fragment.getContext());
        FileServiceInterface fileService = FileService.getInstance(fragment.getContext());
        DataServiceInterface dataService = DataService.getInstance(fragment.getContext());

        MasterInteractor interactor = new MasterInteractor(id, idToken, isAdmin, isMaster, masterService, userService, fileService, dataService);
        MasterPresenter presenter = new MasterPresenter();
        MasterRouter router = new MasterRouter(fragment.getActivity());

        fragment.setViewOutput(presenter);

        presenter.setRouter(router);
        presenter.setInteractor(interactor);
        presenter.setViewInterface(fragment);

        interactor.setOutput(presenter);

        if (isForCreate) {
            presenter.configureForCreate();
        } else if (isMyProfile) {
            presenter.configureForMyProfile();
        } else if (isForEdit){
            presenter.configureForEdit();
        }
    }
}
