package com.example.barbershop.modules.Loader.View;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.example.barbershop.R;
import com.example.barbershop.modules.Loader.Assembly.LoaderAssembly;

import java.util.Objects;


public class LoaderFragment extends Fragment implements LoaderViewInterface {

    private LoaderViewOutput viewOutput;

    Toolbar toolbar;
    ImageView loaderImageView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LoaderAssembly.configureModule(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_loader, container, false);

        toolbar = Objects.requireNonNull(getActivity()).findViewById(R.id.toolbar);

        toolbar.setVisibility(View.GONE);
        loaderImageView = view.findViewById(R.id.loaderImageView);

        return view;
    }

    public void setViewOutput(LoaderViewOutput viewOutput) {
        this.viewOutput = viewOutput;
    }

    @Override
    public void onResume() {
        super.onResume();
//        //конфигурирование анимации
//        RotateAnimation anim = new RotateAnimation(0f, 360f,
//                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
//                0.5f);
//        anim.setInterpolator(new LinearInterpolator());
//        anim.setRepeatCount(Animation.INFINITE);
//        anim.setDuration(2000);
//
//        loaderImageView.startAnimation(anim);//запуск анимации

        viewOutput.viewDidStarted();
    }

    public void showBar() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
