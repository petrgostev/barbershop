package com.example.barbershop.modules.login.Interactor;

import com.example.barbershop.Logic.UserService.UserCallback;
import com.example.barbershop.Logic.UserService.UserServiceInterface;


public class LoginInteractor implements LoginInteractorInput {

    private LoginInteractorOutput output;
    private UserServiceInterface userService;

    public LoginInteractor(UserServiceInterface userService) {
        this.userService = userService;
    }

    public void setOutput(LoginInteractorOutput output) {
        this.output = output;
    }

    @Override
    public void authenticationUser(String login, String password) {
        userService.authenticationUser(login, password, new UserCallback() {
            @Override
            public void done(Error error, String id, String idToken, boolean isAdmin, boolean isMaster) {
                if (error != null) {
                    output.interactorDidReceivedError(error);

                } else {
                    if (isMaster) {
                        output.interactorDidAuthenticationMaster(id, idToken, isAdmin, isMaster);
                    } else {
                        output.interactorDidAuthenticationClient(id, idToken);
                    }
                }
            }
        });
    }
}
