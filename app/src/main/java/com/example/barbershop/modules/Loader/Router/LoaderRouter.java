package com.example.barbershop.modules.Loader.Router;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.example.barbershop.R;
import com.example.barbershop.modules.ClientWritesList.Assembly.ClientWritesListAssembly;
import com.example.barbershop.modules.MasterWritesList.Assembly.MasterWritesListAssembly;
import com.example.barbershop.modules.login.Assembly.LoginAssembly;
import com.example.barbershop.modules.login.View.LoginFragment;

public class LoaderRouter implements LoaderRouterInput {

    private FragmentActivity activity;

    public LoaderRouter(FragmentActivity activity) {
        this.activity = activity;
    }

    @Override
    public void presentLogin() {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        LoginFragment fragment = LoginAssembly.loginModule();

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.commit();
    }

    @Override
    public void presentMasterWritesList(String id, String idToken, boolean isAdmin, boolean isMaster) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = MasterWritesListAssembly.MyWritesListModule(id, idToken, isAdmin, isMaster);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void presentClientWritesList(String id, String idToken) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = ClientWritesListAssembly.MyWritesListModule(id, idToken);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
