package com.example.barbershop.modules.WorkMode.Assembly;

import android.os.Bundle;

import com.example.barbershop.Logic.ClientService.ClientService;
import com.example.barbershop.Logic.ClientService.ClientServiceInterface;
import com.example.barbershop.Logic.DataService.DataService;
import com.example.barbershop.Logic.DataService.DataServiceInterface;
import com.example.barbershop.Logic.DataService.Objects.Callback.WorkModeCallback;
import com.example.barbershop.Logic.MasterService.MasterService;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.modules.WorkMode.Interactor.WorkModeInteractor;
import com.example.barbershop.modules.WorkMode.Presenter.WorkModePresenter;
import com.example.barbershop.modules.WorkMode.Router.WorkModeRouter;
import com.example.barbershop.modules.WorkMode.View.WorkModeFragment;


public class WorkModeAssembly {

    private static String OWNER_ID_KEY = "ID_KEY";
    private static String ID_TOKEN_KEY = "ID_TOKEN_KEY";
    private static String IS_FOR_CREATE_KEY = "ID_TOKEN_KEY";

    private WorkModeCallback callback;

    public static WorkModeFragment workModeModuleForCreate(String ownerId, WorkModeCallback callback) {
        WorkModeFragment fragment = new WorkModeFragment();

        Bundle args = new Bundle();

        args.putString(OWNER_ID_KEY, ownerId);
        args.putBoolean(IS_FOR_CREATE_KEY, true);

        fragment.setArguments(args);

        fragment.callback = callback;

        return fragment;
    }

    public static WorkModeFragment workModeModule(String ownerId) {
        WorkModeFragment fragment = new WorkModeFragment();

        Bundle args = new Bundle();

        args.putString(OWNER_ID_KEY, ownerId);

        fragment.setArguments(args);

        return fragment;
    }

    public static void configureModule(WorkModeFragment fragment) {
        Bundle args = fragment.getArguments();

        String ownerId = null;
        boolean isForCreate = false;

        if (args != null) {
            ownerId = args.getString(OWNER_ID_KEY);
            isForCreate = args.getBoolean(IS_FOR_CREATE_KEY);
        }

        ClientServiceInterface clientService = ClientService.getInstance(fragment.getContext());
        MasterServiceInterface masterService = MasterService.getInstance(fragment.getContext());
        DataServiceInterface dataService = DataService.getInstance(fragment.getContext());

        WorkModeInteractor interactor = new WorkModeInteractor(ownerId, clientService, masterService, dataService);
        WorkModePresenter presenter = new WorkModePresenter();
        WorkModeRouter router = new WorkModeRouter(fragment.getActivity());

        fragment.setViewOutput(presenter);

        interactor.workModeCallback = fragment.callback;

        presenter.setRouter(router);
        presenter.setInteractor(interactor);
        presenter.setViewInterface(fragment);

        interactor.setOutput(presenter);

        if (isForCreate) {
            presenter.configureForCreate();
        }
    }
}
