package com.example.barbershop.modules.MasterWrite.Interactor;

public interface MasterWriteInteractorOutput {
    void interactorDidLoadViewItem(MasterWriteViewItem viewItem);

    void interactorDidSaveWrite();

    void interactorDidError(String s);
}
