package com.example.barbershop.modules.MasterWrite.Assembly;

import android.os.Bundle;

import com.example.barbershop.Logic.ClientService.ClientService;
import com.example.barbershop.Logic.ClientService.ClientServiceInterface;
import com.example.barbershop.Logic.DataService.DataService;
import com.example.barbershop.Logic.DataService.DataServiceInterface;
import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;
import com.example.barbershop.Logic.MasterService.MasterService;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.modules.MasterWrite.Interactor.MasterWriteInteractor;
import com.example.barbershop.modules.MasterWrite.Presenter.MasterWritePresenter;
import com.example.barbershop.modules.MasterWrite.Router.MasterWriteRouter;
import com.example.barbershop.modules.MasterWrite.View.MasterWriteFragment;


public class MasterWriteAssembly {

    private static String OWNER_ID_KEY = "OWNER_ID_KEY";
    private static String ID_TOKEN_KEY = "ID_TOKEN_KEY";
    private EditCallBack callBack;

    public static MasterWriteFragment masterWriteModule(String ownerId, String idToken, EditCallBack callBack) {
        MasterWriteFragment fragment = new MasterWriteFragment();
        fragment.callBack = callBack;

        Bundle args = new Bundle();

        args.putString(OWNER_ID_KEY, ownerId);
        args.putString(ID_TOKEN_KEY, idToken);

        fragment.setArguments(args);

        return fragment;
    }

    public static void configureModule(MasterWriteFragment fragment) {
        Bundle args = fragment.getArguments();

        String ownerId = null;
        String idToken = null;

        if (args != null) {
            ownerId = args.getString(OWNER_ID_KEY);
            idToken = args.getString(ID_TOKEN_KEY);
        }

        ClientServiceInterface clientService = ClientService.getInstance(fragment.getContext());
        MasterServiceInterface masterService = MasterService.getInstance(fragment.getContext());
        DataServiceInterface dataService = DataService.getInstance(fragment.getContext());

        MasterWriteInteractor interactor = new MasterWriteInteractor(ownerId, idToken, clientService, masterService, dataService);
        MasterWritePresenter presenter = new MasterWritePresenter();
        MasterWriteRouter router = new MasterWriteRouter(fragment.getActivity());

        fragment.setViewOutput(presenter);

        presenter.setRouter(router);
        presenter.setInteractor(interactor);
        presenter.setViewInterface(fragment);
        presenter.setEditCallBack(fragment.callBack);

        interactor.setOutput(presenter);
    }
}
