package com.example.barbershop.modules.MasterWritesList.Router;

import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;


public interface MasterWritesListRouterInput {

    void presentMasterWriteFragment(String ownerId, String idToken, EditCallBack callBack);

    void showEditAlert(String title, EditCallBack callBack);

    void presentMasterFragment(String id, String idToken, boolean isAdmin);

    void presentMastersListFragment(boolean isAdmin, boolean isMaster, String idToken);

    void presentMasterWritesList(String id, String idToken, boolean isAdmin, boolean isMaster);

    void presentServicesBarberListFragment(String id, String idToken, boolean isAdmin, boolean isMaster);
}
