package com.example.barbershop.modules.ClientsForChoice.Interactor;

public interface ClientsForChoiceInteractorInput {
    void loadClients(String query);
}
