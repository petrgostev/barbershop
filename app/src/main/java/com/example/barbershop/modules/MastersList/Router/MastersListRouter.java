package com.example.barbershop.modules.MastersList.Router;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.example.barbershop.R;
import com.example.barbershop.modules.Master.Assembly.MasterAssembly;
import com.example.barbershop.modules.Master.View.MasterFragment;


public class MastersListRouter implements MastersListRouterInput {
    FragmentActivity getActivity;

    public MastersListRouter(FragmentActivity getActivity) {
        this.getActivity = getActivity;
    }

    @Override
    public void showMasterModuleForCreate(boolean isAdmin) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        MasterFragment fragment = MasterAssembly.masterModuleForCreate(isAdmin);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void presentMasterFragment(String id, String idToken, boolean isAdmin, boolean isMaster, boolean isWrite) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        MasterFragment fragment = MasterAssembly.masterModuleForBrowse(id, idToken, isAdmin, isMaster, isWrite);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void closeModule() {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();

        if (fragmentManager != null) {
            fragmentManager.popBackStack();
        }
    }
}
