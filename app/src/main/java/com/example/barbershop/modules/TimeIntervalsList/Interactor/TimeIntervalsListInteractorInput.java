package com.example.barbershop.modules.TimeIntervalsList.Interactor;


public interface TimeIntervalsListInteractorInput {
    void loadTimeIntervals();
}
