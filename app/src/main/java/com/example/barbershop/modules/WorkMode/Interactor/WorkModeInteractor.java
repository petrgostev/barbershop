package com.example.barbershop.modules.WorkMode.Interactor;

import android.text.Editable;

import com.example.barbershop.Logic.ClientService.ClientServiceInterface;
import com.example.barbershop.Logic.DataService.DataServiceInterface;
import com.example.barbershop.Logic.DataService.Objects.Callback.WorkModeCallback;
import com.example.barbershop.Logic.DataService.Objects.WorkMode;
import com.example.barbershop.Logic.MasterService.MasterCallback;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.Logic.MasterService.Objects.Master;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;


public class WorkModeInteractor implements WorkModeInteractorInput {

    private String ownerId;
    private String idToken;

    public WorkModeCallback workModeCallback;

    private WorkModeInteractorOutput output;

    private ClientServiceInterface clientService;
    private MasterServiceInterface masterService;
    private DataServiceInterface dataService;

    private WorkMode workMode;

    public WorkModeInteractor(String ownerId, ClientServiceInterface clientService, MasterServiceInterface masterService,
                              DataServiceInterface dataService) {
        this.ownerId = ownerId;
        this.clientService = clientService;
        this.masterService = masterService;
        this.dataService = dataService;
    }

    public void setOutput(WorkModeInteractorOutput output) {
        this.output = output;
    }

    @Override
    public void createWorkMode() {
        workMode = new WorkMode();
    }

    @Override
    public void loadWorkMode() {
//        masterService.getMasterById(ownerId, idToken, new MasterCallback() {
//            @Override
//            public void done(Error error, Master master) {
//                if (error == null && master != null) {
//                    workMode = master.getWorkMode();
//
//                    loadViewItem(workMode);
//
//                } else {
//                    //error
//                }
//            }
//        });
    }

    @Override
    public void saveWorkMode(Editable startWork, Editable endWork, Editable startTimeout,
                             Editable endTimeout, Editable weekend) {

        if (startWork.toString().equals("") || endWork.toString().equals("")
                || startTimeout.toString().equals("") || endTimeout.toString().equals("")) {
            output.interactorDidError("Все поля должны быть заполнены!");
            return;
        }

        double startWorkDouble = Double.parseDouble(startWork.toString());
        double endWorkDouble = Double.parseDouble(endWork.toString());
        double startTimeoutDouble = Double.parseDouble(startTimeout.toString());
        double endTimeoutDouble = Double.parseDouble(endTimeout.toString());
        String weekendString = weekend.toString();

        if (startWorkDouble > 24 || endWorkDouble > 24 || startTimeoutDouble > 24 || endTimeoutDouble > 24) {
            output.interactorDidError("Все поля должны быть заполнены корректно!");
            return;
        }

        workMode.setStartWork(startWorkDouble);
        workMode.setEndWork(endWorkDouble);
        workMode.setStartTimeout(startTimeoutDouble);
        workMode.setEndTimeout(endTimeoutDouble);
        workMode.setWeekend(weekendString);

        if (workModeCallback != null) workModeCallback.done(workMode);

        output.ineractorDidSaveWorkMode();
    }

    @Override
    public String getOwnerId() {
        return ownerId;
    }

    //Private

    private void loadViewItem(WorkMode workMode) {
        WorkModeViewItem viewItem = new WorkModeViewItem();

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
        String pattern = "#0.00";
        DecimalFormat decimalFormat = new DecimalFormat(pattern, otherSymbols);

        viewItem.startWork = decimalFormat.format(workMode.getStartWork());
        viewItem.endWork = decimalFormat.format(workMode.getEndWork());
        viewItem.startTimeout = decimalFormat.format(workMode.getStartTimeout());
        viewItem.endTimeout = decimalFormat.format(workMode.getEndTimeout());
        viewItem.lengthWorkingDay = decimalFormat.format(workMode.getLengthWorkingDay());
        viewItem.weekend = workMode.getWeekend();

        output.ineractorDidLoadViewItem(viewItem);
    }
}
