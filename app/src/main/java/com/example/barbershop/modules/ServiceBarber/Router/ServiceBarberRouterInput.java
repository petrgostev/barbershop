package com.example.barbershop.modules.ServiceBarber.Router;

import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;


public interface ServiceBarberRouterInput {
    void showEditAlert(String title, EditCallBack callBack);

    void showInfoAlert(String error);

    void presentServicesBarberListFragment(String masterId, String idToken, boolean isAdmin, boolean isMaster);

    void closeModule();
}
