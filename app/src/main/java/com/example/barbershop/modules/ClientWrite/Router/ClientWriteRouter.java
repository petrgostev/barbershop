package com.example.barbershop.modules.ClientWrite.Router;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.DatePicker;

import com.example.barbershop.Logic.DataService.Objects.Callback.DateCallback;
import com.example.barbershop.Logic.DataService.Objects.Callback.SelectedObjectCallback;
import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;
import com.example.barbershop.R;
import com.example.barbershop.modules.MastersList.Assembly.MastersListAssembly;
import com.example.barbershop.modules.ServicesBarberList.Assembly.ServicesBarberListAssembly;
import com.example.barbershop.modules.TimeIntervalsList.Assembly.TimeIntervalsListAssembly;

import java.util.Calendar;
import java.util.Date;


public class ClientWriteRouter implements ClientWriteRouterInput {

    private FragmentActivity getActivity;

    Date date;

    public ClientWriteRouter(FragmentActivity getActivity) {
        this.getActivity = getActivity;
    }

    @Override
    public void presentMastersForChoice(SelectedObjectCallback callback) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = MastersListAssembly.MastersListModuleForSelectMaster(callback);

        fragmentTransaction.replace(R.id.frame, fragment, null)
        .addToBackStack(fragment.getTag())
        .commit();
    }

    @Override
    public void showInfoAlert(String title) {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity);

        builder.setTitle(title)
                .setCancelable(false)
                .setNegativeButton("ОК",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void presentAlertDatePickerDialog(Date selectedDay, final DateCallback callback) {
        final Calendar cal = Calendar.getInstance();

        if (selectedDay != null) {
            cal.setTime(selectedDay);
        }

        final int mYear = cal.get(Calendar.YEAR);
        final int mMonth = cal.get(Calendar.MONTH);
        final int mDay = cal.get(Calendar.DAY_OF_MONTH);

        // инициализируем диалог выбора даты текущими значениями
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity,
                android.R.style.Theme_Material_Dialog, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar newCal = Calendar.getInstance();
                newCal.set(year, monthOfYear, dayOfMonth);
                date = newCal.getTime();

                callback.done(date);
            }
        }, mYear, mMonth, mDay);


//        datePickerDialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "", datePickerDialog);
//        datePickerDialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "", datePickerDialog);

        //set MIN and MAX dates for select
        datePickerDialog.getDatePicker().setMinDate(new Date().getTime());
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime() + 1000 * 60 * 60 * 24 * 14);
        datePickerDialog.show();

//        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setVisibility(GONE);
//        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setVisibility(GONE);

    }

    @Override
    public void showEditAlert(String title, final EditCallBack callBack) {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity);

        builder.setTitle(title)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callBack.done(true);
                    }
                })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                callBack.done(false);
                                dialog.cancel();
                            }
                        });

        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void showServicesForChoice(String masterId, SelectedObjectCallback callback) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = ServicesBarberListAssembly.servicesBarberListModuleForSelectService(masterId, true, callback);

        fragmentTransaction.replace(R.id.frame, fragment, null)
                .addToBackStack(fragment.getTag())
                .commit();
    }

    @Override
    public void showTimesForChoice(String date, String masterId, String idToken, SelectedObjectCallback callback) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = TimeIntervalsListAssembly.TimeIntervalsListModule(date, masterId, idToken, callback);

        fragmentTransaction.replace(R.id.frame, fragment, null)
                .addToBackStack(fragment.getTag())
                .commit();
    }

    @Override
    public void closeModule() {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();

        if (fragmentManager != null) {
            fragmentManager.popBackStack();
        }
    }
}
