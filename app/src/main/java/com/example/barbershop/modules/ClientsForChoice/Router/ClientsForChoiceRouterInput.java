package com.example.barbershop.modules.ClientsForChoice.Router;


public interface ClientsForChoiceRouterInput {
    void closeModule();

    void showInfoAlert(String title);
}
