package com.example.barbershop.modules.login.Router;

import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;

import com.example.barbershop.R;
import com.example.barbershop.modules.Client.Assembly.ClientAssembly;
import com.example.barbershop.modules.Client.View.ClientFragment;
import com.example.barbershop.modules.Master.Assembly.MasterAssembly;
import com.example.barbershop.modules.Master.View.MasterFragment;
import com.example.barbershop.modules.ClientWritesList.Assembly.ClientWritesListAssembly;
import com.example.barbershop.modules.MasterWritesList.Assembly.MasterWritesListAssembly;


public class LoginRouter implements LoginRouterInput {

    private FragmentActivity activity;

    public LoginRouter(FragmentActivity activity) {
        this.activity = activity;
    }

    @Override
    public void presentClientFragment() {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        ClientFragment fragment = ClientAssembly.clientModuleForCreate();

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void presentMasterFragment() {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        MasterFragment fragment = MasterAssembly.masterModuleForCreate(false);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void presentMasterWritesList(String id, String idToken, boolean isAdmin, boolean isMaster) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = MasterWritesListAssembly.MyWritesListModule(id, idToken, isAdmin, isMaster);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void presentClientWritesList(String id, String idToken) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = ClientWritesListAssembly.MyWritesListModule(id, idToken);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void showInfoAlert(Error error) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(error.toString())
                .setCancelable(false)
                .setNegativeButton("ОК",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
