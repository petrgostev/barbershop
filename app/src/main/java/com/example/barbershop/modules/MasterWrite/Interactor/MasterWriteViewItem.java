package com.example.barbershop.modules.MasterWrite.Interactor;


public class MasterWriteViewItem {

    public String date;
    public String time;
    public String client;
    public String service;
    public String price;
    public String id;
    public String clientId;
    public String masterId;
}
