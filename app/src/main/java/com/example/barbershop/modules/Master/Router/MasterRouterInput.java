package com.example.barbershop.modules.Master.Router;

import com.example.barbershop.Logic.DataService.Objects.Callback.WorkModeCallback;
import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;


public interface MasterRouterInput {

    void showInfoAlert(String title);

    void closeModule();

    void showEditAlert(String title, EditCallBack callBack);

    void presentCreatePhoto(String id, String idToken);

    void presentCreateWorkMode(String id, WorkModeCallback callback);

    void presentServicesBarberForBrowse(String id, String idToken, boolean isAdmin, boolean isMaster);

    void presentWorkMode(String id);

    void presentLoginModule();

    void presentMastersListModule(boolean isAdmin, boolean isMaster, String idToken);

    void presentInfoFragment();
}
