package com.example.barbershop.modules.ServiceBarber.View;

import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberViewItem;


public interface ServiceBarberViewInterface {
    
    void showViewItem(ServiceBarberViewItem viewItem);

    void setEditMode(boolean isEdit);
}
