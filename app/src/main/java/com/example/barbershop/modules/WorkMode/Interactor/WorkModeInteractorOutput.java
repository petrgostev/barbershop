package com.example.barbershop.modules.WorkMode.Interactor;

public interface WorkModeInteractorOutput {
    void ineractorDidSaveWorkMode();

    void interactorDidError(String error);

    void ineractorDidLoadViewItem(WorkModeViewItem viewItem);
}
