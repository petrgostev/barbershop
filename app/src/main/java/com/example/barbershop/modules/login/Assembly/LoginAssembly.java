package com.example.barbershop.modules.login.Assembly;

import com.example.barbershop.Logic.MasterService.MasterService;
import com.example.barbershop.Logic.UserService.UserService;
import com.example.barbershop.MainActivity;
import com.example.barbershop.modules.login.Interactor.LoginInteractor;
import com.example.barbershop.modules.login.Presenter.LoginPresenter;
import com.example.barbershop.modules.login.Router.LoginRouter;
import com.example.barbershop.modules.login.View.LoginFragment;


public class LoginAssembly {

    public static LoginFragment loginModule() {
        return new LoginFragment();
    }

    public static void configureModule(LoginFragment fragment) {

        UserService masterService = UserService.getInstance((MainActivity) fragment.getActivity());
        LoginInteractor interactor = new LoginInteractor(masterService);
        LoginPresenter presenter = new LoginPresenter();
        LoginRouter router = new LoginRouter((MainActivity)fragment.getActivity());

        fragment.setViewOutput(presenter);

        presenter.setRouter(router);
        presenter.setInteractor(interactor);
        presenter.setViewInterface(fragment);

        interactor.setOutput(presenter);
    }
}
