package com.example.barbershop.modules.WorkMode.Router;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;
import com.example.barbershop.R;
import com.example.barbershop.modules.Master.Assembly.MasterAssembly;
import com.example.barbershop.modules.Master.View.MasterFragment;


public class WorkModeRouter implements WorkModeRouterInput {

    private FragmentActivity getActivity;

    public WorkModeRouter(FragmentActivity getActivity) {
        this.getActivity = getActivity;
    }

    @Override
    public void showMasterModuleForEdit(String ownerId) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentManager.popBackStack();

        MasterFragment fragment = MasterAssembly.masterModuleForEdit(ownerId);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void showEditAlert(String title, final EditCallBack callBack) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity, R.style.MyAlertDialogTheme);

        builder.setTitle(title)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callBack.done(true);
                    }
                })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                callBack.done(false);
                                dialog.cancel();
                            }
                        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void showInfoAlert(String title) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity, R.style.MyAlertDialogTheme);

        builder.setMessage(title).setPositiveButton("Ок", null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void closeModule() {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();

        if (fragmentManager != null) {
            fragmentManager.popBackStack();
        }
    }
}
