package com.example.barbershop.modules.MasterWritesList.Presenter;

import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;
import com.example.barbershop.modules.MasterWrite.Interactor.MasterWriteViewItem;
import com.example.barbershop.modules.MasterWritesList.Interactor.MasterWritesListInteractorInput;
import com.example.barbershop.modules.MasterWritesList.Interactor.MasterWritesListInteractorOutput;
import com.example.barbershop.modules.MasterWritesList.Router.MasterWritesListRouterInput;
import com.example.barbershop.modules.MasterWritesList.View.MasterWritesListViewOutput;
import com.example.barbershop.modules.MasterWritesList.View.MasterWritesListViewinterface;

import java.util.ArrayList;


public class MasterWritesListPresenter implements MasterWritesListViewOutput, MasterWritesListInteractorOutput {

    private MasterWritesListRouterInput router;
    private MasterWritesListViewinterface userInterface;
    private MasterWritesListInteractorInput interactor;

    public void setRouter(MasterWritesListRouterInput router) {
        this.router = router;
    }

    public void setInteractor(MasterWritesListInteractorInput interactor) {
        this.interactor = interactor;
    }

    public void setViewInterface(MasterWritesListViewinterface userInterface) {
        this.userInterface = userInterface;
    }

    //MasterWritesListViewOutput

    @Override
    public void viewDidStarted() {
        interactor.loadWrites();
    }

    @Override
    public void viewDidTapCreateNewWrite() {
        router.presentMasterWriteFragment(interactor.getOwnerId(), interactor.getIdToken(), new EditCallBack() {
            @Override
            public void done(boolean ok) {
                if (ok) {
                    interactor.loadWrites();
                }
            }
        });
    }

    @Override
    public void viewDidTapDeleteButton(final MasterWriteViewItem viewItem) {
        String title = "Удалить запись на " + viewItem.date + " на " + viewItem.time + "?";
        router.showEditAlert(title, new EditCallBack() {
            @Override
            public void done(boolean ok) {
                if (ok) interactor.deleteWrite(viewItem.id, viewItem.clientId);
            }

        });
    }

    @Override
    public void viewDidTapMasters() {
     router.presentMastersListFragment(interactor.isAdmin(), interactor.isMaster(), interactor.getIdToken());
    }

    @Override
    public void viewDidTapServicesButton() {
        router.presentServicesBarberListFragment(null, interactor.getIdToken(), false, false);
    }

    @Override
    public void viewDidTapMyProfile() {
      router.presentMasterFragment(interactor.getOwnerId(), interactor.getIdToken(), interactor.isAdmin());
    }

    @Override
    public void viewDidTapMyWrites() {
        router.presentMasterWritesList(interactor.getOwnerId(), interactor.getIdToken(), interactor.isAdmin(), interactor.isMaster());
    }

    //MasterWritesListInteractorOutput

    @Override
    public void interactorDidLoadViewItems(ArrayList<MasterWriteViewItem> viewItems) {
        userInterface.showViewItems(viewItems);
    }
}
