package com.example.barbershop.modules.MastersList.Interactor;


import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.Logic.MasterService.MastersCallback;
import com.example.barbershop.Logic.MasterService.Objects.Master;
import com.example.barbershop.modules.Master.Interactor.MasterViewItem;

import java.util.ArrayList;

import io.realm.RealmList;

public class MastersListInteractor implements MastersListInteractorInput {

    private MastersListInteractorOutput output;
    private MasterServiceInterface masterService;
    private String idToken;

    public MastersListInteractor(String idToken, MasterServiceInterface masterService) {
        this.idToken = idToken;
        this.masterService = masterService;
    }

    public void setOutput(MastersListInteractorOutput output) {
        this.output = output;
    }

    @Override
    public void loadMasters() {
        masterService.getMasters(idToken, new MastersCallback() {
            @Override
            public void done(Error error, RealmList<Master> masters, boolean isAdmin) {
                if (error == null && masters != null) {
                    ArrayList<MasterViewItem> viewItems = new ArrayList<>();

                    for (Master master : masters) {
                        MasterViewItem item = new MasterViewItem();

                        item.id = master.getId();
                        item.surname = master.getSurname();
                        item.name = master.getName();
                        item.fullName = master.getFullName();
                        item.src = master.getPhotoSrc();

                        viewItems.add(item);
                    }

                    output.interactorDidLoadViewItems(viewItems);

                } else {
                    //
                }
            }
        });
    }

    @Override
    public String getIdToken() {
        return idToken;
    }
}
