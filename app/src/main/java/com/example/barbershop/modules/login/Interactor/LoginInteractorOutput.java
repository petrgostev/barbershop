package com.example.barbershop.modules.login.Interactor;


public interface LoginInteractorOutput {
    void interactorDidAuthenticationClient(String id, String idToken);

    void interactorDidReceivedError(Error error);

    void interactorDidAuthenticationMaster(String id, String idToken, boolean isAdmin, boolean isMaster);
}
