package com.example.barbershop.modules.ServicesBarberList.Assembly;

import android.os.Bundle;

import com.example.barbershop.Logic.DataService.DataService;
import com.example.barbershop.Logic.DataService.DataServiceInterface;
import com.example.barbershop.Logic.DataService.Objects.Callback.SelectedObjectCallback;
import com.example.barbershop.Logic.MasterService.MasterService;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.modules.ServicesBarberList.Interactor.ServicesBarberListInteractor;
import com.example.barbershop.modules.ServicesBarberList.Presenter.ServicesBarberListPresenter;
import com.example.barbershop.modules.ServicesBarberList.Router.ServicesBarberListRouter;
import com.example.barbershop.modules.ServicesBarberList.View.ServicesBarberListFragment;


public class ServicesBarberListAssembly {

    private static String ID_TOKEN_KEY = "ID_TOKEN_KEY";
    private static String IS_ADMIN_KEY = "IS_ADMIN_KEY";
    private static String IS_MASTER_KEY = "IS_MASTER_KEY";
    private static String OWNER_ID_KEY = "OWNER_ID_KEY";
    private static String IS_FOR_WRITING = "IS_FOR_WRITING";

    public static ServicesBarberListFragment servicesBarberListModule(String ownerId, String idToken, boolean isAdmin, boolean isMaster) {
        ServicesBarberListFragment fragment = new ServicesBarberListFragment();

        Bundle args = new Bundle();

        args.putString(OWNER_ID_KEY, ownerId);
        args.putString(ID_TOKEN_KEY, idToken);
        args.putBoolean(IS_ADMIN_KEY, isAdmin);
        args.putBoolean(IS_MASTER_KEY, isMaster);

        fragment.setArguments(args);
        return fragment;
    }

    public static ServicesBarberListFragment servicesBarberListModuleForSelectService(String masterId, boolean isForWriting,
                                                                                      SelectedObjectCallback callback) {
        ServicesBarberListFragment fragment = new ServicesBarberListFragment();
        fragment.callback = callback;

        Bundle args = new Bundle();

        args.putString(OWNER_ID_KEY, masterId);
        args.putBoolean(IS_FOR_WRITING, isForWriting);

        fragment.setArguments(args);

        return fragment;
    }

    public static void configureModule(ServicesBarberListFragment fragment) {
        Bundle args = fragment.getArguments();

        String idToken = "";
        String masterId = "";
        boolean isAdmin = false;
        boolean isMaster = false;
        boolean isForWriting = false;

        if (args != null) {
            idToken = args.getString(ID_TOKEN_KEY);
            masterId = args.getString(OWNER_ID_KEY);
            isAdmin = args.getBoolean(IS_MASTER_KEY);
            isMaster = args.getBoolean(IS_ADMIN_KEY);
            isForWriting = args.getBoolean(IS_FOR_WRITING);
        }

        DataServiceInterface dataService = DataService.getInstance(fragment.getContext());
        MasterServiceInterface masterService = MasterService.getInstance(fragment.getContext());

        ServicesBarberListInteractor interactor = new ServicesBarberListInteractor(idToken, isAdmin, isMaster,  masterId, dataService, masterService);
        ServicesBarberListPresenter presenter = new ServicesBarberListPresenter();
        ServicesBarberListRouter router = new ServicesBarberListRouter(fragment.getActivity());

        fragment.setViewOutput(presenter);

        presenter.setRouter(router);
        presenter.setInteractor(interactor);
        presenter.setViewInterface(fragment);

        presenter.selectedCallback = fragment.callback;

        if (isForWriting){
            presenter.configureForWriting();
        }

        if (masterId != null) {
            presenter.configureForShowMasterServicesBarber();
        }

        interactor.setOutput(presenter);
    }
}
