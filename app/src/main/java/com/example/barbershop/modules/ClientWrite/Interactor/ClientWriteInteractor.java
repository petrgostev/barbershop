package com.example.barbershop.modules.ClientWrite.Interactor;

import com.example.barbershop.Logic.ClientService.Objects.Callback.ClientCallback;
import com.example.barbershop.Logic.ClientService.ClientServiceInterface;
import com.example.barbershop.Logic.ClientService.Objects.Client;
import com.example.barbershop.Logic.DataService.DataServiceInterface;
import com.example.barbershop.Logic.DataService.Objects.Callback.WriteCallback;
import com.example.barbershop.Logic.MasterService.Objects.ServiceBarber;
import com.example.barbershop.Logic.DataService.Objects.Write;
import com.example.barbershop.Logic.MasterService.MasterCallback;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.Logic.MasterService.Objects.Master;
import com.example.barbershop.modules.Master.Interactor.MasterViewItem;
import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberViewItem;

import java.util.ArrayList;
import java.util.Date;

import io.realm.RealmList;


public class ClientWriteInteractor implements ClientWriteInteractorInput {

    private ClientWriteInteractorOutput output;

    private ClientServiceInterface clientService;
    private MasterServiceInterface masterService;
    private DataServiceInterface dataService;

    private String idClient;
    private String idMaster;
    private String idToken;
    private String serviceId;
    private String serviceName = "";
    private Date date;

    private Write write;

    private MasterViewItem selectedMasterItem;
    private ServiceBarberViewItem selectedServiceItem;
    private Date selectedDate;

    public ClientWriteInteractor(String idClient, String serviceId,  String idToken, ClientServiceInterface clientService,
                                 MasterServiceInterface masterService, DataServiceInterface dataService) {
        this.clientService = clientService;
        this.masterService = masterService;
        this.dataService = dataService;
        this.idClient = idClient;
        this.serviceId = serviceId;
        this.idToken = idToken;
    }

    public void setOutput(ClientWriteInteractorOutput output) {
        this.output = output;
    }

    @Override
    public void updateFieldMaster(final String masterId) {

        masterService.getMasterById(masterId, idToken, new MasterCallback() {
            @Override
            public void done(Error error, Master master) {
                if (error == null && master != null) {
                    idMaster = masterId;
                    write.setMasterName(master.getFullName());
                } else {
                    //
                }
            }
        });
    }

    @Override
    public void updateFieldDate(final Date date) {
        if (date.getTime() < new Date().getTime()) {
            output.interactorDidError("Нельзя выбирать прошедшую дату!");

        } else {
            this.date = date;

            masterService.getMasterById(idMaster, idToken, new MasterCallback() {
                @Override
                public void done(Error error, Master master) {
                    if (error == null && master != null) {
//                        master.getWorkMode().setDate(date);
                        write.setDate(date);


                    } else {
                        //error
                    }
                }
            });
        }
    }

    @Override
    public void saveWrite() {
        write.setMasterId(idMaster);

        clientService.getClientById(idClient, idToken, new ClientCallback() {
            @Override
            public void done(Error error, Client client) {
                if (error == null && client != null) {
                    client.addWriteId(write.getId());
                    write.setClientName(client.getFullName());

                    clientService.updateClient(client, new ClientCallback() {
                        @Override
                        public void done(Error error, Client client) {
                            if (error == null) {

                                masterService.getMasterById(idMaster, idToken, new MasterCallback() {
                                    @Override
                                    public void done(Error error, Master master) {
                                        if (error == null && master != null) {
//                                            master.addWriteId(write.getId());

                                            masterService.updateMaster(master, new MasterCallback() {
                                                @Override
                                                public void done(Error error, Master master) {
                                                    if (error == null) {

                                                        dataService.saveWrite(write, idToken, new WriteCallback() {
                                                            @Override
                                                            public void done(Error error, Write write) {
                                                                output.interactorDidSaveWrite();

                                                            }
                                                        });

                                                    } else {
//                                                                    error
                                                    }
                                                }
                                            });

                                        } else {
//                                                        error
                                        }
                                    }
                                });

                            } else {
//                                            error
                            }
                        }
                    });

                } else {
//                                error
                }
            }
        });
    }

    @Override
    public ArrayList<MasterViewItem> getMasters() {
        ArrayList<MasterViewItem> masterViewItems = new ArrayList<MasterViewItem>();
        RealmList<Master> loadedMasters = masterService.getLoadedMasters();

        if (loadedMasters != null) {
            for (Master master : loadedMasters) {
                MasterViewItem item = new MasterViewItem();

                item.id = master.getId();
                item.surname = master.getSurname();
                item.name = master.getName();
                item.fullName = master.getFullName();
                item.src = master.getPhotoSrc();


                item.services = new ArrayList<>();

                if (master.services != null) {

                    for (ServiceBarber service : master.services) {
                        ServiceBarberViewItem serviceItem = new ServiceBarberViewItem();

                        if (service.getPrice() == 0) {
                            serviceItem.price = "";
                        } else {
                            serviceItem.price = String.valueOf(service.getPrice());
                        }

                        if (service.getTimeInterval() == 0)  {
                            serviceItem.timeInterval = "";
                        } else  {
                            serviceItem.timeInterval = String.valueOf(service.getTimeInterval());
                        }

                        serviceItem.id = service.getId();
                        serviceItem.name = service.getName();

                        item.services.add(serviceItem);
                    }
                }

                masterViewItems.add(item);
            }
        }

        return masterViewItems;
    }

    @Override
    public void setSelectedMaster(MasterViewItem item) {
        selectedMasterItem = item;
    }

    @Override
    public void setSelectedService(ServiceBarberViewItem item) {
        selectedServiceItem = item;
    }

    @Override
    public void setDay(Date date) {
        selectedDate = date;
    }

    @Override
    public Date getSelectedDay() {
        return selectedDate;
    }

    @Override
    public void getTimes(TimesCallback callback) {
        ArrayList<WriteTimeItem> items = new ArrayList<>();

        WriteTimeItem item1 = new WriteTimeItem();
        item1.timeText = "10 : 00";
        item1.isBusy = true;
        items.add(item1);

        WriteTimeItem item2 = new WriteTimeItem();
        item2.timeText = "10 : 30";
        item2.isBusy = false;
        items.add(item2);

        WriteTimeItem item3 = new WriteTimeItem();
        item3.timeText = "11 : 00";
        item3.isBusy = false;
        items.add(item3);

        WriteTimeItem item4 = new WriteTimeItem();
        item4.timeText = "11 : 30";
        item4.isBusy = false;
        items.add(item4);

        WriteTimeItem item5 = new WriteTimeItem();
        item5.timeText = "12 : 00";
        item5.isBusy = true;
        items.add(item5);

        WriteTimeItem item6 = new WriteTimeItem();
        item6.timeText = "12 : 30";
        item6.isBusy = false;
        items.add(item6);

        WriteTimeItem item7 = new WriteTimeItem();
        item7.timeText = "13 : 00";
        item7.isBusy = false;
        items.add(item7);

        WriteTimeItem item8 = new WriteTimeItem();
        item8.timeText = "13 : 30";
        item8.isBusy = false;
        items.add(item8);

        WriteTimeItem item9 = new WriteTimeItem();
        item9.timeText = "14 : 00";
        item9.isBusy = true;
        items.add(item9);

        WriteTimeItem item10 = new WriteTimeItem();
        item10.timeText = "14 : 30";
        item10.isBusy = false;
        items.add(item10);

        callback.loadTimeItems(items);
    }
}
