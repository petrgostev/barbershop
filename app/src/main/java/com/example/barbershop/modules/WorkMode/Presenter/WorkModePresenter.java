package com.example.barbershop.modules.WorkMode.Presenter;

import android.text.Editable;

import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;
import com.example.barbershop.modules.WorkMode.Interactor.WorkModeInteractorInput;
import com.example.barbershop.modules.WorkMode.Interactor.WorkModeInteractorOutput;
import com.example.barbershop.modules.WorkMode.Interactor.WorkModeViewItem;
import com.example.barbershop.modules.WorkMode.Router.WorkModeRouterInput;
import com.example.barbershop.modules.WorkMode.View.WorkModeViewInterface;
import com.example.barbershop.modules.WorkMode.View.WorkModeViewoutput;


public class WorkModePresenter implements WorkModeInteractorOutput, WorkModeViewoutput {

    private WorkModeInteractorInput interactor;
    private WorkModeRouterInput router;
    private WorkModeViewInterface userInterface;

    private boolean isForCreate = false;

    public void setRouter(WorkModeRouterInput router) {
        this.router = router;
    }

    public void setInteractor(WorkModeInteractorInput interactor) {
        this.interactor = interactor;
    }

    public void setViewInterface(WorkModeViewInterface userInterface) {
        this.userInterface = userInterface;
    }

    public void configureForCreate() {
        isForCreate = true;
    }

    //WorkModeViewoutput

    @Override
    public void viewDidStarted() {
        if (isForCreate) {
            interactor.createWorkMode();

        } else {
            interactor.loadWorkMode();
        }
    }

    @Override
    public void viewDidSaveButton(final Editable startWork, final Editable endWork, final Editable startTimeout,
                                  final Editable endTimeout, final Editable weekend) {

        router.showEditAlert("Сохранить режим работы?", new EditCallBack() {
            @Override
            public void done(boolean ok) {
                if (ok) {
                    interactor.saveWorkMode(startWork, endWork, startTimeout, endTimeout, weekend);
                }
            }
        });
    }

    //WorkModeInteractorOutput

    @Override
    public void ineractorDidSaveWorkMode() {
//        router.showMasterModuleForEdit(interactor.getOwnerId());
        router.closeModule();
    }

    @Override
    public void ineractorDidLoadViewItem(WorkModeViewItem viewItem) {
        userInterface.showViewItem(viewItem);
    }

    @Override
    public void interactorDidError(String title) {
        router.showInfoAlert(title);
    }
}
