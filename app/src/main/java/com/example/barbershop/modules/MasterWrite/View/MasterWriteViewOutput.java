package com.example.barbershop.modules.MasterWrite.View;

import com.example.barbershop.modules.MasterWrite.Interactor.MasterWriteViewItem;


public interface MasterWriteViewOutput {
    void viewDidStarted();

    void viewDidTapClientEdit();

    void viewDidTapSaveButton(MasterWriteViewItem viewItem);

    void viewDidTapTimeEdit(MasterWriteViewItem viewItem);

    void viewDidTapDateEdit(MasterWriteViewItem viewItem);

    void viewDidTapServiceEdit(MasterWriteViewItem viewItem);
}
