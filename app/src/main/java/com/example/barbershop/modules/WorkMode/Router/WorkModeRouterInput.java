package com.example.barbershop.modules.WorkMode.Router;

import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;

public interface WorkModeRouterInput {

    void showEditAlert(String title, EditCallBack editCallBack);

    void showInfoAlert(String title);

    void showMasterModuleForEdit(String ownerId);

    void closeModule();
}
