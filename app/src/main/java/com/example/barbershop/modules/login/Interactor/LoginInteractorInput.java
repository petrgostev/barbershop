package com.example.barbershop.modules.login.Interactor;


public interface LoginInteractorInput {
    void authenticationUser(String login, String password);
}
