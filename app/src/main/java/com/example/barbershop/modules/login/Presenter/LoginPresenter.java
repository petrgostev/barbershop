package com.example.barbershop.modules.login.Presenter;

import com.example.barbershop.modules.login.Interactor.LoginInteractorInput;
import com.example.barbershop.modules.login.Interactor.LoginInteractorOutput;
import com.example.barbershop.modules.login.Router.LoginRouterInput;
import com.example.barbershop.modules.login.View.LoginViewInterface;
import com.example.barbershop.modules.login.View.LoginViewOutput;


public class LoginPresenter implements LoginViewOutput, LoginInteractorOutput {

    private LoginRouterInput router;
    private LoginInteractorInput interactor;
    private LoginViewInterface userInterface;

    public void setRouter(LoginRouterInput router) {
        this.router = router;
    }

    public void setInteractor(LoginInteractorInput interactor) {
        this.interactor = interactor;
    }

    public void setViewInterface(LoginViewInterface userInterface) {
        this.userInterface = userInterface;
    }

    //LoginViewOutput

    @Override
    public void didTapauthenticationButton(String login, String password) {
        interactor.authenticationUser(login, password);
        userInterface.hideKeyboard();
    }

    @Override
    public void didTapRegisterButton() {
        router.presentClientFragment();
//        router.presentMasterFragment();
        userInterface.hideKeyboard();
    }

    //LoginInteractorOutput

    @Override
    public void interactorDidAuthenticationMaster(String id, String idToken, boolean isAdmin, boolean isMaster) {
        router.presentMasterWritesList(id, idToken, isAdmin, isMaster);
    }

    @Override
    public void interactorDidAuthenticationClient(String id, String idToken) {
        router.presentClientWritesList(id, idToken);
    }

    @Override
    public void interactorDidReceivedError(Error error) {
        router.showInfoAlert(error);
    }
}
