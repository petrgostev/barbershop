package com.example.barbershop.modules.MastersList.View;


import com.example.barbershop.modules.Master.Interactor.MasterViewItem;

public interface MastersListViewOutput {
    void viewDidStarted();

    void viewDidTapCreateNewMaster();

    void viewDidSelectItem(MasterViewItem viewItem);
}
