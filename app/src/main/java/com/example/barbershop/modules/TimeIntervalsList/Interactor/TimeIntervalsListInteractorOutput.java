package com.example.barbershop.modules.TimeIntervalsList.Interactor;

import java.util.ArrayList;


public interface TimeIntervalsListInteractorOutput {

    void interactorDidLoadViewItems(ArrayList<TimeIntervalViewItem> viewItems, String date);
}
