package com.example.barbershop.modules.Master.View;


public interface MasterViewOutput {
    void viewDidStarted();

    void viewDidTapRegistrationButton(String surname, String name, String patronymic, String phone,
                                      String email, String password, String workingHours);

    void viewDidTapDeleteButton();

    void viewDidTapSaveButton(String surname, String name, String patronymic, String phone,
                              String email, String password, String workingHours);

    void viewDidTapExitButton();

    void viewDidTapAddPhotoButtonButton();

    void viewDidTapReplacePhotoButton();

    void viewDidTapWorkMode(String workMode);

    void viewDidTapServicesCardViewForBrowse();

    void viewNeedsUpdateData(String surname, String name, String patronymic, String phone, String email, String password);

    void viewDidTapInfoButton();
}
