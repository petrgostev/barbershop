package com.example.barbershop.modules.MastersList.Interactor;


public interface MastersListInteractorInput {
    void loadMasters();

    String getIdToken();
}
