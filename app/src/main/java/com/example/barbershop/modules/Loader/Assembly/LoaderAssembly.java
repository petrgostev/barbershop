package com.example.barbershop.modules.Loader.Assembly;

import com.example.barbershop.Logic.UserService.UserService;
import com.example.barbershop.MainActivity;
import com.example.barbershop.modules.Loader.Interactor.LoaderInteractor;
import com.example.barbershop.modules.Loader.Presenter.LoaderPresenter;
import com.example.barbershop.modules.Loader.Router.LoaderRouter;
import com.example.barbershop.modules.Loader.View.LoaderFragment;


public class LoaderAssembly {

    public static LoaderFragment loaderModule() {
        return new LoaderFragment();
    }

    public static void configureModule(LoaderFragment fragment) {

        UserService masterService = UserService.getInstance(fragment.getActivity());
        LoaderInteractor interactor = new LoaderInteractor(masterService);
        LoaderPresenter presenter = new LoaderPresenter();
        LoaderRouter router = new LoaderRouter(fragment.getActivity());

        fragment.setViewOutput(presenter);

        presenter.setRouter(router);
        presenter.setInteractor(interactor);

        interactor.setOutput(presenter);
    }
}
