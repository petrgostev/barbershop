package com.example.barbershop.modules.Client.Router;

import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;


public interface ClientRouterInput {

    void showEditAlert(String title, EditCallBack deleteCallBack);

    void presentLoginModule();

    void presentCreatePhoto(String id, String idToken);

    void showInfoAlert(String title);
}
