package com.example.barbershop.modules.ClientsForChoice.Presenter;

import com.example.barbershop.Logic.DataService.Objects.Callback.SelectedObjectCallback;
import com.example.barbershop.modules.Client.Interactor.ClientViewItem;
import com.example.barbershop.modules.ClientsForChoice.Interactor.ClientsForChoiceInteractorInput;
import com.example.barbershop.modules.ClientsForChoice.Interactor.ClientsForChoiceInteractorOutput;
import com.example.barbershop.modules.ClientsForChoice.Router.ClientsForChoiceRouterInput;
import com.example.barbershop.modules.ClientsForChoice.View.ClientsForChoiceViewInterface;
import com.example.barbershop.modules.ClientsForChoice.View.ClientsForChoiceViewOutput;

import java.util.ArrayList;


public class ClientsForChoicePresenter implements ClientsForChoiceViewOutput, ClientsForChoiceInteractorOutput {

    public SelectedObjectCallback selectedCallback;

    private ClientsForChoiceRouterInput router;
    private ClientsForChoiceViewInterface userInterface;
    private ClientsForChoiceInteractorInput interactor;

    public void setRouter(ClientsForChoiceRouterInput router) {
        this.router = router;
    }

    public void setInteractor(ClientsForChoiceInteractorInput interactor) {
        this.interactor = interactor;
    }

    public void setViewInterface(ClientsForChoiceViewInterface userInterface) {
        this.userInterface = userInterface;
    }

    //ClientsForChoiceViewOutput

    @Override
    public void viewDidTapSearch(String query) {
        interactor.loadClients(query);
    }

    @Override
    public void viewDidSelectItem(ClientViewItem viewItem) {
        if (selectedCallback != null) {
            selectedCallback.didSelect(viewItem.id);
            router.closeModule();
        }
    }

    //ClientsForChoiceInteractorOutput

    @Override
    public void interactorDidLoadViewItems(ArrayList<ClientViewItem> viewItems) {
        userInterface.showViewItems(viewItems);
    }

    @Override
    public void interactorDidErrorLoadClients() {
        router.showInfoAlert("Клиент не найден!");
    }
}
