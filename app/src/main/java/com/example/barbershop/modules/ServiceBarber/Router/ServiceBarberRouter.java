package com.example.barbershop.modules.ServiceBarber.Router;

import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;

import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;
import com.example.barbershop.R;
import com.example.barbershop.modules.ServicesBarberList.Assembly.ServicesBarberListAssembly;

public class ServiceBarberRouter implements ServiceBarberRouterInput {

    private FragmentActivity getActivity;

    public ServiceBarberRouter(FragmentActivity getActivity) {
        this.getActivity = getActivity;
    }

    @Override
    public void showEditAlert(String title, final EditCallBack callBack) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity);

        builder.setTitle(title)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callBack.done(true);
                    }
                })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                callBack.done(false);
                                dialog.cancel();
                            }
                        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void showInfoAlert(String error) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity);

        builder.setTitle(error.toString())
                .setCancelable(false)
                .setNegativeButton("ОК",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void presentServicesBarberListFragment(String masterId, String idToken, boolean isAdmin, boolean isMaster) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = ServicesBarberListAssembly.servicesBarberListModule(masterId, idToken, isAdmin, isMaster);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void closeModule() {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();

        if (fragmentManager != null) {
            fragmentManager.popBackStack();
        }
    }
}
