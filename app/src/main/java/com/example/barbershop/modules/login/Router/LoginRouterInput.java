package com.example.barbershop.modules.login.Router;


public interface LoginRouterInput {

    void presentClientFragment();

    void showInfoAlert(Error error);

    void presentMasterFragment();

    void presentMasterWritesList(String id, String idToken, boolean isAdmin, boolean isMaster);

    void presentClientWritesList(String id, String idToken);
}
