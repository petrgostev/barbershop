package com.example.barbershop.modules.ServicesBarberList.Interactor;


public interface ServicesBarberListInteractorInput {

    void loadServicesBarber();

    boolean isAdmin();

    String getOwnerId();

    void addServiceToMaster(String serviceId);

    boolean isMaster();
}
