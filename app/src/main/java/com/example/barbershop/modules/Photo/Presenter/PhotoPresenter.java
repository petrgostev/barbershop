package com.example.barbershop.modules.Photo.Presenter;

import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;
import com.example.barbershop.modules.Photo.Interactor.PhotoInteractorInput;
import com.example.barbershop.modules.Photo.Interactor.PhotoInteractorOutput;
import com.example.barbershop.modules.Photo.Interactor.PhotoViewItem;
import com.example.barbershop.modules.Photo.Router.PhotoRouterInput;
import com.example.barbershop.modules.Photo.View.PhotoViewInterface;
import com.example.barbershop.modules.Photo.View.PhotoViewOutput;


public class PhotoPresenter implements PhotoViewOutput, PhotoInteractorOutput {

    private PhotoRouterInput router;
    private PhotoInteractorInput interactor;
    private PhotoViewInterface userInterface;

    private Boolean editOn;
    private Boolean isForCreate;

    public void setRouter(PhotoRouterInput router) {
        this.router = router;
    }

    public void setInteractor(PhotoInteractorInput interactor) {
        this.interactor = interactor;
    }

    public void setViewInterface(PhotoViewInterface userInterface) {
        this.userInterface = userInterface;
    }

    public void configureForCreate() {
        editOn = true;
        isForCreate = true;
        interactor.createPhoto();
    }

    public void configureForEdit() {
        editOn = false;
        isForCreate = false;
    }

    //PhotoViewOutput

    @Override
    public void viewDidStarted() {
        userInterface.showEditButton();

//        if (interactor.canDelete()) {
//            userInterface.showDeleteButton();
//        }

        if (isForCreate) {
//            userInterface.setEditButtonsVisibility(true);
//            userInterface.hideDeleteButton();
        }

        if (!editOn) {
            interactor.load();
        } else {
            interactor.loadForEdit();
        }
    }

    @Override
    public void viewDidTapSaveButton() {
        router.showEditAlert("Сохранить фото?", new EditCallBack() {
            @Override
            public void done(boolean ok) {
                if (ok) {
                    interactor.savePhoto();
                }
            }
        });
    }

    @Override
    public void viewDidTapDeleteButton() {
        router.showEditAlert("Удалить фото?", new EditCallBack() {
            @Override
            public void done(boolean ok) {
                if (ok) {
                    interactor.deletePhoto();
                }
            }
        });
    }

    //PhotoInteractorOutput

    @Override
    public void interactorDidLoadViewItem(PhotoViewItem viewItem) {
        userInterface.loadViewItem(viewItem);
    }

    @Override
    public void interactorDidSavePhoto() {
        router.closeModule();
    }
}
