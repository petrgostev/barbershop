package com.example.barbershop.modules.Loader.Interactor;

import com.example.barbershop.Logic.UserService.UserCallback;
import com.example.barbershop.Logic.UserService.UserServiceInterface;


public class LoaderInteractor implements LoaderInteractorInput {

    private LoaderInteractorOutput output;
    private UserServiceInterface userService;

    public void setOutput(LoaderInteractorOutput output) {
        this.output = output;
    }

    public LoaderInteractor(UserServiceInterface userService) {
        this.userService = userService;
    }

    @Override
    public void loadUser() {

        userService.authenticationSavedUser(new UserCallback() {
            @Override
            public void done(Error error, String id, String idToken, boolean isAdmin, boolean isMaster) {
                if (error == null ) {

                    if (isMaster) {
                        output.interactorDidAuthenticationMaster(id, idToken, isAdmin, isMaster);
                    } else {
                        output.interactorDidAuthenticationClient(id, idToken);
                    }

                } else {
                    output.interactorNeedsPresentLogin();
                }
            }
        });
    }
}
