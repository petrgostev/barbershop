package com.example.barbershop.modules.MasterWritesList.View;

import com.example.barbershop.modules.MasterWrite.Interactor.MasterWriteViewItem;

import java.util.ArrayList;

public interface MasterWritesListViewinterface {
    void showViewItems(ArrayList<MasterWriteViewItem> viewItems);
}
