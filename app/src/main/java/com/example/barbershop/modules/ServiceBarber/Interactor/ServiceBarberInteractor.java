package com.example.barbershop.modules.ServiceBarber.Interactor;

import com.example.barbershop.Logic.DataService.DataServiceInterface;
import com.example.barbershop.Logic.DataService.Objects.Callback.ServiceBarberCallback;
import com.example.barbershop.Logic.MasterService.Objects.ServiceBarber;
import com.example.barbershop.Logic.MasterService.MasterCallback;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.Logic.MasterService.Objects.Master;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;


public class ServiceBarberInteractor implements ServiceBarberInteractorInput {

    private ServiceBarberInteractorOutput output;

    private String idToken;
    private String id;
    private String masterId;
    private boolean isAdmin;
    private boolean isMaster;

    private ServiceBarber serviceBarber;

    private MasterServiceInterface masterService;
    private DataServiceInterface dataService;

    public ServiceBarberInteractor(String id, String masterId, String idToken, boolean isAdmin, boolean isMaster,
                                   MasterServiceInterface masterService, DataServiceInterface dataService) {
        this.idToken = idToken;
        this.id = id;
        this.masterId = masterId;
        this.isAdmin = isAdmin;
        this.isMaster = isMaster;
        this.masterService = masterService;
        this.dataService = dataService;
    }

    public void setOutput(ServiceBarberInteractorOutput output) {
        this.output = output;
    }

    @Override
    public void createServiceBarber() {
        id = UUID.randomUUID().toString();

        serviceBarber = new ServiceBarber(id, null, 0, 0 );
    }

    @Override
    public void loadServiceBarber() {

        if (serviceBarber != null) {
            loadViewItem(serviceBarber);
            return;
        }

        dataService.getServiceBarberById(id, idToken, new ServiceBarberCallback() {
            @Override
            public void done(Error error, ServiceBarber serviceBarber) {
                if (error == null && serviceBarber != null) {
                    ServiceBarberInteractor.this.serviceBarber = serviceBarber;
                    loadViewItem(serviceBarber);

                } else {
                    //error
                }
            }
        });
    }

    @Override
    public void saveServiceBarber(String name, String price, String timeInterval) {

        if (name.equals("") || price.equals("") || timeInterval.equals("")) {
            output.interactorDidReceivedError("Все поля должны быть заполнены!");
        }

        double priceDouble = Double.parseDouble(price);
        double timeIntervalDouble = Double.parseDouble(timeInterval);

        serviceBarber.setName(name);
        serviceBarber.setPrice(priceDouble);
        serviceBarber.setTimeInterval(timeIntervalDouble);

        dataService.saveServiceBarber(serviceBarber, new ServiceBarberCallback() {
            @Override
            public void done(Error error, ServiceBarber serviceBarber) {
                if (error == null) {
                    output.interactorDidSaveServiceBarber();
                }
            }
        });
    }

    @Override
    public void updateServiceBarber(String name, String price, String timeInterval) {
        if (name.equals("") || price.equals("") || timeInterval.equals("")) {
            output.interactorDidReceivedError("Все поля должны быть заполнены!");
        }

        double priceDouble = Double.parseDouble(price);
        double timeIntervalDouble = Double.parseDouble(timeInterval);

        serviceBarber.setName(name);
        serviceBarber.setPrice(priceDouble);
        serviceBarber.setTimeInterval(timeIntervalDouble);

        dataService.updateServiceBarber(serviceBarber, new ServiceBarberCallback() {
            @Override
            public void done(Error error, ServiceBarber serviceBarber) {
                if (error == null && serviceBarber != null) {
                    output.interactorDidUpdateServiceBarber();

                } else {
                    //error
                }
            }
        });
    }

    @Override
    public void deleteServiceBarber() {

        dataService.deleteServiceBarber(id, new ServiceBarberCallback() {
            @Override
            public void done(Error error, ServiceBarber serviceBarber) {
                if (error == null) {
                    output.interactorDidDeleteServiceBarber();

                } else {
//                    error
                }
            }
        });
    }

    @Override
    public void deleteServiceBarberFromMaster() {

        masterService.getMasterById(masterId, idToken, new MasterCallback() {
            @Override
            public void done(Error error, Master master) {
                if (error == null && master != null) {
//                    master.deleteServiceBarber(id);

                    masterService.updateMaster(master, new MasterCallback() {
                        @Override
                        public void done(Error error, Master master) {
                            if (error == null && master != null) {

                                output.interactorDidDeleteServiceBarberFromMaster();

                            } else {
                                //error
                            }
                        }
                    });

                } else {
                    //error
                }
            }
        });
    }

    @Override
    public String getIdToken() {
        return idToken;
    }

    @Override
    public String getMasterId() {
        return masterId;
    }

    @Override
    public boolean isAdmin() {
        return isAdmin;
    }

    @Override
    public boolean isMaster() {
        return isMaster;
    }

    //Private

    private void loadViewItem(ServiceBarber serviceBarber) {

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
        String pattern = "#0.00";
        DecimalFormat decimalFormat = new DecimalFormat(pattern, otherSymbols);

        ServiceBarberViewItem viewItem = new ServiceBarberViewItem();

        viewItem.id = serviceBarber.getId();
        viewItem.name = serviceBarber.getName();

        if (serviceBarber.getPrice() == 0) viewItem.price = "";
        else viewItem.price = decimalFormat.format(serviceBarber.getPrice());

        if (serviceBarber.getTimeInterval() == 0) viewItem.timeInterval = "";
        else
            viewItem.timeInterval = decimalFormat.format(serviceBarber.getTimeInterval());

        output.interactorDidLoadViewItem(viewItem);
    }
}
