package com.example.barbershop.modules.ClientWritesList.Router;

import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;
import com.example.barbershop.R;
import com.example.barbershop.modules.Client.Assembly.ClientAssembly;
import com.example.barbershop.modules.Client.View.ClientFragment;
import com.example.barbershop.modules.ClientWrite.Assembly.ClientWriteAssembly;
import com.example.barbershop.modules.ClientWrite.View.ClientWriteFragment;
import com.example.barbershop.modules.ClientWritesList.Assembly.ClientWritesListAssembly;
import com.example.barbershop.modules.ClientWritesList.View.ClientWritesListFragment;
import com.example.barbershop.modules.MastersList.Assembly.MastersListAssembly;
import com.example.barbershop.modules.MastersList.View.MastersListFragment;
import com.example.barbershop.modules.ServicesBarberList.Assembly.ServicesBarberListAssembly;

public class ClientWritesListRouter implements ClientWritesListRouterInput {

    private FragmentActivity getActivity;

    public ClientWritesListRouter(FragmentActivity getActivity) {
        this.getActivity = getActivity;
    }

    @Override
    public void presentClientWriteFragment(String ownerId, String idToken) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        ClientWriteFragment fragment = ClientWriteAssembly.clientWriteModule(ownerId, null,  idToken);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void presentClientFragment(String ownerId, String idToken) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        ClientFragment fragment = ClientAssembly.clientModuleMyProfile(ownerId, idToken);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void presentClientWritesListFragment(String ownerId, String idToken) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        ClientWritesListFragment fragment = ClientWritesListAssembly.MyWritesListModule(ownerId, idToken);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void presentMastersListFragment(boolean isAdmin, boolean isMaster, String idToken) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        MastersListFragment fragment = MastersListAssembly.MastersListModule(isAdmin, isMaster, idToken);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void presentServicesBarberListFragment(String id, String idToken, boolean isAdmin, boolean isMaster) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = ServicesBarberListAssembly.servicesBarberListModule(id, idToken, isAdmin, isMaster);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void showEditAlert(String title, final EditCallBack callBack) {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity);

        builder.setTitle(title)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callBack.done(true);
                    }
                })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                callBack.done(false);
                                dialog.cancel();
                            }
                        });

        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }
}
