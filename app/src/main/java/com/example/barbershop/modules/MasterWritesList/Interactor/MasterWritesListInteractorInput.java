package com.example.barbershop.modules.MasterWritesList.Interactor;

public interface MasterWritesListInteractorInput {
    void loadWrites();

    String getOwnerId();

    String getIdToken();

    void deleteWrite(String id, String masterId);

    boolean isAdmin();

    boolean isMaster();
}
