package com.example.barbershop.modules.WorkMode.View;

import android.text.Editable;


public interface WorkModeViewoutput {
    void viewDidStarted();

    void viewDidSaveButton(Editable startWork, Editable endWork, Editable startTimeout,
                           Editable endTimeout, Editable weekend);
}
