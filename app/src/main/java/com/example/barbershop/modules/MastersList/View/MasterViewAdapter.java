package com.example.barbershop.modules.MastersList.View;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.barbershop.Logic.FileService.FileService;
import com.example.barbershop.R;
import com.example.barbershop.modules.Master.Interactor.MasterViewItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MasterViewAdapter extends RecyclerView.Adapter<MasterViewAdapter.ViewHolder> {
    Context context;

    private ArrayList<MasterViewItem> viewItems;
    private FileService fileService = FileService.getInstance(context);

    MasterViewAdapter.Listener selectListener;

    public MasterViewAdapter(Context context, ArrayList<MasterViewItem> viewItems) {
        this.viewItems = viewItems;
        this.context = context;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        ViewHolder(CardView cardView) {
            super(cardView);
            this.cardView = cardView;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        CardView cv = (CardView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.master_card, viewGroup, false);

        return new ViewHolder(cv);
    }

    @Override
    public void onBindViewHolder(@NonNull MasterViewAdapter.ViewHolder viewHolder, int position) {
        CardView cardView = viewHolder.cardView;

        configureView(cardView, position, viewHolder);
    }

    private void configureView(CardView cardView, int position, final ViewHolder viewHolder) {
        final MasterViewItem item = viewItems.get(position);

        final TextView fullnameTV = cardView.findViewById(R.id.fullname);
        final TextView specializationTV = cardView.findViewById(R.id.specialization);
        final ImageView photoIV = cardView.findViewById(R.id.photo);

        fullnameTV.setText(item.surname + " " + item.name);
        photoIV.setImageResource(R.drawable.nophoto);

        if (!item.src.equals("")) {
            Picasso.with(photoIV.getContext())
                    .load(item.src)
                    .error(R.drawable.nophoto)
                    .placeholder(R.drawable.nophoto)
                    .into(photoIV);
        }

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectListener != null) {
                    selectListener.onClick(viewHolder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return viewItems.size();
    }

    interface Listener {
        void onClick(int position);
    }

    public void setSelectListener(Listener selectListener) {
        this.selectListener = selectListener;
    }
}
