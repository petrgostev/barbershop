package com.example.barbershop.modules.Info;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.example.barbershop.R;

import java.util.Objects;


public class InfoFragment extends Fragment {

    private TextView infoTextView;

    MenuItem itemEdit;
    MenuItem itemAdd;
    MenuItem itemSave;
    MenuItem itemDelete;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);

        Toolbar toolbar = Objects.requireNonNull(getActivity()).findViewById(R.id.toolbar);
        TextView title = Objects.requireNonNull(getActivity()).findViewById(R.id.titleView);

        infoTextView = view.findViewById(R.id.info_text_view);

        title.setText("О нас");

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        inflater.inflate(R.menu.menu_main, menu);
        itemDelete = menu.findItem(R.id.delete);
        itemSave = menu.findItem(R.id.save);
        itemEdit = menu.findItem(R.id.edit);
        itemAdd = menu.findItem(R.id.add);

        itemEdit.setVisible(false);
        itemSave.setVisible(false);
        itemAdd.setVisible(false);
        itemDelete.setVisible(false);

        super.onCreateOptionsMenu(menu, inflater);
    }
}
