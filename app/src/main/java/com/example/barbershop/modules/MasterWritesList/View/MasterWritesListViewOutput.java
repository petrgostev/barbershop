package com.example.barbershop.modules.MasterWritesList.View;

import com.example.barbershop.modules.MasterWrite.Interactor.MasterWriteViewItem;

public interface MasterWritesListViewOutput {
    void viewDidStarted();

    void viewDidTapCreateNewWrite();

    void viewDidTapDeleteButton(MasterWriteViewItem viewItem);

    void viewDidTapMasters();

    void viewDidTapServicesButton();

    void viewDidTapMyProfile();

    void viewDidTapMyWrites();
}
