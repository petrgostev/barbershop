package com.example.barbershop.modules.Photo.Interactor;

import android.graphics.Bitmap;


public class PhotoViewItem {
    public String id;
    public String src;
    public Bitmap bitmap;
}
