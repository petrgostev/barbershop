package com.example.barbershop.modules.MasterWritesList.Interactor;


import android.os.Build;
import android.support.annotation.RequiresApi;

import com.example.barbershop.Logic.ClientService.ClientServiceInterface;
import com.example.barbershop.Logic.ClientService.Objects.Callback.ClientCallback;
import com.example.barbershop.Logic.ClientService.Objects.Client;
import com.example.barbershop.Logic.DataService.DataServiceInterface;
import com.example.barbershop.Logic.DataService.Objects.Callback.ServiceBarberCallback;
import com.example.barbershop.Logic.DataService.Objects.Callback.WriteCallback;
import com.example.barbershop.Logic.DataService.Objects.Callback.WritesCallback;
import com.example.barbershop.Logic.MasterService.Objects.ServiceBarber;
import com.example.barbershop.Logic.DataService.Objects.Write;
import com.example.barbershop.Logic.MasterService.MasterCallback;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.Logic.MasterService.Objects.Master;
import com.example.barbershop.modules.MasterWrite.Interactor.MasterWriteViewItem;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;


public class MasterWritesListInteractor implements MasterWritesListInteractorInput {

    private MasterWritesListInteractorOutput output;

    private String ownerId;
    private String idToken;
    private boolean isAdmin;
    private boolean isMaster;

    private ClientServiceInterface clientService;
    private MasterServiceInterface masterService;
    private DataServiceInterface dataService;

    public MasterWritesListInteractor(String ownerId, String idToken, boolean isAdmin, boolean isMaster,
                                      ClientServiceInterface clientService, MasterServiceInterface masterService, DataServiceInterface dataService) {
        this.ownerId = ownerId;
        this.idToken = idToken;
        this.isAdmin = isAdmin;
        this.isMaster = isMaster;
        this.clientService = clientService;
        this.masterService = masterService;
        this.dataService = dataService;
    }

    public void setOutput(MasterWritesListInteractorOutput output) {
        this.output = output;
    }

    @Override
    public void loadWrites() {
        dataService.getMasterWrites(ownerId, idToken, new WritesCallback() {
            @Override
            public void done(Error error, ArrayList<Write> writes) {
                if (error == null && writes != null) {

                    loadViewItems(writes);

                } else {
                    //error
                }
            }
        });
    }

    @Override
    public void deleteWrite(final String id, final String clientId) {

        dataService.deleteWrite(id, new WriteCallback() {
            @Override
            public void done(Error error, Write write) {
                if (error == null) {

                    masterService.getMasterById(ownerId, idToken, new MasterCallback() {
                        @Override
                        public void done(Error error, Master master) {
                            if (error == null && master != null) {
//                                master.deleteWriteId(id);

                                masterService.updateMaster(master, new MasterCallback() {
                                    @Override
                                    public void done(Error error, Master master) {
                                        if (error == null && master != null) {

                                            clientService.getClientById(clientId, idToken, new ClientCallback() {
                                                @Override
                                                public void done(Error error, Client client) {
                                                    if (error == null && client != null) {
                                                        client.deleteWriteId(id);

                                                        clientService.updateClient(client, new ClientCallback() {
                                                            @Override
                                                            public void done(Error error, Client client) {
                                                                if (error == null && client != null) {

                                                                    loadWrites();

                                                                } else {
                                                                    //error
                                                                }
                                                            }
                                                        });

                                                    } else {
                                                        //error
                                                    }
                                                }
                                            });

                                        } else {
                                            //error
                                        }
                                    }
                                });

                            } else {
                                //error
                            }
                        }
                    });

                } else {
                    //error
                }
            }
        });
    }

    @Override
    public String getOwnerId() {
        return ownerId;
    }

    @Override
    public String getIdToken() {
        return idToken;
    }

    @Override
    public boolean isAdmin() {
        return isAdmin;
    }

    @Override
    public boolean isMaster() {
        return isMaster;
    }

    //Private

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void loadViewItems(final ArrayList<Write> myWrites) {

        final ArrayList<MasterWriteViewItem> viewItems = new ArrayList<>();

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
        String pattern = "#00.00";
        final DecimalFormat decimalFormat = new DecimalFormat(pattern, otherSymbols);

            for (int i = 0; i <myWrites.size() ; i++) {

                final MasterWriteViewItem viewItem = new MasterWriteViewItem();
                final Write write = myWrites.get(i);
                final int count = i;
                dataService.getServiceBarberById(Objects.requireNonNull(write).getServiceBarberId(), idToken, new ServiceBarberCallback() {
                    @Override
                    public void done(Error error, ServiceBarber serviceBarber) {
                        if (error == null && serviceBarber != null) {

                            if (write.getDate() == null) viewItem.date = "";
                            else
                                viewItem.date = new SimpleDateFormat("d.MM.yyyy", Locale.getDefault()).format(write.getDate());

                            viewItem.client = write.getClientName();
                            viewItem.clientId = write.getClientId();
                            viewItem.masterId = write.getMasterId();
                            viewItem.id = write.getId();
                            viewItem.service = serviceBarber.getName();
                            viewItem.price = decimalFormat.format(write.getPrise());

                            String timeStart = decimalFormat.format(write.getTimeStart());
                            String timeEnd = decimalFormat.format(write.getTimeEnd());
                            viewItem.time = timeStart + " - " + timeEnd;

                            if (write.getDate().after(new Date()) || write.getDate().equals(new Date())) {
                                viewItems.add(viewItem);
                            }

                            if (count == myWrites.size() - 1) {
                                output.interactorDidLoadViewItems(viewItems);
                                return;
                            }
                        } else {
                            //error
                        }
                    }
                });
            }
        }

}
