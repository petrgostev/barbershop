package com.example.barbershop.modules.ServiceBarber.Interactor;

public interface ServiceBarberInteractorInput {
    void createServiceBarber();

    void saveServiceBarber(String name, String price, String timeInterval);

    void loadServiceBarber();

    void updateServiceBarber(String name, String price, String timeInterval);

    String getIdToken();

    boolean isAdmin();

    void deleteServiceBarber();

    void deleteServiceBarberFromMaster();

    String getMasterId();

    boolean isMaster();
}
