package com.example.barbershop.modules.ServiceBarber.View;

public interface ServiceBarberViewOutput {
    void viewDidStarted();

    void viewDidTapSaveButton(String name, String price, String timeInterval);

    void viewDidTapUpdateButton(String name, String price, String timeInterval);

    void viewDidTapDeleteServiceBarber();

    void viewDidTapDeleteServiceBarberFromMaster();
}
