package com.example.barbershop.modules.ClientsForChoice.Interactor;

import com.example.barbershop.modules.Client.Interactor.ClientViewItem;

import java.util.ArrayList;

public interface ClientsForChoiceInteractorOutput {
    void interactorDidLoadViewItems(ArrayList<ClientViewItem> viewItems);

    void interactorDidErrorLoadClients();
}
