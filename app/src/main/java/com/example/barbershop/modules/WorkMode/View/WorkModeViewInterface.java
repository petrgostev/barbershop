package com.example.barbershop.modules.WorkMode.View;

import com.example.barbershop.modules.WorkMode.Interactor.WorkModeViewItem;


public interface WorkModeViewInterface {

    void showViewItem(WorkModeViewItem viewItem);
}
