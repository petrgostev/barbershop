package com.example.barbershop.modules.Photo.Router;

import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;

import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;

public class PhotoRouter implements PhotoRouterInput {

    private FragmentActivity getActivity;

    public PhotoRouter(FragmentActivity getActivity) {
        this.getActivity = getActivity;
    }

    @Override
    public void showEditAlert(String title, final EditCallBack callBack) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity);

        builder.setTitle(title)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callBack.done(true);
                    }
                })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                callBack.done(false);
                                dialog.cancel();
                            }
                        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void closeModule() {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();

        if (fragmentManager != null) {
            fragmentManager.popBackStack();
        }
    }
}
