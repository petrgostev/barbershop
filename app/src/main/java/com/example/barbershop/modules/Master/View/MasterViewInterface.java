package com.example.barbershop.modules.Master.View;

import com.example.barbershop.modules.Master.Interactor.MasterViewItem;


public interface MasterViewInterface {

    void showMasterViewItem(MasterViewItem viewItem, boolean isForMyProfile, boolean isForCreate);

    void setEditMode(boolean edit, boolean isForCreate);
}
