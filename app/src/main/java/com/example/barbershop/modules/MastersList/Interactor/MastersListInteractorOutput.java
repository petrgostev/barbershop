package com.example.barbershop.modules.MastersList.Interactor;

import com.example.barbershop.modules.Master.Interactor.MasterViewItem;

import java.util.ArrayList;


public interface MastersListInteractorOutput {
    void interactorDidLoadViewItems(ArrayList<MasterViewItem> viewItems);
}
