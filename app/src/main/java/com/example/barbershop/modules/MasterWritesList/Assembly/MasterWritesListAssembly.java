package com.example.barbershop.modules.MasterWritesList.Assembly;

import android.os.Bundle;

import com.example.barbershop.Logic.ClientService.ClientService;
import com.example.barbershop.Logic.ClientService.ClientServiceInterface;
import com.example.barbershop.Logic.DataService.DataService;
import com.example.barbershop.Logic.DataService.DataServiceInterface;
import com.example.barbershop.Logic.MasterService.MasterService;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.modules.MasterWritesList.Interactor.MasterWritesListInteractor;
import com.example.barbershop.modules.MasterWritesList.Presenter.MasterWritesListPresenter;
import com.example.barbershop.modules.MasterWritesList.Router.MasterWritesListRouter;
import com.example.barbershop.modules.MasterWritesList.View.MasterWritesListFragment;


public class MasterWritesListAssembly {

    private static String ID_TOKEN_KEY = "ID_TOKEN_KEY";
    private static String OWNER_ID_KEY = "OWNER_ID_KEY";
    private static String IS_ADMIN_KEY = "IS_ADMIN_KEY";
    private static String IS_MASTER_KEY = "IS_MASTER_KEY";

    public static MasterWritesListFragment MyWritesListModule(String ownerId, String idToken, boolean isAdmin, boolean isMaster) {

        MasterWritesListFragment fragment = new MasterWritesListFragment();

        Bundle args = new Bundle();

        args.putString(OWNER_ID_KEY, ownerId);
        args.putString(ID_TOKEN_KEY, idToken);
        args.putBoolean(IS_ADMIN_KEY, isAdmin);
        args.putBoolean(IS_MASTER_KEY, isMaster);

        fragment.setArguments(args);
        return fragment;
    }

    public static void configureModule(MasterWritesListFragment fragment) {
        Bundle args = fragment.getArguments();

        String ownerId = "";
        String idToken = "";
        boolean isAdmin = false;
        boolean isMaster = false;

        if (args != null) {
            ownerId = args.getString(OWNER_ID_KEY);
            idToken = args.getString(ID_TOKEN_KEY);
            isAdmin = args.getBoolean(IS_ADMIN_KEY);
            isMaster = args.getBoolean(IS_MASTER_KEY);
        }

        ClientServiceInterface clientService = ClientService.getInstance(fragment.getContext());
        MasterServiceInterface masterService = MasterService.getInstance(fragment.getContext());
        DataServiceInterface dataService = DataService.getInstance(fragment.getContext());

        MasterWritesListInteractor interactor = new MasterWritesListInteractor(ownerId, idToken, isAdmin, isMaster,
                clientService, masterService, dataService);
        MasterWritesListPresenter presenter = new MasterWritesListPresenter();
        MasterWritesListRouter router = new MasterWritesListRouter(fragment.getActivity());

        fragment.setViewOutput(presenter);

        presenter.setRouter(router);
        presenter.setInteractor(interactor);
        presenter.setViewInterface(fragment);

        interactor.setOutput(presenter);
    }
}
