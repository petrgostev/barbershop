package com.example.barbershop.modules.ServicesBarberList.Presenter;

import com.example.barbershop.Logic.DataService.Objects.Callback.SelectedObjectCallback;
import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberViewItem;
import com.example.barbershop.modules.ServicesBarberList.Interactor.ServicesBarberListInteractorInput;
import com.example.barbershop.modules.ServicesBarberList.Interactor.ServicesBarberListInteractorOutput;
import com.example.barbershop.modules.ServicesBarberList.Router.ServicesBarberListRouterInput;
import com.example.barbershop.modules.ServicesBarberList.View.ServicesBarberListViewInterface;
import com.example.barbershop.modules.ServicesBarberList.View.ServicesBarberListViewOutput;

import java.util.ArrayList;


public class ServicesBarberListPresenter implements ServicesBarberListViewOutput, ServicesBarberListInteractorOutput {

    public SelectedObjectCallback selectedCallback;

    private ServicesBarberListViewInterface userInterface;
    private ServicesBarberListInteractorInput interactor;
    private ServicesBarberListRouterInput router;

    private boolean isForShowMasterServicesBarber = false;
    private boolean isForWriting = false;

    public void setRouter(ServicesBarberListRouterInput router) {
        this.router = router;
    }

    public void setInteractor(ServicesBarberListInteractorInput interactor) {
        this.interactor = interactor;
    }

    public void setViewInterface(ServicesBarberListViewInterface userInterface) {
        this.userInterface = userInterface;
    }

    public void configureForShowMasterServicesBarber() {
        isForShowMasterServicesBarber = true;
    }

    public void configureForWriting() {
        isForWriting = true;
    }

    //ServicesBarberListViewOutput

    @Override
    public void viewDidStarted() {
        interactor.loadServicesBarber();
    }

    @Override
    public void viewDidTapAddServiceBarber() {

//        if (!isForShowMasterServicesBarber) {
//            router.showServiceBarberModule(null, null, interactor.getIdToken(),
//                    interactor.isAdmin(), interactor.isMaster(), true, false);
//
//        } else {
//            interactor.loadServicesBarberForChose(new SelectedObjectCallback() {
//                @Override
//                public void didSelect(final String serviceId) {
//                    if (serviceId != null) {
//
//                        router.showEditAlert("Добавить услугу?", new EditCallBack() {
//                            @Override
//                            public void done(boolean ok) {
//
//                                if (ok) {
//                                    interactor.addServiceToMaster(serviceId);
//
//                                } else {
//                                    router.closeModule();
//                                }
//                            }
//                        });
//                    }
//                }
//            });
//        }
    }

    @Override
    public void viewDidTapAddWrite(final String serviceId) {
//        router.showEditAlert("Создать запись?", new EditCallBack() {
//            @Override
//            public void done(boolean ok) {
//                if (ok) {
//                    router.presentClientWriteFragment(interactor.getOwnerId(), serviceId, interactor.getIdToken());
//                }
//            }
//        });
    }

    @Override
    public void viewDidSelectItem(ServiceBarberViewItem viewItem) {
//        if (selectedCallback != null) {
//            selectedCallback.didSelect(viewItem.id);
//
//            if (isForWriting) router.closeModule();
//
//        } else {
//            router.showServiceBarberModule(viewItem.id, interactor.getOwnerId(), interactor.getIdToken(),
//                    interactor.isAdmin(), interactor.isMaster(), false, isForShowMasterServicesBarber);
//        }
    }

    //ServicesBarberListInteractorOutput

    @Override
    public void interactorDidLoadViewItems(ArrayList<ServiceBarberViewItem> viewItems) {
        userInterface.showViewItems(viewItems);
    }

    @Override
    public void interactorDidAddServiceBarberToMaster() {
    }

    @Override
    public void interactorDidError(String error) {
        router.showInfoAlert(error);
    }
}
