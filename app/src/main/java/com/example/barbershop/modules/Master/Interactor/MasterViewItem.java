package com.example.barbershop.modules.Master.Interactor;


import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberViewItem;

import java.util.ArrayList;

public class MasterViewItem {
    public String id;
    public String name;
    public String fullName;
    public String surname;
    public String phone;
    public String email;
    public String password;
    public String src;
    public String workMode;
    public boolean isAdmin;
    public boolean isMaster;

    public ArrayList<ServiceBarberViewItem> services;
}
