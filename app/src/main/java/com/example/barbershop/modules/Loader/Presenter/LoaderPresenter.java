package com.example.barbershop.modules.Loader.Presenter;

import com.example.barbershop.modules.Loader.Interactor.LoaderInteractorInput;
import com.example.barbershop.modules.Loader.Interactor.LoaderInteractorOutput;
import com.example.barbershop.modules.Loader.Router.LoaderRouterInput;
import com.example.barbershop.modules.Loader.View.LoaderViewOutput;


public class LoaderPresenter implements LoaderViewOutput, LoaderInteractorOutput {

    private LoaderRouterInput router;
    private LoaderInteractorInput interactor;

    public void setRouter(LoaderRouterInput router) {
        this.router = router;
    }

    public void setInteractor(LoaderInteractorInput interactor) {
        this.interactor = interactor;
    }

    //LoaderViewOutput

    @Override
    public void viewDidStarted() {
        interactor.loadUser();
    }

    //LoaderInteractorOutput

    @Override
    public void interactorDidAuthenticationMaster(String id, String idToken, boolean isAdmin, boolean isMaster) {
        router.presentMasterWritesList(id, idToken, isAdmin, isMaster);
    }

    @Override
    public void interactorDidAuthenticationClient(String id, String idToken) {
        router.presentClientWritesList(id, idToken);
    }

    @Override
    public void interactorNeedsPresentLogin() {
        router.presentLogin();
    }
}
