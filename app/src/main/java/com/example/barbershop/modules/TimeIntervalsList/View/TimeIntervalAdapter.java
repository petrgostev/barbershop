package com.example.barbershop.modules.TimeIntervalsList.View;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.barbershop.R;
import com.example.barbershop.modules.TimeIntervalsList.Interactor.TimeIntervalViewItem;

import java.util.ArrayList;


public class TimeIntervalAdapter extends RecyclerView.Adapter<TimeIntervalAdapter.ViewHolder> {

    private ArrayList<TimeIntervalViewItem> viewItems;

    private TimeIntervalAdapter.Listener selectListener;

    public TimeIntervalAdapter(Context context, ArrayList<TimeIntervalViewItem> viewItems) {
        this.viewItems = viewItems;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        ViewHolder(CardView cardView) {
            super(cardView);
            this.cardView = cardView;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        CardView cv = (CardView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.time_interval_card, viewGroup, false);

        return new ViewHolder(cv);
    }

    @Override
    public void onBindViewHolder(@NonNull TimeIntervalAdapter.ViewHolder viewHolder, int position) {
        CardView cardView = viewHolder.cardView;

        configureView(cardView, position, viewHolder);
    }

    private void configureView(CardView cardView, int position, final ViewHolder viewHolder) {
        final TimeIntervalViewItem item = viewItems.get(position);

        TextView startTV = cardView.findViewById(R.id.start);
        TextView endTV = cardView.findViewById(R.id.end);

        startTV.setText(item.start);
        endTV.setText(item.end);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectListener != null) {
                    selectListener.onClick(viewHolder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return viewItems.size();
    }

    interface Listener {
        void onClick(int position);
    }

    public void setSelectListener(Listener selectListener) {
        this.selectListener = selectListener;
    }
}
