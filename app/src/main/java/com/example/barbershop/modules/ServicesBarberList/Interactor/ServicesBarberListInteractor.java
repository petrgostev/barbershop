package com.example.barbershop.modules.ServicesBarberList.Interactor;

import com.example.barbershop.Logic.DataService.DataServiceInterface;
import com.example.barbershop.Logic.MasterService.Objects.ServiceBarber;
import com.example.barbershop.Logic.MasterService.MasterCallback;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.Logic.MasterService.Objects.Master;
import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberViewItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import io.realm.RealmList;


public class ServicesBarberListInteractor implements ServicesBarberListInteractorInput {

    private ServicesBarberListInteractorOutput output;

    private String idToken;
    private String ownerId;
    private boolean isAdmin;
    private boolean isMaster;

    ArrayList<ServiceBarberViewItem> viewItems;

    private DataServiceInterface dataService;
    private MasterServiceInterface masterService;

    public ServicesBarberListInteractor(String idToken, boolean isAdmin, boolean isMaster, String ownerId,
                                        DataServiceInterface dataService, MasterServiceInterface masterService) {
        this.idToken = idToken;
        this.ownerId = ownerId;
        this.isAdmin = isAdmin;
        this.isMaster = isMaster;
        this.dataService = dataService;
        this.masterService = masterService;
    }

    public void setOutput(ServicesBarberListInteractorOutput output) {
        this.output = output;
    }

    @Override
    public void loadServicesBarber() {

        HashMap<String, ServiceBarberViewItem> services = new HashMap<>();

        RealmList<Master> loadedMasters = masterService.getLoadedMasters();

        if (loadedMasters != null) {
            for (Master master : loadedMasters) {

                if (master.services != null) {

                    for (ServiceBarber service : master.services) {
                        ServiceBarberViewItem serviceItem = new ServiceBarberViewItem();

                        if (service.getPrice() == 0) {
                            serviceItem.price = "";
                        } else {
                            serviceItem.price = String.valueOf(service.getPrice());
                        }

                        if (service.getTimeInterval() == 0)  {
                            serviceItem.timeInterval = "";
                        } else  {
                            serviceItem.timeInterval = String.valueOf(service.getTimeInterval());
                        }

                        serviceItem.id = service.getId();
                        serviceItem.name = service.getName();

                        services.put(serviceItem.name, serviceItem);
                    }
                }
            }
        }

        Collection<ServiceBarberViewItem> values = services.values();

        ArrayList<ServiceBarberViewItem> list = new ArrayList<>(values);

        output.interactorDidLoadViewItems(list);
    }


    @Override
    public void addServiceToMaster(final String serviceId) {

//        masterService.getMasterById(ownerId, idToken, new MasterCallback() {
//            @Override
//            public void done(Error error, Master master) {
//                if (error == null && master != null) {
//
//                    for (String serviceBarberId : master.getServiceBarberIds()) {
//                        if (serviceBarberId.equals(serviceId)) {
//                            output.interactorDidError("Такая услуга уже добавлена");
//                            return;
//                        }
//                    }
//
//                    master.addServiceBarber(serviceId);
//
//                    masterService.updateMaster(master, new MasterCallback() {
//                        @Override
//                        public void done(Error error, Master master) {
//                            if (error == null && master != null) {
//                                output.interactorDidAddServiceBarberToMaster();
//
//                            } else {
//                                //error
//                            }
//                        }
//                    });
//                } else {
//                    //error
//                }
//            }
//        });
    }

    @Override
    public boolean isAdmin() {
        return isAdmin;
    }

    @Override
    public boolean isMaster() {
        return isMaster;
    }

    @Override
    public String getOwnerId() {
        return ownerId;
    }
}
