package com.example.barbershop.modules.ClientWritesList.View;

import com.example.barbershop.modules.ClientWrite.Interactor.ClientWriteViewItem;

public interface ClientWritesListViewOutput {
    void viewDidTapCreateNewWrite();

    void viewDidStarted();

    void viewDidTapDeleteButton(ClientWriteViewItem viewItem);

    void viewDidTapMyProfile();

    void viewDidTapMyWrites();

    void viewDidTapMasters();

    void viewDidTapServices();
}
