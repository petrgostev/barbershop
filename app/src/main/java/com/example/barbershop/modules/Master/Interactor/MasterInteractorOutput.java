package com.example.barbershop.modules.Master.Interactor;


public interface MasterInteractorOutput {
    void interactorDidLoadViewItem(MasterViewItem viewItem);

    void interactorDidRegistrationNewMaster(String id, String idToken, boolean isAdmin, boolean isMaster);

    void interactorDidUpdateViewItem(MasterViewItem viewItem);

    void interactorDidReceivedError(Error error);

    void interactorDidDeleteMaster();

    void interactorDidLogoutMaster();

    void interactorDidDeletePhoto();

    void adminDidRegistrationNewMaster();
}
