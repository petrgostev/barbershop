package com.example.barbershop.modules.TimeIntervalsList.Router;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;


public class TimeIntervalsListRouter implements TimeIntervalsListRouterInput {

    private FragmentActivity getActivity;

    public TimeIntervalsListRouter(FragmentActivity getActivity) {
        this.getActivity = getActivity;
    }

    @Override
    public void closeModule() {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();

        if (fragmentManager != null) {
            fragmentManager.popBackStack();
        }
    }
}
