package com.example.barbershop.modules.Photo.View;

import com.example.barbershop.modules.Photo.Interactor.PhotoViewItem;

public interface PhotoViewInterface {
    void showEditButton();

    void loadViewItem(PhotoViewItem viewItem);
}
