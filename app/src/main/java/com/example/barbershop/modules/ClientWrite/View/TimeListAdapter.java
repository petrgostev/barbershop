package com.example.barbershop.modules.ClientWrite.View;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.barbershop.R;
import com.example.barbershop.modules.ClientWrite.Interactor.WriteTimeItem;

import java.util.ArrayList;


public class TimeListAdapter extends RecyclerView.Adapter<TimeListAdapter.ViewHolder> {

    private ArrayList<WriteTimeItem> viewItems;

    private Listener selectListener;
    private int selectedPosition = -1;

    public TimeListAdapter(Context context, ArrayList<WriteTimeItem> viewItems) {
        this.viewItems = viewItems;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout linearLayout;

        ViewHolder(LinearLayout linearLayout) {
            super(linearLayout);
            this.linearLayout = linearLayout;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LinearLayout linearLayout =
                (LinearLayout) LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.time_list_item, viewGroup, false);

        return new ViewHolder(linearLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        configureView(viewHolder.linearLayout, position, viewHolder);
    }

    private void configureView(final LinearLayout linearLayout, int position, final ViewHolder viewHolder) {
        final WriteTimeItem item = viewItems.get(position);

        final TextView checkedTextView = linearLayout.findViewById(R.id.text);
        final RadioButton radioButton = linearLayout.findViewById(R.id.radio);

        checkedTextView.setText(item.timeText);
        linearLayout.setEnabled(!item.isBusy);
        radioButton.setVisibility(item.isBusy ? View.GONE : View.VISIBLE);

        if (selectedPosition == position) {
            radioButton.setChecked(true);
            linearLayout.setSelected(true);
        } else {
            radioButton.setChecked(false);
            linearLayout.setSelected(false);
        }

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = viewHolder.getAdapterPosition();
                if (selectListener != null) {
                    selectListener.onClick(selectedPosition);
                }

                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return viewItems.size();
    }

    public interface Listener {
        void onClick(int position);
    }

    public void setSelectListener(Listener selectListener) {
        this.selectListener = selectListener;
    }
}
