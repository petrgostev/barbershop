package com.example.barbershop.modules.Client.Presenter;

import com.example.barbershop.modules.Client.Interactor.ClientInteractorInput;
import com.example.barbershop.modules.Client.Interactor.ClientInteractorOutput;
import com.example.barbershop.modules.Client.Interactor.ClientViewItem;
import com.example.barbershop.modules.Client.Router.ClientRouterInput;
import com.example.barbershop.modules.Client.View.ClientViewInterface;
import com.example.barbershop.modules.Client.View.ClientViewOutput;
import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;


public class ClientPresenter implements ClientViewOutput, ClientInteractorOutput {

    private ClientRouterInput router;
    private ClientInteractorInput interactor;
    private ClientViewInterface userInterface;

    private boolean isForCreate;
    private boolean isForMyProfile;
    private boolean isForEdit;

    public void setRouter(ClientRouterInput router) {
        this.router = router;
    }

    public void setInteractor(ClientInteractorInput interactor) {
        this.interactor = interactor;
    }

    public void setViewInterface(ClientViewInterface userInterface) {
        this.userInterface = userInterface;
    }

    public void configureForCreate() {
        isForEdit = true;
        isForMyProfile = false;
        isForCreate = true;
    }

    public void configureForMyProfile() {
        isForEdit = false;
        isForMyProfile = true;
        isForCreate = false;
    }

    public void configureForEdit() {
        isForEdit = true;
        isForMyProfile = false;
        isForCreate = false;
    }

    //ClientViewOutput

    @Override
    public void viewDidStarted() {
        if (isForCreate) {
            interactor.createClient();
            userInterface.setEditMode(isForEdit, isForCreate);
        } else if (isForMyProfile) {
            interactor.loadMyProfile();
            userInterface.setEditMode(isForEdit, isForCreate);
        }
    }

    @Override
    public void viewDidTapSaveButton(final String surname, final String name,
                                     final String phone, final String email, final String password) {
        router.showEditAlert("Сохранить изменения?", new EditCallBack() {
            @Override
            public void done(boolean ok) {
                if (ok) {
                    interactor.updateClient(surname, name, phone, email, password);
                }
            }
        });
    }

    @Override
    public void viewDidTapDeleteButton() {
        router.showEditAlert("Пользователь будет удален!", new EditCallBack() {
            @Override
            public void done(boolean delete) {
                if (delete) {
                    interactor.deleteClient();
                }
            }
        });
    }

    @Override
    public void viewDidTapRegistrationButton(String surname, String name,
                                             String phone, String email, String password) {

        if (surname.isEmpty() || name.isEmpty() || email.isEmpty() || password.isEmpty()) {
            router.showInfoAlert("Поля: Фамилия, Имя, Email, Password - обязательны для заполнения!");

        } else {
            interactor.registrationClient(surname, name, phone, email, password);
        }
    }

    @Override
    public void viewDidTapExitButton() {
        router.showEditAlert("Выйти из аккаунта?", new EditCallBack() {
            @Override
            public void done(boolean ok) {
                if (ok) {
                    interactor.logoutClient();
                }
            }
        });
    }

    @Override
    public void viewDidTapAddPhotoButtonButton() {
        router.presentCreatePhoto(interactor.getId(), interactor.getIdToken());
    }

    @Override
    public void viewDidTapReplacePhotoButton() {
        router.showEditAlert("Заменить фото?", new EditCallBack() {
            @Override
            public void done(boolean ok) {
                if (ok) {
                    interactor.removePhoto();
                }
            }
        });
    }

    //ClientInteractorOutput

    @Override
    public void interactorDidRegistrationNewClient(String id, String idToken) {

    }

    @Override
    public void interactorDidLoadViewItem(ClientViewItem viewItem) {
        userInterface.showClientViewItem(viewItem);
        userInterface.setEditMode(isForEdit, isForCreate);
    }

    @Override
    public void interactorDidLogoutClient() {
        router.presentLoginModule();
    }

    @Override
    public void interactorDidDeletePhoto() {
        router.presentCreatePhoto(interactor.getId(), interactor.getIdToken());
    }
}
