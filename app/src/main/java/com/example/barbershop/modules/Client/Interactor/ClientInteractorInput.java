package com.example.barbershop.modules.Client.Interactor;


public interface ClientInteractorInput {

    void registrationClient(String surname, String name, String phone, String email, String password);

    void createClient();

    void loadMyProfile();

    void deleteClient();

    void logoutClient();

    String getId();

    String getIdToken();

    void removePhoto();

    void updateClient(String surname, String name, String phone, String email, String password);
}
