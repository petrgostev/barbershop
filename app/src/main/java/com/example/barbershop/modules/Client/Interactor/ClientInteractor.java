package com.example.barbershop.modules.Client.Interactor;

import com.example.barbershop.Logic.ClientService.Objects.Callback.ClientCallback;
import com.example.barbershop.Logic.ClientService.ClientServiceInterface;
import com.example.barbershop.Logic.ClientService.Objects.Client;
import com.example.barbershop.Logic.FileService.FileCallback;
import com.example.barbershop.Logic.FileService.FileServiceInterface;
import com.example.barbershop.Logic.FileService.Objects.Photo;
import com.example.barbershop.Logic.UserService.UserCallback;
import com.example.barbershop.Logic.UserService.UserServiceInterface;

public class ClientInteractor implements ClientInteractorInput {

    private String id;
    private String idToken;

    private ClientInteractorOutput output;
    private Client client;
    private ClientViewItem viewItem;

    private ClientServiceInterface clientService;
    private UserServiceInterface userService;
    private FileServiceInterface fileService;

    public ClientInteractor(String id, String idToken, ClientServiceInterface clientService,
                            UserServiceInterface userService, FileServiceInterface fileService) {
        this.id = id;
        this.idToken = idToken;
        this.clientService = clientService;
        this.userService = userService;
        this.fileService = fileService;
    }

    public void setOutput(ClientInteractorOutput output) {
        this.output = output;
    }

    @Override
    public void createClient() {
        client = new Client();
    }

    @Override
    public void registrationClient(final String surname, final String name, final String phone,
                                   final String email, final String password) {
        boolean isAdmin = false;

        userService.registrationNewUser(isAdmin, email, password, new UserCallback() {
            @Override
            public void done(Error error, final String id, final String idToken, boolean isAdmin, boolean isMaster) {

                if (error == null) {
                    clientService.registrationNewClient(idToken, id, surname, name, phone,
                            password, email, new ClientCallback() {
                                @Override
                                public void done(Error error, Client responseUser) {
                                    if (error == null && responseUser != null) {
                                        ClientInteractor.this.idToken = idToken;

                                        output.interactorDidRegistrationNewClient(id, idToken);

                                    } else {
                                        // output.interactorDidReceivedError(error);
                                    }
                                }
                            });
                } else {
                    // output.interactorDidReceivedError(error);
                }
            }
        });
    }

    @Override
    public void updateClient(String surname, String name, String phone, String email, String password) {
        client.setSurname(surname);
        client.setName(name);
        client.setPhone(phone);
        client.setEmail(email);
        client.setPassword(password);

        clientService.updateClient(client, new ClientCallback() {
            @Override
            public void done(Error error, Client client) {
                if (error == null && client != null) {
                    loadClientViewItem();

                } else {
                    //error
                }
            }
        });
    }

    @Override
    public void deleteClient() {

        userService.deleteUser(idToken, new UserCallback() {
            @Override
            public void done(Error error, String userId, String idToken, boolean isAdmin, boolean isMaster) {
                if (error == null) {

                    clientService.deleteClient(id, new ClientCallback() {
                        @Override
                        public void done(Error error, Client client) {
                            if (error == null) {

                                output.interactorDidLogoutClient();

                            } else {
                                //error
                            }
                        }
                    });

                } else {
                    //error
                }
            }
        });

    }

    @Override
    public void logoutClient() {
        userService.logoutUser(new UserCallback() {
            @Override
            public void done(Error error, String id, String idToken, boolean isAdmin, boolean isMaster) {

                if (error == null) {
                    output.interactorDidLogoutClient();

                } else {
                    //
                }
            }
        });
    }

    @Override
    public void removePhoto() {
        fileService.deletePhoto(id, client.getPhotoName(), new FileCallback() {
            @Override
            public void done(Error error, Photo photo) {
                if (error == null) {
                    client.removePhoto();

                    clientService.updateClient(client, new ClientCallback() {
                        @Override
                        public void done(Error error, Client client) {
                            if (error == null && client != null) {
                                output.interactorDidDeletePhoto();

                            } else {
//                                output.interactorDidReceivedError(error);
                            }
                        }
                    });
                } else {
//                    output.interactorDidReceivedError(error);
                }
            }
        });
    }

    @Override
    public void loadMyProfile() {
        clientService.getClientById(id, idToken, new ClientCallback() {
            @Override
            public void done(Error error, Client client) {
                if (error == null) {
                    ClientInteractor.this.client = client;
                    loadClientViewItem();
                } else {
                    //
                }
            }
        });
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getIdToken() {
        return idToken;
    }

    //Private

    private void loadClientViewItem() {
        viewItem = new ClientViewItem();

        viewItem.surname = client.getSurname();
        viewItem.name = client.getName();
        viewItem.fullName = client.getFullName();
        viewItem.email = client.getEmail();
        viewItem.password = client.getPassword();
        viewItem.patronymic = client.getPatronymic();
        viewItem.phone = client.getPhone();
        viewItem.src = client.getPhotoSrc();

        output.interactorDidLoadViewItem(viewItem);
    }
}
