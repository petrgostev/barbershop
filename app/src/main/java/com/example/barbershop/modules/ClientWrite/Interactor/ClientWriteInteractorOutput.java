package com.example.barbershop.modules.ClientWrite.Interactor;

public interface ClientWriteInteractorOutput {
    void interactorDidLoadViewItem(ClientWriteViewItem viewItem);

    void interactorDidSaveWrite();

    void interactorDidError(String error);
}
