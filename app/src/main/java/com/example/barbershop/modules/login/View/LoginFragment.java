package com.example.barbershop.modules.login.View;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.bottomappbar.BottomAppBar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.barbershop.R;
import com.example.barbershop.modules.login.Assembly.LoginAssembly;

import java.util.Objects;


public class LoginFragment extends Fragment implements LoginViewInterface {

    private LoginViewOutput viewOutput;

    private EditText loginEditText;
    private EditText passwordEditText;

    Toolbar toolbar;
    private BottomAppBar bottomAppBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LoginAssembly.configureModule(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_login, container, false);

        toolbar = Objects.requireNonNull(getActivity()).findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);

        bottomAppBar = Objects.requireNonNull(getActivity()).findViewById(R.id.bottom_app_bar);
        bottomAppBar.setVisibility(View.GONE);

        loginEditText = view.findViewById(R.id.loginEditText);
        passwordEditText = view.findViewById(R.id.passwordEditText);
        Button authenticationButton = view.findViewById(R.id.loginButton);
        Button registerButton = view.findViewById(R.id.registerButton);

        authenticationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loginEditText.getText().toString().isEmpty() || passwordEditText.getText().toString().isEmpty()) {
                    Toast.makeText(view.getContext(),
                            "Поля email и пароль не должны быть пустыми!",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                viewOutput.didTapauthenticationButton(loginEditText.getText().toString(), passwordEditText.getText().toString());
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewOutput.didTapRegisterButton();
            }
        });

        return view;
    }

    public void setViewOutput(LoginViewOutput viewOutput) {
        this.viewOutput = viewOutput;
    }

    @Override
    public void hideKeyboard() {
        if (loginEditText != null && getActivity() != null) {
            InputMethodManager imm =
                    (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(loginEditText.getWindowToken(), 0);
            }
        }
    }
}
