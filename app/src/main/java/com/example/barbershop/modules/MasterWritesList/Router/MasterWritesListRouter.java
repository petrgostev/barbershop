package com.example.barbershop.modules.MasterWritesList.Router;


import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;
import com.example.barbershop.R;
import com.example.barbershop.modules.Master.Assembly.MasterAssembly;
import com.example.barbershop.modules.Master.View.MasterFragment;
import com.example.barbershop.modules.MasterWrite.Assembly.MasterWriteAssembly;
import com.example.barbershop.modules.MasterWritesList.Assembly.MasterWritesListAssembly;
import com.example.barbershop.modules.MastersList.Assembly.MastersListAssembly;
import com.example.barbershop.modules.MastersList.View.MastersListFragment;
import com.example.barbershop.modules.ServicesBarberList.Assembly.ServicesBarberListAssembly;

public class MasterWritesListRouter implements MasterWritesListRouterInput {

    private FragmentActivity getActivity;

    public MasterWritesListRouter(FragmentActivity getActivity) {
        this.getActivity = getActivity;
    }

    @Override
    public void presentMasterWriteFragment(String ownerId, String idToken, EditCallBack callBack) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = MasterWriteAssembly.masterWriteModule(ownerId, idToken, callBack);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void presentMasterFragment(String id, String idToken, boolean isAdmin) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        MasterFragment fragment = MasterAssembly.masterModuleMyProfile(id, idToken, isAdmin);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void presentMastersListFragment(boolean isAdmin, boolean isMaster, String idToken) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        MastersListFragment fragment = MastersListAssembly.MastersListModule(isAdmin, isMaster, idToken);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void presentMasterWritesList(String id, String idToken, boolean isAdmin, boolean isMaster) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = MasterWritesListAssembly.MyWritesListModule(id, idToken, isAdmin, isMaster);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void presentServicesBarberListFragment(String id, String idToken, boolean isAdmin, boolean isMaster) {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = ServicesBarberListAssembly.servicesBarberListModule(id, idToken, isAdmin, isMaster);

        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void showEditAlert(String title, final EditCallBack callBack) {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity);

        builder.setTitle(title)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callBack.done(true);
                    }
                })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                callBack.done(false);
                                dialog.cancel();
                            }
                        });

        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }
}
