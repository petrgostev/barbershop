package com.example.barbershop.modules.TimeIntervalsList.View;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.barbershop.Logic.DataService.Objects.Callback.SelectedObjectCallback;
import com.example.barbershop.R;
import com.example.barbershop.modules.TimeIntervalsList.Assembly.TimeIntervalsListAssembly;
import com.example.barbershop.modules.TimeIntervalsList.Interactor.TimeIntervalViewItem;

import java.util.ArrayList;
import java.util.Objects;


public class TimeIntervalsListFragment extends Fragment implements TimeIntervalsListViewInterface {

    public SelectedObjectCallback callback;

    private TimeIntervalsListViewOutput viewOutput;

    private TimeIntervalAdapter adapter;

    private RecyclerView recycler;

    private TextView dateTV;

    private Toolbar toolbar;
    private MenuItem itemEdit;
    private MenuItem itemSave;
    private MenuItem itemDelete;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        TimeIntervalsListAssembly.configureModule(this);
    }

    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_time_interval, container, false);

        recycler = view.findViewById(R.id.recycler);

        TextView title = Objects.requireNonNull(getActivity()).findViewById(R.id.titleView);

        toolbar = Objects.requireNonNull(getActivity()).findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);

        dateTV = view.findViewById(R.id.date);

        title.setText("Время");
        return view;
    }

    public void setViewOutput(TimeIntervalsListViewOutput viewOutput) {
        this.viewOutput = viewOutput;
    }

    @Override
    public void onStart() {
        super.onStart();
        viewOutput.viewDidStarted();
    }

    @Override
    public void showViewItems(final ArrayList<TimeIntervalViewItem> viewItems, String date) {

        dateTV.setText(date);

        adapter = new TimeIntervalAdapter(getContext(), viewItems);
        recycler.setAdapter(adapter);
        LinearLayoutManager layoutManager = new GridLayoutManager(getContext(),3);
        recycler.setLayoutManager(layoutManager);

        adapter.setSelectListener(new TimeIntervalAdapter.Listener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(int position) {
                TimeIntervalViewItem viewItem = viewItems.get(position);

                viewOutput.viewDidSelectItem(viewItem);
            }
        });
    }
}