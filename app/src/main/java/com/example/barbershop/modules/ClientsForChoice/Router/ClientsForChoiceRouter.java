package com.example.barbershop.modules.ClientsForChoice.Router;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;

public class ClientsForChoiceRouter implements ClientsForChoiceRouterInput {

    private FragmentActivity getActivity;

    public ClientsForChoiceRouter(FragmentActivity getActivity) {
        this.getActivity = getActivity;
    }

    @Override
    public void closeModule() {
        FragmentManager fragmentManager = getActivity.getSupportFragmentManager();

        if (fragmentManager != null) {
            fragmentManager.popBackStack();
        }
    }

    @Override
    public void showInfoAlert(String title) {
        @SuppressLint("InlinedApi") final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);

        builder.setTitle("Внимание!")
                .setMessage(title)
                .setCancelable(false)
                .setNegativeButton("ОК",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
