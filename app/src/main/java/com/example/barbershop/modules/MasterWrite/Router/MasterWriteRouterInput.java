package com.example.barbershop.modules.MasterWrite.Router;

import com.example.barbershop.Logic.DataService.Objects.Callback.DateCallback;
import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;
import com.example.barbershop.Logic.DataService.Objects.Callback.SelectedObjectCallback;

public interface MasterWriteRouterInput {
    void showClientsForChoice(String idToken, SelectedObjectCallback callback);

    void closeModule();

    void showEditAlert(String s, EditCallBack callBack);

    void showTimesForChoice(String date, String masterId, String idToken, SelectedObjectCallback callback);

    void presentAlertDatePickerDialog(DateCallback dateCallback);

    void showServicesForChoice(String masterId, SelectedObjectCallback callback);

    void showInfoAlert(String error);
}
