package com.example.barbershop.modules.Client.View;

import com.example.barbershop.modules.Client.Interactor.ClientViewItem;


public interface ClientViewInterface {

    void setEditMode(boolean edit, boolean isForCreate);

    void showClientViewItem(ClientViewItem viewItem);
}
