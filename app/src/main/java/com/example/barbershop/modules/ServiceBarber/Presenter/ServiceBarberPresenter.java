package com.example.barbershop.modules.ServiceBarber.Presenter;

import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;
import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberInteractorInput;
import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberInteractorOutput;
import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberViewItem;
import com.example.barbershop.modules.ServiceBarber.Router.ServiceBarberRouterInput;
import com.example.barbershop.modules.ServiceBarber.View.ServiceBarberViewInterface;
import com.example.barbershop.modules.ServiceBarber.View.ServiceBarberViewOutput;


public class ServiceBarberPresenter implements ServiceBarberViewOutput, ServiceBarberInteractorOutput {

    private ServiceBarberViewInterface userInterface;
    private ServiceBarberInteractorInput interactor;
    private ServiceBarberRouterInput router;

    private boolean isEdit;

    public void setRouter(ServiceBarberRouterInput router) {
        this.router = router;
    }

    public void setInteractor(ServiceBarberInteractorInput interactor) {
        this.interactor = interactor;
    }

    public void setViewInterface(ServiceBarberViewInterface userInterface) {
        this.userInterface = userInterface;
    }

    public void configureForShow() {
        isEdit = false;
    }

    public void configureForEdit() {
        isEdit = true;
    }

    //ServiceBarberViewOutput

    @Override
    public void viewDidStarted() {

        userInterface.setEditMode(isEdit);

        if (isEdit) {
            interactor.createServiceBarber();

        }
        else{
            interactor.loadServiceBarber();
        }
    }

    @Override
    public void viewDidTapSaveButton(final String name, final String price, final String timeInterval) {
        router.showEditAlert("Сохранить услугу?", new EditCallBack() {
            @Override
            public void done(boolean ok) {
                if (ok) {
                    interactor.saveServiceBarber(name, price, timeInterval);
                }
            }
        });
    }

    @Override
    public void viewDidTapUpdateButton(final String name, final String price, final String timeInterval) {
        router.showEditAlert("Сохранить изменения?", new EditCallBack() {
            @Override
            public void done(boolean ok) {
                if (ok) {
                    interactor.updateServiceBarber(name, price, timeInterval);
                }
            }
        });
    }

    @Override
    public void viewDidTapDeleteServiceBarber() {
        router.showEditAlert("Удалить услугу?", new EditCallBack() {
            @Override
            public void done(boolean ok) {
                if (ok) {
                    interactor.deleteServiceBarber();
                }
            }
        });
    }

    @Override
    public void viewDidTapDeleteServiceBarberFromMaster() {
        router.showEditAlert("Удалить услугу у мастера?", new EditCallBack() {
            @Override
            public void done(boolean ok) {
                if (ok) {
                    interactor.deleteServiceBarberFromMaster();
                }
            }
        });
    }

    //ServiceBarberInteractorOutput

    @Override
    public void interactorDidLoadViewItem(ServiceBarberViewItem viewItem) {
        userInterface.showViewItem(viewItem);
    }

    @Override
    public void interactorDidReceivedError(String error) {
        router.showInfoAlert(error);
    }

    @Override
    public void interactorDidSaveServiceBarber() {
        router.presentServicesBarberListFragment(null, interactor.getIdToken(), interactor.isAdmin(), interactor.isMaster());
    }

    @Override
    public void interactorDidUpdateServiceBarber() {
        router.presentServicesBarberListFragment(null, interactor.getIdToken(), interactor.isAdmin(), interactor.isMaster());
    }

    @Override
    public void interactorDidDeleteServiceBarber() {
        router.presentServicesBarberListFragment(null, interactor.getIdToken(), interactor.isAdmin(), interactor.isMaster());
    }

    @Override
    public void interactorDidDeleteServiceBarberFromMaster() {
        router.closeModule();
    }
}
