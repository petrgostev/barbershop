package com.example.barbershop.modules.WorkMode.View;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.bottomappbar.BottomAppBar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.barbershop.Logic.DataService.Objects.Callback.WorkModeCallback;
import com.example.barbershop.R;
import com.example.barbershop.modules.WorkMode.Assembly.WorkModeAssembly;
import com.example.barbershop.modules.WorkMode.Interactor.WorkModeViewItem;

import java.util.Objects;


public class WorkModeFragment extends Fragment implements WorkModeViewInterface {

    public WorkModeCallback callback;

    private EditText startWorkET;
    private EditText endWorkET;
    private EditText startTimeoutET;
    private EditText endTimeoutET;
    private EditText weekendET;

    WorkModeViewoutput viewOutput;

    private BottomAppBar bottomAppBar;
    private Toolbar toolbar;

    MenuItem itemAdd;
    MenuItem itemEdit;
    MenuItem itemSave;
    MenuItem itemDelete;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        WorkModeAssembly.configureModule(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_work_mode, container, false);

        toolbar = Objects.requireNonNull(getActivity()).findViewById(R.id.toolbar);
        bottomAppBar = Objects.requireNonNull(getActivity()).findViewById(R.id.bottom_app_bar);
        bottomAppBar.setVisibility(View.GONE);

        TextView title = Objects.requireNonNull(getActivity()).findViewById(R.id.titleView);

        startWorkET = view.findViewById(R.id.start_work);
        endWorkET = view.findViewById(R.id.end_work);
        startTimeoutET = view.findViewById(R.id.start_timeout);
        endTimeoutET = view.findViewById(R.id.end_timeout);
        weekendET = view.findViewById(R.id.weekend);

        title.setText("Режим работы");

        return view;
    }

    public void setViewOutput(WorkModeViewoutput viewOutput) {
        this.viewOutput = viewOutput;
    }

    @Override
    public void onStart() {
        super.onStart();

        viewOutput.viewDidStarted();
    }

    @Override
    public void showViewItem(WorkModeViewItem viewItem) {
        startWorkET.setText(viewItem.startWork);
        endWorkET.setText(viewItem.endWork);
        startTimeoutET.setText(viewItem.startTimeout);
        endTimeoutET.setText(viewItem.endTimeout);
        weekendET.setText(viewItem.weekend);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        inflater.inflate(R.menu.menu_main, menu);
        itemAdd = menu.findItem(R.id.add);
        itemDelete = menu.findItem(R.id.delete);
        itemSave = menu.findItem(R.id.save);
        itemEdit = menu.findItem(R.id.edit);

        itemAdd.setVisible(false);
        itemDelete.setVisible(false);
        itemEdit.setVisible(false);
        itemSave.setVisible(true);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.save) {
//            itemSave.setVisible(false);

            viewOutput.viewDidSaveButton(startWorkET.getText(),
                    endWorkET.getText(), startTimeoutET.getText(),
                    endTimeoutET.getText(), weekendET.getText());
        }

        return super.onOptionsItemSelected(item);
    }
}
