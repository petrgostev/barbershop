package com.example.barbershop.modules.ClientWritesList.View;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.bottomappbar.BottomAppBar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.barbershop.R;
import com.example.barbershop.modules.ClientWrite.Interactor.ClientWriteViewItem;
import com.example.barbershop.modules.ClientWritesList.Assembly.ClientWritesListAssembly;

import java.util.ArrayList;
import java.util.Objects;


public class ClientWritesListFragment extends Fragment implements ClientWritesListViewInterface {

    private ClientWritesListViewOutput viewOutput;

    private ClientWriteAdapter clientWriteAdapter;

    private RecyclerView recycler;

    private Toolbar toolbar;
    private BottomAppBar bottomAppBar;

    private TextView myWritesTv;

    private ImageButton mastersButton;
    private ImageButton servicesButton;
    private ImageButton myProfileButton;
    private ImageButton myWritesButton;

    MenuItem itemAdd;
    MenuItem itemEdit;
    MenuItem itemSave;
    MenuItem itemDelete;

    ClientWriteViewItem viewItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        ClientWritesListAssembly.configureModule(this);
    }

    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        recycler = view.findViewById(R.id.recycler);

        toolbar = Objects.requireNonNull(getActivity()).findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);

        bottomAppBar = Objects.requireNonNull(getActivity()).findViewById(R.id.bottom_app_bar);
        bottomAppBar.setVisibility(View.VISIBLE);

        mastersButton = Objects.requireNonNull(getActivity()).findViewById(R.id.masters_button);
        servicesButton = Objects.requireNonNull(getActivity()).findViewById(R.id.services_button);
        myProfileButton = Objects.requireNonNull(getActivity()).findViewById(R.id.my_profile_button);
        myWritesButton = Objects.requireNonNull(getActivity()).findViewById(R.id.my_writes_button);

        myWritesTv = Objects.requireNonNull(getActivity()).findViewById(R.id.my_writes_tv);

        TextView title = Objects.requireNonNull(getActivity()).findViewById(R.id.titleView);

        selectAction();

        title.setText("Мои записи");
        return view;
    }

    public void setViewOutput(ClientWritesListViewOutput viewOutput) {
        this.viewOutput = viewOutput;
    }

    @Override
    public void onStart() {
        super.onStart();
        enableIcon(true);
        viewOutput.viewDidStarted();
    }

    @Override
    public void onPause() {
        super.onPause();
        enableIcon(false);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void showViewItems(final ArrayList<ClientWriteViewItem> viewItems) {
        if (itemAdd != null) {
            itemAdd.setVisible(true);
        }

        clientWriteAdapter = new ClientWriteAdapter(getContext(), viewItems);
        recycler.setAdapter(clientWriteAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycler.setLayoutManager(layoutManager);

        clientWriteAdapter.setSelectListener(new ClientWriteAdapter.Listener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(int position) {
                viewItem = viewItems.get(position);

                itemDelete.setVisible(true);
                itemAdd.setVisible(false);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        inflater.inflate(R.menu.menu_main, menu);
        itemDelete = menu.findItem(R.id.delete);
        itemSave = menu.findItem(R.id.save);
        itemEdit = menu.findItem(R.id.edit);
        itemAdd = menu.findItem(R.id.add);

        itemSave.setVisible(false);
        itemDelete.setVisible(false);
        itemEdit.setVisible(false);
        itemAdd.setVisible(true);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.delete) {
            itemDelete.setVisible(false);

            viewOutput.viewDidTapDeleteButton(viewItem);
        }

        if (id == R.id.add) {
            viewOutput.viewDidTapCreateNewWrite();
        }

        return super.onOptionsItemSelected(item);
    }

    //Private

    private void selectAction() {
        myProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewOutput.viewDidTapMyProfile();
            }
        });

        myWritesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewOutput.viewDidTapMyWrites();
            }
        });

        mastersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewOutput.viewDidTapMasters();
            }
        });

        servicesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewOutput.viewDidTapServices();
            }
        });
    }

    private void enableIcon(boolean enable) {
        if (enable) {
            myWritesButton.setColorFilter(0xffff0000);
            myWritesTv.setTextColor(0xffff0000);

        } else {
            myWritesButton.setColorFilter(0xffffffff);
            myWritesTv.setTextColor(0xffffffff);
        }
    }
}
