package com.example.barbershop.modules.ClientWrite.Presenter;

import android.os.Build;
import android.support.annotation.RequiresApi;

import com.example.barbershop.Logic.DataService.Objects.Callback.DateCallback;
import com.example.barbershop.Logic.DateFormatter;
import com.example.barbershop.modules.ClientWrite.Interactor.ClientWriteInteractorInput;
import com.example.barbershop.modules.ClientWrite.Interactor.ClientWriteInteractorOutput;
import com.example.barbershop.modules.ClientWrite.Interactor.ClientWriteViewItem;
import com.example.barbershop.modules.ClientWrite.Interactor.WriteTimeItem;
import com.example.barbershop.modules.ClientWrite.Router.ClientWriteRouterInput;
import com.example.barbershop.modules.ClientWrite.View.ClientWriteViewInput;
import com.example.barbershop.modules.ClientWrite.View.ClientWriteViewOutput;
import com.example.barbershop.modules.Master.Interactor.MasterViewItem;
import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberViewItem;

import java.util.ArrayList;
import java.util.Date;


public class ClientWritePresenter implements ClientWriteViewOutput, ClientWriteInteractorOutput {

    private ClientWriteRouterInput router;
    private ClientWriteInteractorInput interactor;
    private ClientWriteViewInput userInterface;

    public void setRouter(ClientWriteRouterInput router) {
        this.router = router;
    }

    public void setInteractor(ClientWriteInteractorInput interactor) {
        this.interactor = interactor;
    }

    public void setViewInterface(ClientWriteViewInput userInterface) {
        this.userInterface = userInterface;
    }

    //ClientWriteViewOutput

    @Override
    public void viewDidStarted() {
        userInterface.fillMasters(interactor.getMasters());
    }

    @Override
    public void viewDidTapDateEdit() {
        router.presentAlertDatePickerDialog(interactor.getSelectedDay(), new DateCallback() {
            @Override
            public void done(Date date) {
                setDate(date);
            }
        });
    }

    @Override
    public void viewDidTapTimeEdit(ClientWriteViewItem viewItem) {
//        if (viewItem == null || viewItem.master.isEmpty() || viewItem.date.isEmpty() || viewItem.service.isEmpty()) {
//            router.showInfoAlert("поля \"Мастер\" , \"Дата\" и \"Услуга\" должны быть заполены!");
//
//        } else {
//            router.showTimesForChoice(interactor.getDate(), interactor.getMasterId(), interactor.getIdToken(), new SelectedObjectCallback() {
//                @Override
//                public void didSelect(String timeIntervalId) {
//                    interactor.updateFieldTime(timeIntervalId);
//                }
//            });
//        }
    }

    @Override
    public void viewDidSaveButton() {
//        if (viewItem == null || viewItem.master.isEmpty() || viewItem.date.isEmpty() || viewItem.service.isEmpty() || viewItem.time == null) {
//            router.showInfoAlert("поля \"Мастер\" , \"Дата\" , \"Услуга\" и \"Время\" должны быть заполены!");
//
//        } else {
//            router.showEditAlert("Сохранить запись?", new EditCallBack() {
//                @Override
//                public void done(boolean ok) {
//                    if (ok) interactor.saveWrite();
//                }
//            });
//        }
    }

    @Override
    public void deselectMaster() {
        userInterface.fillMasters(interactor.getMasters());
    }

    @Override
    public void selectMaster(MasterViewItem item) {
        interactor.setSelectedMaster(item);
        userInterface.fillServices(item.services);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void selectService(ServiceBarberViewItem item) {
        interactor.setSelectedService(item);

        setDate(new Date());
    }

    //ClientWriteInteractorOutput


    @Override
    public void interactorDidLoadViewItem(ClientWriteViewItem viewItem) {
//        userInterface.loadViewItem(viewItem);
    }

    @Override
    public void interactorDidSaveWrite() {
        router.closeModule();
    }

    @Override
    public void interactorDidError(String error) {
        router.showInfoAlert(error);
    }

    //Private

    private void setDate(Date date) {

        interactor.setDay(date);
        userInterface.setDate(DateFormatter.stringFromDate(date));
        userInterface.fillTimes(null);

        interactor.getTimes(new ClientWriteInteractorInput.TimesCallback() {
            @Override
            public void loadTimeItems(ArrayList<WriteTimeItem> items) {
                userInterface.fillTimes(items);
            }
        });
    }
}
