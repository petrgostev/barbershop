package com.example.barbershop.modules.Master.Interactor;


import com.example.barbershop.Logic.DataService.Objects.WorkMode;

public interface MasterInteractorInput {
    void createMaster();

    void registrationMaster(String surname, String name, String patronymic, String phone, String email, String password, String workingHours);

    void loadMaster();

    void deleteMaster();

    void updateMaster(String surname, String name, String patronymic, String phone, String email, String password, String workingHours);

    void logoutMaster();

    String getId();

    String getIdToken();

    void removePhoto();

    void updateWorkMode(WorkMode workMode);

    boolean isAdmin();

    boolean isMaster();

    void updateData(String surname, String name, String patronymic, String phone, String email, String password);
}
