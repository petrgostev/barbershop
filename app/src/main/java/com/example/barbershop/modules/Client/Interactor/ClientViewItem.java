package com.example.barbershop.modules.Client.Interactor;


public class ClientViewItem {

    public String  id;
    public String name;
    public String fullName;
    public String surname;
    public String patronymic;
    public String phone;
    public String login;
    public String email;
    public String password;
    public String src;
}
