package com.example.barbershop.modules.ClientsForChoice.View;

import com.example.barbershop.modules.Client.Interactor.ClientViewItem;

import java.util.ArrayList;


public interface ClientsForChoiceViewInterface {
    void showViewItems(ArrayList<ClientViewItem> viewItems);
}
