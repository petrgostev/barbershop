package com.example.barbershop.modules.ClientsForChoice.Assembly;

import android.os.Bundle;

import com.example.barbershop.Logic.ClientService.ClientService;
import com.example.barbershop.Logic.ClientService.ClientServiceInterface;
import com.example.barbershop.Logic.DataService.Objects.Callback.SelectedObjectCallback;
import com.example.barbershop.Logic.MasterService.MasterService;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.modules.ClientsForChoice.Interactor.ClientsForChoiceInteractor;
import com.example.barbershop.modules.ClientsForChoice.Presenter.ClientsForChoicePresenter;
import com.example.barbershop.modules.ClientsForChoice.Router.ClientsForChoiceRouter;
import com.example.barbershop.modules.ClientsForChoice.View.ClientsForChoiceFragment;

public class ClientsForChoiceAssembly {

    private static String IS_ADMIN_KEY = "IS_ADMIN_KEY";
    private static String IS_MASTER_KEY = "IS_MASTER_KEY";
    private static String ID_TOKEN_KEY = "ID_TOKEN_KEY";

    public static ClientsForChoiceFragment ClientsListModuleForSelectMaster(String idToken, SelectedObjectCallback callback) {
        ClientsForChoiceFragment fragment = new ClientsForChoiceFragment();
        fragment.callback = callback;
        return fragment;
    }

    public static void configureModule(ClientsForChoiceFragment fragment) {
        Bundle args = fragment.getArguments();

        String idToken = "";

        if (args != null) {
            idToken = args.getString(ID_TOKEN_KEY);
        }

        MasterServiceInterface masterService = MasterService.getInstance(fragment.getContext());
        ClientServiceInterface clientService = ClientService.getInstance(fragment.getContext());

        ClientsForChoiceInteractor interactor = new ClientsForChoiceInteractor(idToken, clientService);
        ClientsForChoicePresenter presenter = new ClientsForChoicePresenter();
        ClientsForChoiceRouter router = new ClientsForChoiceRouter(fragment.getActivity());

        fragment.setViewOutput(presenter);

        presenter.setRouter(router);
        presenter.setInteractor(interactor);
        presenter.setViewInterface(fragment);
        presenter.selectedCallback = fragment.callback;

        interactor.setOutput(presenter);
    }
}
