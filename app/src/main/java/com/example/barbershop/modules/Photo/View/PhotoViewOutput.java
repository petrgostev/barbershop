package com.example.barbershop.modules.Photo.View;

public interface PhotoViewOutput {
    void viewDidStarted();

    void viewDidTapSaveButton();

    void viewDidTapDeleteButton();
}
