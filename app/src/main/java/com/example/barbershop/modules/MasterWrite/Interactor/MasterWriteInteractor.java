package com.example.barbershop.modules.MasterWrite.Interactor;

import com.example.barbershop.Logic.ClientService.ClientServiceInterface;
import com.example.barbershop.Logic.ClientService.Objects.Callback.ClientCallback;
import com.example.barbershop.Logic.ClientService.Objects.Client;
import com.example.barbershop.Logic.DataService.DataServiceInterface;
import com.example.barbershop.Logic.DataService.Objects.Callback.ServiceBarberCallback;
import com.example.barbershop.Logic.DataService.Objects.Callback.WriteCallback;
import com.example.barbershop.Logic.DataService.Objects.Callback.WritesCallback;
import com.example.barbershop.Logic.MasterService.Objects.ServiceBarber;
import com.example.barbershop.Logic.DataService.Objects.TimeInterval;
import com.example.barbershop.Logic.DataService.Objects.WorkMode;
import com.example.barbershop.Logic.DataService.Objects.Write;
import com.example.barbershop.Logic.MasterService.MasterCallback;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.Logic.MasterService.Objects.Master;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class MasterWriteInteractor implements MasterWriteInteractorInput {

    private MasterWriteInteractorOutput output;

    private String id;
    private String ownerId;
    private String clientId;
    private String idToken;

    private ClientServiceInterface clientService;
    private MasterServiceInterface masterService;
    private DataServiceInterface dataService;

    private Write write;
    private MasterWriteViewItem viewItem;
    private Date date;
    private String serviceId;
    private String serviceName = "";

    public MasterWriteInteractor(String ownerId, String idToken, ClientServiceInterface clientService,
                                 MasterServiceInterface masterService, DataServiceInterface dataService) {
        this.ownerId = ownerId;
        this.idToken = idToken;
        this.clientService = clientService;
        this.masterService = masterService;
        this.dataService = dataService;
    }

    public void setOutput(MasterWriteInteractorOutput output) {
        this.output = output;
    }

    @Override
    public void createWrite() {
        if (write == null) {
            write = new Write();

            id = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(new Date());

            write.setId(id);

        } else loadViewItem();
    }

    @Override
    public void updateFieldClient(String clientId) {
        this.clientId = clientId;

        clientService.getClientById(clientId, idToken, new ClientCallback() {
            @Override
            public void done(Error error, Client client) {
                if (error == null && client != null) {
                    write.setClientName(client.getFullName());

                    loadViewItem();

                } else {
                    //error
                }
            }
        });
    }

    @Override
    public void updateFieldDate(final Date date) {
        if (date.getTime() < new Date().getTime()) {
            output.interactorDidError("Нельзя выбирать прошедшую дату!");

        } else {
            this.date = date;

            masterService.getMasterById(ownerId, idToken, new MasterCallback() {
                @Override
                public void done(Error error, Master master) {
                    if (error == null && master != null) {
//                        master.getWorkMode().setDate(date);
                        write.setDate(date);

                        loadViewItem();

                    } else {
                        //error
                    }
                }
            });
        }
    }

    @Override
    public void updateFieldService(final String serviceId) {
        this.serviceId = serviceId;

        dataService.getServiceBarberById(serviceId, idToken, new ServiceBarberCallback() {
            @Override
            public void done(Error error, ServiceBarber serviceBarber) {
                if (error == null && serviceBarber != null) {

                    serviceName = serviceBarber.getName();

                    if (!write.getServiceBarberId().isEmpty()) {
                        write.removeServiceBarber();
                    }

                    write.setLengthTimeInterval(serviceBarber.getTimeInterval());

                    write.setServiceBarberId(serviceId);
                    write.setPrise(serviceBarber.getPrice());

                    loadViewItem();

                } else {
                    //error
                }
            }
        });
    }

    @Override
    public void updateFieldTime(final String timeIntervalId) {
//
//        masterService.getMasterById(ownerId, idToken, new MasterCallback() {
//            @Override
//            public void done(Error error, final Master master) {
//                if (error == null && master != null) {
//
////                    final WorkMode workMode = master.getWorkMode();
//
//                    final double endWork = workMode.getEndWork();
//
//                    dataService.getMasterWrites(ownerId, idToken, new WritesCallback() {
//                        @Override
//                        public void done(Error error, ArrayList<Write> writes) {
//                            if (error == null) {
//
//                                ArrayList<TimeInterval> nonFreeTimeIntervals = new ArrayList<>();
//                                TimeInterval timeOutInterval = workMode.getTimeOutInterval();
//
//                                if (writes.size() != 0) {
//
//                                    for (Write writeMaster : writes) {
//
//                                        if (writeMaster.getDate().equals(date)) {
//
//                                            nonFreeTimeIntervals = workMode.getTimeIntervalsForService(writeMaster.getTimeStart(),
//                                                    writeMaster.getLengthTimeInterval());
//
//                                            nonFreeTimeIntervals.add(timeOutInterval);
//                                        }
//                                    }
//                                }
//
//                                nonFreeTimeIntervals.add(timeOutInterval);
//
//                                final ArrayList<TimeInterval> finalNonFreeTimeIntervals = nonFreeTimeIntervals;
//                                dataService.getServiceBarberById(serviceId, idToken, new ServiceBarberCallback() {
//                                    @Override
//                                    public void done(Error error, ServiceBarber serviceBarber) {
//
//                                        if (error == null && serviceBarber != null) {
//                                            TimeInterval timeInterval = dataService.getTimeIntervalById(timeIntervalId);
//
//                                            double lengthTimeService = serviceBarber.getTimeInterval();
//
//                                            double countTimeInterval = 1;//количество интервалов для услуги
//
//                                            if (lengthTimeService > 0.31) {
//                                                countTimeInterval = (lengthTimeService * 2);//кол-во занятых интервалов
//                                            }
//
//                                            if (countTimeInterval % 1 > 0.31) {
//                                                countTimeInterval = countTimeInterval - 0.3;//количество интервалов
//                                            }
//
//                                            double timeStart = timeInterval.getStartTimeInterval();
//                                            double timeEndService = 0;
//
//                                            ArrayList<TimeInterval> intervalsRequiredForService =
//                                                    workMode.getTimeIntervalsForService(timeStart, countTimeInterval);//интервалы, нужные для услуги
//
//                                            for (TimeInterval interval : intervalsRequiredForService) {
//
//                                                for (TimeInterval nonFreeTimeInterval : finalNonFreeTimeIntervals) {
//
//                                                    if (interval.getStartTimeInterval() == nonFreeTimeInterval.getStartTimeInterval() ||
//                                                            interval.getEndTimeInterval() == nonFreeTimeInterval.getEndTimeInterval() ||
//                                                            interval.getEndTimeInterval() > endWork) {
//
//                                                        output.interactorDidError("Выберите другое время!");
//                                                        return;
//                                                    }
//                                                }
//
//                                                timeEndService = interval.getEndTimeInterval();
//                                            }
//
//                                            write.setTimeEnd(timeEndService);
//
//                                            write.setTimeStart(timeInterval.getStartTimeInterval());
//
//                                            loadViewItem();
//
//                                        } else {
//                                            //error
//                                        }
//
//                                    }
//                                });
//
//                            } else {
//                                //error
//                            }
//                        }
//                    });
//                } else {
//                    //error
//                }
//            }
//        });
    }

    @Override
    public void saveWrite() {
//
//        if (viewItem.client.isEmpty() || viewItem.date.isEmpty() || viewItem.service.isEmpty() || viewItem.time.isEmpty()) {
//            output.interactorDidError("Все поля должны быть заполнены!");
//            return;
//        }
//        masterService.getMasterById(ownerId, idToken, new MasterCallback() {
//            @Override
//            public void done(Error error, Master master) {
//                if (error == null && master != null) {
//                    master.addWriteId(write.getId());
//                    write.setMasterId(ownerId);
//                    write.setMasterName(master.getFullName());
//
//                    masterService.updateMaster(master, new MasterCallback() {
//                        @Override
//                        public void done(Error error, Master master) {
//                            if (error == null && master != null) {
//
//                                clientService.getClientById(clientId, idToken, new ClientCallback() {
//                                    @Override
//                                    public void done(Error error, Client client) {
//                                        if (error == null && client != null) {
//                                            client.addWriteId(write.getId());
//                                            write.setClientId(clientId);
//                                            write.setClientName(client.getFullName());
//
//                                            clientService.updateClient(client, new ClientCallback() {
//                                                @Override
//                                                public void done(Error error, Client client) {
//                                                    if (error == null && client != null) {
//
//                                                        dataService.saveWrite(write, idToken, new WriteCallback() {
//                                                            @Override
//                                                            public void done(Error error, Write write) {
//                                                                if (error == null) {
//                                                                    output.interactorDidSaveWrite();
//
//                                                                } else {
//                                                                    //error
//                                                                }
//                                                            }
//                                                        });
//
//                                                    } else {
//                                                        //error
//                                                    }
//                                                }
//                                            });
//
//                                        } else {
//                                            //error
//                                        }
//                                    }
//                                });
//
//                            } else {
//                                //error
//                            }
//                        }
//                    });
//
//                } else {
//                    //error
//                }
//            }
//        });
    }

    @Override
    public String getIdToken() {
        return idToken;
    }

    //Private

    private void loadViewItem() {
        viewItem = new MasterWriteViewItem();

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
        String pattern = "#00.00";
        DecimalFormat decimalFormat = new DecimalFormat(pattern, otherSymbols);

        String timeStart = decimalFormat.format(write.getTimeStart());
        String timeEnd = decimalFormat.format(write.getTimeEnd());

        viewItem.client = write.getClientName();
        viewItem.service = serviceName;
        viewItem.masterId = ownerId;

        if (write.getDate() == null) viewItem.date = "";
        else
            viewItem.date = new SimpleDateFormat("d.MM.yyyy", Locale.getDefault()).format(write.getDate());

        if (write.getTimeStart() != 0) viewItem.time = timeStart + " - " + timeEnd;

        viewItem.price = decimalFormat.format(write.getPrise());

        output.interactorDidLoadViewItem(viewItem);
    }
}
