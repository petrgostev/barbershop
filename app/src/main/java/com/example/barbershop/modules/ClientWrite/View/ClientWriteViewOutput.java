package com.example.barbershop.modules.ClientWrite.View;

import com.example.barbershop.modules.ClientWrite.Interactor.ClientWriteViewItem;
import com.example.barbershop.modules.Master.Interactor.MasterViewItem;
import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberViewItem;


public interface ClientWriteViewOutput {

    void viewDidStarted();

    void viewDidTapDateEdit();

    void viewDidTapTimeEdit(ClientWriteViewItem viewItem);

    void viewDidSaveButton();

    void deselectMaster();

    void selectMaster(MasterViewItem item);

    void selectService(ServiceBarberViewItem item);
}
