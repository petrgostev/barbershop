package com.example.barbershop.modules.Photo.Interactor;

import android.graphics.Bitmap;

import com.example.barbershop.Logic.ClientService.Objects.Callback.ClientCallback;
import com.example.barbershop.Logic.ClientService.ClientServiceInterface;
import com.example.barbershop.Logic.ClientService.Objects.Client;
import com.example.barbershop.Logic.FileService.FileCallback;
import com.example.barbershop.Logic.FileService.FileServiceInterface;
import com.example.barbershop.Logic.FileService.Objects.Photo;
import com.example.barbershop.Logic.FileService.PhotoUrlCallback;
import com.example.barbershop.Logic.MasterService.MasterCallback;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.Logic.MasterService.Objects.Master;

import java.io.ByteArrayOutputStream;


public class PhotoInteractor implements PhotoInteractorInput {

    private PhotoInteractorOutput output;
    private FileServiceInterface fileService;
    private MasterServiceInterface masterService;
    private ClientServiceInterface clientService;

    private String ownerId;
    private String idToken;
    private String id;
    private Bitmap bitmap;
    private boolean isMaster;

    Photo photo;

    public PhotoInteractor(String ownerId, String idToken, String id, Bitmap bitmap, FileServiceInterface fileService,
                           MasterServiceInterface masterService, ClientServiceInterface clientService, boolean isMaster) {

        this.ownerId = ownerId;
        this.idToken = idToken;
        this.id = id;
        this.bitmap = bitmap;
        this.isMaster = isMaster;
        this.fileService = fileService;
        this.masterService = masterService;
        this.clientService = clientService;
    }

    public void setOutput(PhotoInteractorOutput output) {
        this.output = output;
    }

    @Override
    public void createPhoto() {
        photo = new Photo();
    }

    @Override
    public void load() {

    }

    @Override
    public void loadForEdit() {
        if (photo != null) {
            loadViewItem(photo);
        }
    }

    @Override
    public void savePhoto() {
        if (bitmap != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
            byte[] jpegByte = stream.toByteArray();

            fileService.savePhoto(ownerId, jpegByte, isMaster, new PhotoUrlCallback() {
                @Override
                public void done(Error error, final String url, final Photo photo) {
                    if (error == null && url != null) {

                        if (isMaster) {
                            masterService.getMasterById(ownerId, idToken, new MasterCallback() {
                                @Override
                                public void done(Error error, Master master) {
                                    if (error == null && master != null) {
                                        master.setPhotoSrc(url);
                                        master.setPhotoName(photo.getName());

                                        masterService.updateMaster(master, new MasterCallback() {
                                            @Override
                                            public void done(Error error, Master master) {
                                                if (error == null) {
                                                    output.interactorDidSavePhoto();
                                                }
                                            }
                                        });

                                    } else {
                                        //
                                    }
                                }
                            });
                        } else {
                         clientService.getClientById(ownerId, idToken, new ClientCallback() {
                             @Override
                             public void done(Error error, Client client) {
                                 if (error == null && client != null) {
                                     client.setPhotoSrc(url);
                                     client.setPhotoName(photo.getName());

                                     clientService.updateClient(client, new ClientCallback() {
                                         @Override
                                         public void done(Error error, Client client) {
                                             if (error == null) {
                                                 output.interactorDidSavePhoto();
                                             }
                                         }
                                     });

                                 } else {
                                     //
                                 }
                             }
                         });
                        }
                    } else {
                        //
                    }
                }
            });
        }
    }

    @Override
    public void deletePhoto() {
        fileService.deletePhoto(ownerId, id, new FileCallback() {
            @Override
            public void done(Error error, Photo photo) {

            }
        });
    }

    //Private

    private void loadViewItem(Photo photo) {
        PhotoViewItem viewItem = new PhotoViewItem();

        viewItem.id = photo.getName();
        viewItem.bitmap = bitmap;

        output.interactorDidLoadViewItem(viewItem);
    }
}
