package com.example.barbershop.modules.MastersList.Presenter;

import com.example.barbershop.Logic.DataService.Objects.Callback.SelectedObjectCallback;
import com.example.barbershop.modules.Master.Interactor.MasterViewItem;
import com.example.barbershop.modules.MastersList.Interactor.MastersListInteractorInput;
import com.example.barbershop.modules.MastersList.Interactor.MastersListInteractorOutput;
import com.example.barbershop.modules.MastersList.Router.MastersListRouterInput;
import com.example.barbershop.modules.MastersList.View.MastersListViewInterface;
import com.example.barbershop.modules.MastersList.View.MastersListViewOutput;

import java.util.ArrayList;


public class MastersListPresenter implements MastersListViewOutput, MastersListInteractorOutput {

    private MastersListRouterInput router;
    private MastersListInteractorInput interactor;
    private MastersListViewInterface userInterface;

    public SelectedObjectCallback selectedCallback;

    private boolean isAdmin = false;
    private boolean isMaster = false;
    private boolean isClient = false;
    private boolean isWrite = false;

    public void setRouter(MastersListRouterInput router) {
        this.router = router;
    }

    public void setInteractor(MastersListInteractorInput interactor) {
        this.interactor = interactor;
    }

    public void setViewInterface(MastersListViewInterface userInterface) {
        this.userInterface = userInterface;
    }

    public void configureForMaster() {
        isAdmin = false;
        isClient = false;
        isMaster = true;
    }

    public void configureForClient() {
        isAdmin = false;
        isClient = true;
        isMaster = false;
        isWrite = true;
    }

    public void configureForAdmin() {
        isAdmin = true;
        isClient = false;
        isMaster = false;
    }

    //MastersListViewOutput

    @Override
    public void viewDidStarted() {
        interactor.loadMasters();
    }

    @Override
    public void viewDidTapCreateNewMaster() {
        router.showMasterModuleForCreate(isAdmin);
    }

    @Override
    public void viewDidSelectItem(MasterViewItem viewItem) {
        if (selectedCallback != null) {
            selectedCallback.didSelect(viewItem.id);
            router.closeModule();
        } else {
            router.presentMasterFragment(viewItem.id, interactor.getIdToken(), isAdmin, isMaster, isWrite);
        }
    }

    //MastersListInteractorOutput

    @Override
    public void interactorDidLoadViewItems(ArrayList<MasterViewItem> viewItems) {
        userInterface.showViewItems(viewItems, isAdmin, isMaster, isClient);
    }
}
