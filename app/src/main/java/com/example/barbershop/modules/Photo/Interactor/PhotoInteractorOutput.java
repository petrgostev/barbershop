package com.example.barbershop.modules.Photo.Interactor;


public interface PhotoInteractorOutput {

    void interactorDidLoadViewItem(PhotoViewItem viewItem);

    void interactorDidSavePhoto();
}
