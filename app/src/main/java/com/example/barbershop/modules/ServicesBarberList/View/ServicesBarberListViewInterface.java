package com.example.barbershop.modules.ServicesBarberList.View;

import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberViewItem;

import java.util.ArrayList;


public interface ServicesBarberListViewInterface {

    void showViewItems(ArrayList<ServiceBarberViewItem> viewItems);
}
