package com.example.barbershop.modules.ServicesBarberList.View;


import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberViewItem;

public interface ServicesBarberListViewOutput {

    void viewDidStarted();

    void viewDidSelectItem(ServiceBarberViewItem viewItem);

    void viewDidTapAddServiceBarber();

    void viewDidTapAddWrite(String id);
}
