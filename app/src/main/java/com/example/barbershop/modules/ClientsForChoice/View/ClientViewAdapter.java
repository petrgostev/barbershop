package com.example.barbershop.modules.ClientsForChoice.View;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.barbershop.Logic.FileService.FileService;
import com.example.barbershop.R;
import com.example.barbershop.modules.Client.Interactor.ClientViewItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ClientViewAdapter extends RecyclerView.Adapter<ClientViewAdapter.ViewHolder> {
    Context context;

    private ArrayList<ClientViewItem> viewItems;
    private FileService fileService = FileService.getInstance(context);

    ClientViewAdapter.Listener selectListener;

    public ClientViewAdapter(Context context, ArrayList<ClientViewItem> viewItems) {
        this.viewItems = viewItems;
        this.context = context;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        ViewHolder(CardView cardView) {
            super(cardView);
            this.cardView = cardView;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        CardView cv = (CardView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.client_card, viewGroup, false);

        return new ViewHolder(cv);
    }

    @Override
    public void onBindViewHolder(@NonNull ClientViewAdapter.ViewHolder viewHolder, int position) {
        CardView cardView = viewHolder.cardView;

        configureView(cardView, position, viewHolder);
    }

    private void configureView(CardView cardView, int position, final ViewHolder viewHolder) {
        final ClientViewItem item = viewItems.get(position);

        final TextView surnameTV = cardView.findViewById(R.id.surname);
        final TextView nameTV = cardView.findViewById(R.id.name);
        final TextView patronymicTV = cardView.findViewById(R.id.patronymic);
        final ImageView photoIV = cardView.findViewById(R.id.photo);

        surnameTV.setText(item.surname);
        nameTV.setText(item.name);
        patronymicTV.setText(item.patronymic);

        if (!item.src.equals("")) {

            Picasso.with(photoIV.getContext())
                    .load(item.src)
                    .into(photoIV);
        }

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectListener != null) {
                    selectListener.onClick(viewHolder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return viewItems.size();
    }

    interface Listener {
        void onClick(int position);
    }

    public void setSelectListener(Listener selectListener) {
        this.selectListener = selectListener;
    }
}
