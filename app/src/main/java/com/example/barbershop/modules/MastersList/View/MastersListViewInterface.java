package com.example.barbershop.modules.MastersList.View;

import com.example.barbershop.modules.Master.Interactor.MasterViewItem;

import java.util.ArrayList;


public interface MastersListViewInterface {

    void showViewItems(ArrayList<MasterViewItem> viewItems, boolean isAdmin, boolean isMaster, boolean isClient);
}
