package com.example.barbershop.modules.MastersList.Router;


public interface MastersListRouterInput {

    void showMasterModuleForCreate(boolean isAdmin);

    void presentMasterFragment(String id, String idToken, boolean isAdmin, boolean isMaster, boolean isWrite);

    void closeModule();
}
