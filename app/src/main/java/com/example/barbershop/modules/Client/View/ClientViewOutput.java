package com.example.barbershop.modules.Client.View;


public interface ClientViewOutput {
    void viewDidTapRegistrationButton(String surname, String name, String phone, String email, String password);

    void viewDidStarted();

    void viewDidTapDeleteButton();

    void viewDidTapExitButton();

    void viewDidTapAddPhotoButtonButton();

    void viewDidTapReplacePhotoButton();

    void viewDidTapSaveButton(String surname, String name, String phone, String email, String password);
}
