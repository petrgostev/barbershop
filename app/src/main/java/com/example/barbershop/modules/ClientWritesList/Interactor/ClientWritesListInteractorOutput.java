package com.example.barbershop.modules.ClientWritesList.Interactor;

import com.example.barbershop.modules.ClientWrite.Interactor.ClientWriteViewItem;

import java.util.ArrayList;

public interface ClientWritesListInteractorOutput {
    void interactorDidLoadViewItems(ArrayList<ClientWriteViewItem> viewItems);
}
