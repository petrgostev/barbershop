package com.example.barbershop.modules.ClientWrite.Assembly;

import android.os.Bundle;

import com.example.barbershop.Logic.ClientService.ClientService;
import com.example.barbershop.Logic.ClientService.ClientServiceInterface;
import com.example.barbershop.Logic.DataService.DataService;
import com.example.barbershop.Logic.DataService.DataServiceInterface;
import com.example.barbershop.Logic.MasterService.MasterService;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.modules.ClientWrite.Interactor.ClientWriteInteractor;
import com.example.barbershop.modules.ClientWrite.Presenter.ClientWritePresenter;
import com.example.barbershop.modules.ClientWrite.Router.ClientWriteRouter;
import com.example.barbershop.modules.ClientWrite.View.ClientWriteFragment;


public class ClientWriteAssembly {

    private static String OWNER_ID_KEY = "ID_KEY";
    private static String ID_TOKEN_KEY = "ID_TOKEN_KEY";
    private static String SERVICE_ID_KEY = "SERVICE_ID_KEY";

    public static ClientWriteFragment clientWriteModule(String ownerId, String serviceId, String idToken) {
        ClientWriteFragment fragment = new ClientWriteFragment();

        Bundle args = new Bundle();

        args.putString(OWNER_ID_KEY, ownerId);
        args.putString(SERVICE_ID_KEY, serviceId);
        args.putString(ID_TOKEN_KEY, idToken);

        fragment.setArguments(args);

        return fragment;
    }

    public static void configureModule(ClientWriteFragment fragment) {
        Bundle args = fragment.getArguments();

        String ownerId = null;
        String serviceId = null;
        String idToken = null;

        if (args != null) {
            ownerId = args.getString(OWNER_ID_KEY);
            serviceId = args.getString(SERVICE_ID_KEY);
            idToken = args.getString(ID_TOKEN_KEY);
        }

        ClientServiceInterface clientService = ClientService.getInstance(fragment.getContext());
        MasterServiceInterface masterService = MasterService.getInstance(fragment.getContext());
        DataServiceInterface dataService = DataService.getInstance(fragment.getContext());

        ClientWriteInteractor interactor = new ClientWriteInteractor(ownerId, serviceId, idToken, clientService, masterService, dataService);
        ClientWritePresenter presenter = new ClientWritePresenter();
        ClientWriteRouter router = new ClientWriteRouter(fragment.getActivity());

        fragment.setViewOutput(presenter);

        presenter.setRouter(router);
        presenter.setInteractor(interactor);
        presenter.setViewInterface(fragment);

        interactor.setOutput(presenter);
    }
}
