package com.example.barbershop.modules.TimeIntervalsList.Presenter;

import com.example.barbershop.Logic.DataService.Objects.Callback.SelectedObjectCallback;
import com.example.barbershop.modules.TimeIntervalsList.Interactor.TimeIntervalViewItem;
import com.example.barbershop.modules.TimeIntervalsList.Interactor.TimeIntervalsListInteractorInput;
import com.example.barbershop.modules.TimeIntervalsList.Interactor.TimeIntervalsListInteractorOutput;
import com.example.barbershop.modules.TimeIntervalsList.Router.TimeIntervalsListRouterInput;
import com.example.barbershop.modules.TimeIntervalsList.View.TimeIntervalsListViewInterface;
import com.example.barbershop.modules.TimeIntervalsList.View.TimeIntervalsListViewOutput;

import java.util.ArrayList;


public class TimeIntervalsListPresenter implements TimeIntervalsListViewOutput, TimeIntervalsListInteractorOutput {

    public SelectedObjectCallback callback;
    private TimeIntervalsListViewInterface userInterface;
    private TimeIntervalsListRouterInput router;
    private TimeIntervalsListInteractorInput interactor;

    public void setRouter(TimeIntervalsListRouterInput router) {
        this.router = router;
    }

    public void setInteractor(TimeIntervalsListInteractorInput interactor) {
        this.interactor = interactor;
    }

    public void setViewInterface(TimeIntervalsListViewInterface userInterface) {
        this.userInterface = userInterface;
    }

    //TimeIntervalsListViewOutput

    @Override
    public void viewDidStarted() {
        interactor.loadTimeIntervals();
    }

    @Override
    public void viewDidSelectItem(TimeIntervalViewItem viewItem) {
        if (callback != null) {
            callback.didSelect(viewItem.id);
            router.closeModule();
        }
    }

    //TimeIntervalsListInteractorOutput

    @Override
    public void interactorDidLoadViewItems(ArrayList<TimeIntervalViewItem> viewItems, String date) {
        userInterface.showViewItems(viewItems, date);
    }
}
