package com.example.barbershop.modules;

import android.app.Application;

import io.realm.Realm;


public class BarberShopApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
    }
}
