package com.example.barbershop.modules.WorkMode.Interactor;


public class WorkModeViewItem {

    public String startWork;
    public String endWork;
    public String startTimeout;
    public String endTimeout;
    public String lengthWorkingDay;
    public String weekend;
}
