package com.example.barbershop.modules.Loader.Router;


public interface LoaderRouterInput {
    void presentLogin();

    void presentMasterWritesList(String id, String idToken, boolean isAdmin, boolean isMaster);

    void presentClientWritesList(String id, String idToken);
}
