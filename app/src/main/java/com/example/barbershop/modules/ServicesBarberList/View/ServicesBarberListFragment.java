package com.example.barbershop.modules.ServicesBarberList.View;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.barbershop.Logic.DataService.Objects.Callback.SelectedObjectCallback;
import com.example.barbershop.R;
import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberViewItem;
import com.example.barbershop.modules.ServicesBarberList.Assembly.ServicesBarberListAssembly;

import java.util.ArrayList;
import java.util.Objects;

public class ServicesBarberListFragment extends Fragment implements ServicesBarberListViewInterface {

    public SelectedObjectCallback callback;

    private ServicesBarberListViewOutput viewOutput;

    private ServiceBarberAdapter adapter;

    private RecyclerView recycler;

    private Toolbar toolbar;

    private TextView title;

    private MenuItem itemAdd;
    private MenuItem itemEdit;
    private MenuItem itemSave;
    private MenuItem itemDelete;

    private ImageButton servicesButton;
    private TextView servicesTv;

    ServiceBarberViewItem viewItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        ServicesBarberListAssembly.configureModule(this);
    }

    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        recycler = view.findViewById(R.id.recycler);

        servicesButton = Objects.requireNonNull(getActivity()).findViewById(R.id.services_button);
        servicesTv = Objects.requireNonNull(getActivity()).findViewById(R.id.services_tv);

        title = Objects.requireNonNull(getActivity()).findViewById(R.id.titleView);

        toolbar = Objects.requireNonNull(getActivity()).findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);

        return view;
    }

    public void setViewOutput(ServicesBarberListViewOutput viewOutput) {
        this.viewOutput = viewOutput;
    }

    @Override
    public void onStart() {
        super.onStart();
        enableIcon(true);
        viewOutput.viewDidStarted();
    }

    @Override
    public void onPause() {
        super.onPause();
        enableIcon(false);
    }

    @Override
    public void showViewItems(final ArrayList<ServiceBarberViewItem> viewItems) {

        title.setText("Услуги");

        adapter = new ServiceBarberAdapter(getContext(), viewItems);
        recycler.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycler.setLayoutManager(layoutManager);

        adapter.setSelectListener(new ServiceBarberAdapter.Listener() {
            @Override
            public void onClick(int position) {
                viewItem = viewItems.get(position);

                    viewOutput.viewDidTapAddWrite(viewItem.id);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        inflater.inflate(R.menu.menu_main, menu);
        itemDelete = menu.findItem(R.id.delete);
        itemSave = menu.findItem(R.id.save);
        itemEdit = menu.findItem(R.id.edit);
        itemAdd = menu.findItem(R.id.add);

        itemAdd.setVisible(false);
        itemSave.setVisible(false);
        itemDelete.setVisible(false);
        itemEdit.setVisible(false);

        super.onCreateOptionsMenu(menu, inflater);
    }


    //Private

    private void enableIcon(boolean enable) {
        if (enable) {
            servicesButton.setColorFilter(0xffff0000);
            servicesTv.setTextColor(0xffff0000);

        } else {
            servicesButton.setColorFilter(0xffffffff);
            servicesTv.setTextColor(0xffffffff);
        }
    }
}
