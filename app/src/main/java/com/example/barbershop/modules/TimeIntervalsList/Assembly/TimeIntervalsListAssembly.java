package com.example.barbershop.modules.TimeIntervalsList.Assembly;

import android.os.Bundle;

import com.example.barbershop.Logic.ClientService.ClientService;
import com.example.barbershop.Logic.ClientService.ClientServiceInterface;
import com.example.barbershop.Logic.DataService.DataService;
import com.example.barbershop.Logic.DataService.DataServiceInterface;
import com.example.barbershop.Logic.DataService.Objects.Callback.SelectedObjectCallback;
import com.example.barbershop.Logic.MasterService.MasterService;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.modules.TimeIntervalsList.Interactor.TimeIntervalsListInteractor;
import com.example.barbershop.modules.TimeIntervalsList.Presenter.TimeIntervalsListPresenter;
import com.example.barbershop.modules.TimeIntervalsList.Router.TimeIntervalsListRouter;
import com.example.barbershop.modules.TimeIntervalsList.View.TimeIntervalsListFragment;


public class TimeIntervalsListAssembly {

    private static String ID_TOKEN_KEY = "ID_TOKEN_KEY";
    private static String DATE_KEY = "DATE_KEY";
    private static String MASTER_ID_KEY = "MASTER_ID_KEY";

    public static TimeIntervalsListFragment TimeIntervalsListModule(String date, String masterId, String idToken,
                                                                    SelectedObjectCallback callback) {

        TimeIntervalsListFragment fragment = new TimeIntervalsListFragment();

        Bundle args = new Bundle();

        args.putString(DATE_KEY, date);
        args.putString(MASTER_ID_KEY, masterId);
        args.putString(ID_TOKEN_KEY, idToken);

        fragment.setArguments(args);
        fragment.callback = callback;
        return fragment;
    }

    public static void configureModule(TimeIntervalsListFragment fragment) {
        Bundle args = fragment.getArguments();

        String date = "";
        String masterId = "";
        String idToken = "";

        if (args != null) {
            date = args.getString(DATE_KEY);
            masterId = args.getString(MASTER_ID_KEY);
            idToken = args.getString(ID_TOKEN_KEY);
        }

        ClientServiceInterface clientService = ClientService.getInstance(fragment.getContext());
        MasterServiceInterface masterService = MasterService.getInstance(fragment.getContext());
        DataServiceInterface dataService = DataService.getInstance(fragment.getContext());

        TimeIntervalsListInteractor interactor = new TimeIntervalsListInteractor(date, masterId, idToken, clientService,
                                                                                 masterService, dataService);
        TimeIntervalsListPresenter presenter = new TimeIntervalsListPresenter();
        TimeIntervalsListRouter router = new TimeIntervalsListRouter(fragment.getActivity());

        fragment.setViewOutput(presenter);

        presenter.setRouter(router);
        presenter.setInteractor(interactor);
        presenter.setViewInterface(fragment);
        presenter.callback = fragment.callback;

        interactor.setOutput(presenter);
    }
}
