package com.example.barbershop.modules.MasterWrite.View;

import com.example.barbershop.modules.MasterWrite.Interactor.MasterWriteViewItem;


public interface MasterWriteViewInterface {

    void loadViewItem(MasterWriteViewItem viewItem);
}
