package com.example.barbershop.modules.TimeIntervalsList.Interactor;

import com.example.barbershop.Logic.ClientService.ClientServiceInterface;
import com.example.barbershop.Logic.DataService.DataServiceInterface;
import com.example.barbershop.Logic.DataService.Objects.Callback.WritesCallback;
import com.example.barbershop.Logic.DataService.Objects.TimeInterval;
import com.example.barbershop.Logic.DataService.Objects.WorkMode;
import com.example.barbershop.Logic.DataService.Objects.Write;
import com.example.barbershop.Logic.MasterService.MasterCallback;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.Logic.MasterService.Objects.Master;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;


public class TimeIntervalsListInteractor implements TimeIntervalsListInteractorInput {

    private TimeIntervalsListInteractorOutput output;

    private String date;
    private String masterId;
    private String idToken;

    private ArrayList<TimeIntervalViewItem> viewItems;

    private ClientServiceInterface clientService;
    private MasterServiceInterface masterService;
    private DataServiceInterface dataService;

    public TimeIntervalsListInteractor(String date,
                                       String masterId,
                                       String idToken,
                                       ClientServiceInterface clientService,
                                       MasterServiceInterface masterService,
                                       DataServiceInterface dataService) {
        this.date = date;
        this.masterId = masterId;
        this.idToken = idToken;
        this.clientService = clientService;
        this.masterService = masterService;
        this.dataService = dataService;
    }

    public void setOutput(TimeIntervalsListInteractorOutput output) {
        this.output = output;
    }

    @Override
    public void loadTimeIntervals() {
//        masterService.getMasterById(masterId, idToken, new MasterCallback() {
//            @Override
//            public void done(Error error, final Master master) {
//                if (error == null && master != null) {
//
//                    final WorkMode workMode = master.getWorkMode();
//
//                    dataService.getMasterWrites(masterId, idToken, new WritesCallback() {
//                        @Override
//                        public void done(Error error, ArrayList<Write> writes) {
//                            if (error == null) {
//
//                                ArrayList<Write> existentWrite = new ArrayList<>();
//
//                                if (writes.size() != 0) {
//
//                                    for (Write write : writes) {
//                                        String dateWrite = new SimpleDateFormat("d.MM.yyyy", Locale.getDefault()).format(write.getDate());
//
//                                        if (dateWrite.equals(date)) {
//                                            existentWrite.add(write);
//                                        }
//                                    }
//                                }
//
//                                ArrayList<TimeInterval> timeIntervals = workMode.getTimeIntervals(existentWrite);
//
//                                dataService.saveTimeIntervals(timeIntervals);
//
//                                loadViewItems(timeIntervals);
//
//                                output.interactorDidLoadViewItems(viewItems, date);
//
//                            } else {
//                                //error
//                            }
//                        }
//                    });
//                } else {
//                    //error
//                }
//            }
//        });
    }

    //Private

    private void loadViewItems(ArrayList<TimeInterval> timeIntervals) {
        viewItems = new ArrayList<>();

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
        String pattern = "#00.00";
        DecimalFormat decimalFormat = new DecimalFormat(pattern, otherSymbols);

        for (TimeInterval timeInterval : timeIntervals) {
            TimeIntervalViewItem item = new TimeIntervalViewItem();

            item.start = decimalFormat.format(timeInterval.getStartTimeInterval());
            item.end = decimalFormat.format(timeInterval.getEndTimeInterval());

            item.id = timeInterval.getId();

            viewItems.add(item);
        }
    }
}
