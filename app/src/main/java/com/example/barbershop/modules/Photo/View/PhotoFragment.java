package com.example.barbershop.modules.Photo.View;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.barbershop.R;
import com.example.barbershop.modules.Photo.Assembly.PhotoAssembly;
import com.example.barbershop.modules.Photo.Interactor.PhotoViewItem;

import java.util.Objects;

public class PhotoFragment extends Fragment implements PhotoViewInterface {

    private PhotoViewOutput viewOutput;

    private ImageView photoIV;
    private ImageButton rotateLeftButton;
    private ImageButton rotateRightButton;

    private Toolbar toolbar;
    MenuItem itemEdit;
    MenuItem itemAdd;
    MenuItem itemSave;
    MenuItem itemDelete;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PhotoAssembly.configureModule(this);

        setHasOptionsMenu(true);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo, container, false);

        toolbar = Objects.requireNonNull(getActivity()).findViewById(R.id.toolbar);

        TextView title = Objects.requireNonNull(getActivity()).findViewById(R.id.titleView);

        photoIV = view.findViewById(R.id.photo);
        rotateLeftButton = view.findViewById(R.id.left);
        rotateRightButton = view.findViewById(R.id.right);

        title.setText("Фото");

        return view;
    }

    public void setViewOutput(PhotoViewOutput viewOutput) {
        this.viewOutput = viewOutput;
    }

    @Override
    public void onStart() {
        super.onStart();
        viewOutput.viewDidStarted();
    }

    @Override
    public void showEditButton() {
        setMenuVisibility(true);
    }

    @Override
    public void loadViewItem(PhotoViewItem viewItem) {
        if (viewItem.bitmap != null) {
            photoIV.setImageBitmap(viewItem.bitmap);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        inflater.inflate(R.menu.menu_main, menu);
        itemDelete = menu.findItem(R.id.delete);
        itemSave = menu.findItem(R.id.save);
        itemEdit = menu.findItem(R.id.edit);
        itemAdd = menu.findItem(R.id.add);

        itemEdit.setVisible(true);
        itemSave.setVisible(true);
        itemDelete.setVisible(false);
        itemAdd.setVisible(false);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.edit) {
            itemSave.setVisible(true);
            itemEdit.setVisible(false);

            rotateLeftButton.setVisibility(View.VISIBLE);
            rotateRightButton.setVisibility(View.VISIBLE);
        }

        if (id == R.id.save) {
            itemSave.setVisible(false);
            itemDelete.setVisible(false);
            itemEdit.setVisible(true);

            rotateLeftButton.setVisibility(View.GONE);
            rotateRightButton.setVisibility(View.GONE);

            viewOutput.viewDidTapSaveButton();
        }

        return super.onOptionsItemSelected(item);
    }
}
