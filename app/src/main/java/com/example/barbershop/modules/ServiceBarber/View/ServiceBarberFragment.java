package com.example.barbershop.modules.ServiceBarber.View;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.barbershop.R;
import com.example.barbershop.modules.ServiceBarber.Assembly.ServiceBarberAssembly;
import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberViewItem;

import java.util.Objects;

public class ServiceBarberFragment extends Fragment implements ServiceBarberViewInterface {


    private ServiceBarberViewOutput viewOutput;

    private Toolbar toolbar;
    private MenuItem itemAdd;
    private MenuItem itemEdit;
    private MenuItem itemSave;
    private MenuItem itemDelete;

    private TextInputEditText nameET;
    private TextInputEditText priceET;
    private TextInputEditText timeIntervalET;

    private boolean isEdit = false;
    boolean isAdmin = false;
    boolean isForCreate = false;
    boolean isBindToMaster = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        Bundle args = getArguments();

        if (args != null) {
            String IS_BIND_TO_MASTER_KEY = "IS_BIND_TO_MASTER_KEY";
            isBindToMaster = args.getBoolean(IS_BIND_TO_MASTER_KEY);
            String IS_ADMIN_KEY = "IS_ADMIN_KEY";
            isAdmin = args.getBoolean(IS_ADMIN_KEY);
            String IS_FOR_CREATE_KEY = "IS_FOR_CREATE_KEY";
            isForCreate = args.getBoolean(IS_FOR_CREATE_KEY);
        }


        ServiceBarberAssembly.configureModule(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_service_barber, container, false);

        toolbar = Objects.requireNonNull(getActivity()).findViewById(R.id.toolbar);

        TextView title = Objects.requireNonNull(getActivity()).findViewById(R.id.titleView);

        nameET = view.findViewById(R.id.name);
        priceET = view.findViewById(R.id.price);
        timeIntervalET = view.findViewById(R.id.timeInterval);

        if (isBindToMaster) title.setText("Услуга мастера");
        else title.setText("Услуга");

        return view;
    }

    public void setViewOutput(ServiceBarberViewOutput viewOutput) {
        this.viewOutput = viewOutput;
    }

    @Override
    public void onStart() {
        super.onStart();
        viewOutput.viewDidStarted();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void showViewItem(ServiceBarberViewItem viewItem) {

        nameET.setText(viewItem.name);
        priceET.setText(viewItem.price);
        timeIntervalET.setText(viewItem.timeInterval);
    }

    @Override
    public void setEditMode(boolean edit) {
        isEdit = edit;

        nameET.setEnabled(isEdit);
        priceET.setEnabled(isEdit);
        timeIntervalET.setEnabled(isEdit);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        inflater.inflate(R.menu.menu_main, menu);
        itemDelete = menu.findItem(R.id.delete);
        itemSave = menu.findItem(R.id.save);
        itemEdit = menu.findItem(R.id.edit);
        itemAdd = menu.findItem(R.id.add);

        itemAdd.setVisible(false);
        itemDelete.setVisible(false);

        if (isAdmin && !isEdit && !isBindToMaster) itemEdit.setVisible(true);
        else itemEdit.setVisible(false);

        itemSave.setVisible(isEdit);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.edit) {
            setEditMode(true);
            itemEdit.setVisible(false);
            itemSave.setVisible(true);
            itemDelete.setVisible(true);
        }

        if (id == R.id.save) {
            itemEdit.setVisible(true);
            itemSave.setVisible(false);
            itemDelete.setVisible(false);

            if (isForCreate) {
                viewOutput.viewDidTapSaveButton(Objects.requireNonNull(nameET.getText()).toString(),
                        Objects.requireNonNull(priceET.getText()).toString(),
                        Objects.requireNonNull(timeIntervalET.getText()).toString());

            } else {
                viewOutput.viewDidTapUpdateButton(Objects.requireNonNull(nameET.getText()).toString(),
                        Objects.requireNonNull(priceET.getText()).toString(),
                        Objects.requireNonNull(timeIntervalET.getText()).toString());
            }

        }

        if (id == R.id.delete) {
            itemEdit.setVisible(!isBindToMaster);
            itemSave.setVisible(false);
            itemDelete.setVisible(false);

            if (isBindToMaster) viewOutput.viewDidTapDeleteServiceBarberFromMaster();

            else viewOutput.viewDidTapDeleteServiceBarber();
        }

        return super.onOptionsItemSelected(item);
    }
}
