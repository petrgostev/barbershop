package com.example.barbershop.modules.MasterWrite.Presenter;

import com.example.barbershop.Logic.DataService.Objects.Callback.DateCallback;
import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;
import com.example.barbershop.Logic.DataService.Objects.Callback.SelectedObjectCallback;
import com.example.barbershop.modules.MasterWrite.Interactor.MasterWriteInteractorInput;
import com.example.barbershop.modules.MasterWrite.Interactor.MasterWriteInteractorOutput;
import com.example.barbershop.modules.MasterWrite.Interactor.MasterWriteViewItem;
import com.example.barbershop.modules.MasterWrite.Router.MasterWriteRouterInput;
import com.example.barbershop.modules.MasterWrite.View.MasterWriteViewInterface;
import com.example.barbershop.modules.MasterWrite.View.MasterWriteViewOutput;

import java.util.Date;


public class MasterWritePresenter implements MasterWriteViewOutput, MasterWriteInteractorOutput {

    private MasterWriteRouterInput router;
    private MasterWriteViewInterface userInterface;
    private MasterWriteInteractorInput interactor;
    private EditCallBack callBack;

    public void setRouter(MasterWriteRouterInput router) {
        this.router = router;
    }

    public void setInteractor(MasterWriteInteractorInput interactor) {
        this.interactor = interactor;
    }

    public void setViewInterface(MasterWriteViewInterface userInterface) {
        this.userInterface = userInterface;
    }

    //MasterWriteViewOutput

    @Override
    public void viewDidStarted() {
        interactor.createWrite();
    }

    @Override
    public void viewDidTapClientEdit() {
        router.showClientsForChoice(interactor.getIdToken(), new SelectedObjectCallback() {
            @Override
            public void didSelect(String objectId) {
                interactor.updateFieldClient(objectId);
            }
        });
    }

    @Override
    public void viewDidTapDateEdit(MasterWriteViewItem viewItem) {

        if (viewItem == null || viewItem.client.isEmpty()) {
            router.showInfoAlert("поле \"Клиент\" должно быть заполено!");

        } else {
            router.presentAlertDatePickerDialog(new DateCallback() {
                @Override
                public void done(Date date) {
                    interactor.updateFieldDate(date);
                }
            });
        }
    }

    @Override
    public void viewDidTapTimeEdit(MasterWriteViewItem viewItem) {

        if (viewItem == null || viewItem.client.isEmpty() || viewItem.date.isEmpty() || viewItem.service.isEmpty()) {
            router.showInfoAlert("поля \"Клиент\" , \"Дата\" и \"Услуга\" должны быть заполены!");

        } else {
            router.showTimesForChoice(viewItem.date, viewItem.masterId, interactor.getIdToken(), new SelectedObjectCallback() {
                @Override
                public void didSelect(String timeIntervalId) {
                    interactor.updateFieldTime(timeIntervalId);
                }
            });
        }
    }

    @Override
    public void viewDidTapServiceEdit(MasterWriteViewItem viewItem) {

        if (viewItem == null || viewItem.client.isEmpty() || viewItem.date.isEmpty()) {
            router.showInfoAlert("поля \"Клиент\" и \"Дата\" должны быть заполены!");

        } else {
            router.showServicesForChoice(viewItem.masterId, new SelectedObjectCallback() {
                @Override
                public void didSelect(String serviceId) {
                    interactor.updateFieldService(serviceId);
                }
            });
        }
    }

    @Override
    public void viewDidTapSaveButton(MasterWriteViewItem viewItem) {
        if ( viewItem.client.isEmpty() || viewItem.date.isEmpty() || viewItem.service.isEmpty() || viewItem.time == null) {
            router.showInfoAlert("поля \"Клиент\" , \"Дата\" , \"Услуга\" и \"Время\" должны быть заполены!");

        } else {
            router.showEditAlert("Сохранить запись?", new EditCallBack() {
                @Override
                public void done(boolean ok) {
                    if (ok) interactor.saveWrite();
                }
            });
        }
    }

    //MasterWriteInteractorOutput

    @Override
    public void interactorDidLoadViewItem(MasterWriteViewItem viewItem) {
        userInterface.loadViewItem(viewItem);
    }

    @Override
    public void interactorDidSaveWrite() {
        callBack.done(true);
        router.closeModule();
    }

    @Override
    public void interactorDidError(String error) {
        router.showInfoAlert(error);
    }

    public void setEditCallBack(EditCallBack callBack) {
        this.callBack = callBack;
    }
}
