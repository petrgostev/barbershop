package com.example.barbershop.modules.Client.Interactor;


public interface ClientInteractorOutput {
    void interactorDidRegistrationNewClient(String id, String idToken);

    void interactorDidLoadViewItem(ClientViewItem viewItem);

    void interactorDidLogoutClient();

    void interactorDidDeletePhoto();
}
