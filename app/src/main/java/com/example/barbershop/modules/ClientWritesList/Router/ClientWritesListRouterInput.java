package com.example.barbershop.modules.ClientWritesList.Router;

import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;

public interface ClientWritesListRouterInput {
    void presentClientWriteFragment(String ownerId, String idToken);

    void showEditAlert(String title, EditCallBack callBack);

    void presentClientFragment(String ownerId, String idToken);

    void presentClientWritesListFragment(String ownerId, String idToken);

    void presentMastersListFragment(boolean isAdmin, boolean isMaster, String idToken);

    void presentServicesBarberListFragment(String id, String idToken, boolean isAdmin, boolean isMaster);
}
