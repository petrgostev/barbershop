package com.example.barbershop.modules.ServiceBarber.Interactor;


public interface ServiceBarberInteractorOutput {

    void interactorDidReceivedError(String error);

    void interactorDidSaveServiceBarber();

    void interactorDidLoadViewItem(ServiceBarberViewItem viewItem);

    void interactorDidUpdateServiceBarber();

    void interactorDidDeleteServiceBarber();

    void interactorDidDeleteServiceBarberFromMaster();
}
