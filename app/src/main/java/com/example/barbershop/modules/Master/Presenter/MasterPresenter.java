package com.example.barbershop.modules.Master.Presenter;

import com.example.barbershop.Logic.DataService.Objects.Callback.WorkModeCallback;
import com.example.barbershop.Logic.DataService.Objects.WorkMode;
import com.example.barbershop.modules.Master.Interactor.MasterInteractorInput;
import com.example.barbershop.modules.Master.Interactor.MasterInteractorOutput;
import com.example.barbershop.modules.Master.Interactor.MasterViewItem;
import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;
import com.example.barbershop.modules.Master.Router.MasterRouterInput;
import com.example.barbershop.modules.Master.View.MasterViewInterface;
import com.example.barbershop.modules.Master.View.MasterViewOutput;


public class MasterPresenter implements MasterViewOutput, MasterInteractorOutput {

    private MasterRouterInput router;
    private MasterInteractorInput interactor;
    private MasterViewInterface userInterface;

    private boolean isForCreate;
    private boolean isForMyProfile;
    private boolean isForEdit;


    public void setRouter(MasterRouterInput router) {
        this.router = router;
    }

    public void setInteractor(MasterInteractorInput interactor) {
        this.interactor = interactor;
    }

    public void setViewInterface(MasterViewInterface userInterface) {
        this.userInterface = userInterface;
    }

    public void configureForCreate() {
        isForEdit = true;
        isForMyProfile = false;
        isForCreate = true;
    }

    public void configureForEdit() {
        isForEdit = true;
        isForMyProfile = false;
        isForCreate = false;
    }

    public void configureForMyProfile() {
        isForEdit = false;
        isForMyProfile = true;
        isForCreate = false;
    }

    @Override
    public void viewDidStarted() {

        if (isForCreate) {
            userInterface.setEditMode(isForEdit, isForCreate);
            interactor.createMaster();

        } else {
            interactor.loadMaster();
        }
    }

    //MasterViewOutput

    @Override
    public void viewDidTapRegistrationButton(String surname, String name, String patronymic,
                                             String phone, String email, String password, String workingHours) {

        if (surname.isEmpty() || name.isEmpty() || patronymic.isEmpty() || email.isEmpty() || password.isEmpty() || workingHours.isEmpty()) {
            router.showInfoAlert("Поля: Фамилия, Имя, Отчество, Email, Password, Режим работы - обязательны для заполнения!");

        } else {
            interactor.registrationMaster(surname, name, patronymic, phone, email, password, workingHours);
        }
    }

    @Override
    public void viewDidTapSaveButton(final String surname, final String name, final String patronymic,
                                     final String phone, final String email, final String password, final String workingHours) {
        router.showEditAlert("Сохранить изменения?", new EditCallBack() {
            @Override
            public void done(boolean ok) {
                if (ok) {
                    interactor.updateMaster(surname, name, patronymic, phone, email, password, workingHours);
                }
            }
        });
    }

    @Override
    public void
    viewDidTapDeleteButton() {
        router.showEditAlert("Пользователь будет удален!", new EditCallBack() {
            @Override
            public void done(boolean ok) {
                if (ok) {
                    interactor.deleteMaster();
                }
            }
        });
    }

    @Override
    public void viewDidTapExitButton() {
        router.showEditAlert("Выйти из аккаунта?", new EditCallBack() {
            @Override
            public void done(boolean ok) {
                if (ok) {
                    interactor.logoutMaster();
                }
            }
        });
    }

    @Override
    public void viewDidTapAddPhotoButtonButton() {
        router.presentCreatePhoto(interactor.getId(), interactor.getIdToken());
    }

    @Override
    public void viewDidTapReplacePhotoButton() {
        router.showEditAlert("Заменить фото?", new EditCallBack() {
            @Override
            public void done(boolean ok) {
                if (ok) {
                    interactor.removePhoto();
                }
            }
        });
    }

    @Override
    public void viewNeedsUpdateData(String surname, String name, String patronymic, String phone, String email, String password) {
        interactor.updateData(surname, name, patronymic, phone, email, password);
    }

    @Override
    public void viewDidTapWorkMode(String workMode) {

        if (isForCreate || workMode.isEmpty()) {
            router.presentCreateWorkMode(interactor.getId(), new WorkModeCallback() {
                @Override
                public void done(WorkMode workMode) {
                    interactor.updateWorkMode(workMode);
                }
            });
        } else {
            router.presentWorkMode(interactor.getId());
        }
    }

    @Override
    public void viewDidTapServicesCardViewForBrowse() {
        router.presentServicesBarberForBrowse(interactor.getId(), interactor.getIdToken(), interactor.isAdmin(), interactor.isMaster());
    }

    @Override
    public void viewDidTapInfoButton() {
        router.presentInfoFragment();
    }

    //MasterInteractorOutput

    @Override
    public void interactorDidLoadViewItem(MasterViewItem viewItem) {
        userInterface.showMasterViewItem(viewItem, isForMyProfile, isForCreate);
    }

    @Override
    public void interactorDidUpdateViewItem(MasterViewItem viewItem) {
        userInterface.showMasterViewItem(viewItem, isForMyProfile, isForCreate);
        userInterface.setEditMode(true, isForCreate);
    }

    @Override
    public void adminDidRegistrationNewMaster() {
        router.closeModule();
    }

    @Override
    public void interactorDidRegistrationNewMaster(String id, String idToken, boolean isAdmin, boolean isMaster) {
        configureForMyProfile();
    }

    @Override
    public void interactorDidDeleteMaster() {
        router.closeModule();
    }

    @Override
    public void interactorDidReceivedError(Error error) {
        router.showInfoAlert(error.toString());
    }

    @Override
    public void interactorDidLogoutMaster() {
        router.presentLoginModule();
    }

    @Override
    public void interactorDidDeletePhoto() {
        router.presentCreatePhoto(interactor.getId(), interactor.getIdToken());
    }
}
