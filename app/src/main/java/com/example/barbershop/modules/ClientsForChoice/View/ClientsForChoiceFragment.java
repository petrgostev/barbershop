package com.example.barbershop.modules.ClientsForChoice.View;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.bottomappbar.BottomAppBar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.barbershop.Logic.DataService.Objects.Callback.SelectedObjectCallback;
import com.example.barbershop.R;
import com.example.barbershop.modules.Client.Interactor.ClientViewItem;
import com.example.barbershop.modules.ClientsForChoice.Assembly.ClientsForChoiceAssembly;

import java.util.ArrayList;
import java.util.Objects;


public class ClientsForChoiceFragment extends Fragment implements ClientsForChoiceViewInterface {

    public SelectedObjectCallback callback;

    private ClientsForChoiceViewOutput viewOutput;

    private SearchView searchView;
    private RecyclerView recyclerView;

    private ClientViewAdapter adapter;

    private Toolbar toolbar;
    private BottomAppBar bottomAppBar;

    MenuItem itemEdit;
    MenuItem itemAdd;
    MenuItem itemSave;
    MenuItem itemDelete;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        ClientsForChoiceAssembly.configureModule(this);
    }

    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_for_search, container, false);

        toolbar = Objects.requireNonNull(getActivity()).findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        bottomAppBar = Objects.requireNonNull(getActivity()).findViewById(R.id.bottom_app_bar);
        bottomAppBar.setVisibility(View.GONE);

        TextView title = Objects.requireNonNull(getActivity()).findViewById(R.id.titleView);

        searchView = view.findViewById(R.id.search);
        recyclerView = view.findViewById(R.id.recycler);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                viewOutput.viewDidTapSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        title.setText("Выбор клиента");

        return view;
    }

    public void setViewOutput(ClientsForChoiceViewOutput viewOutput) {
        this.viewOutput = viewOutput;
    }

    @Override
    public void showViewItems(final ArrayList<ClientViewItem> viewItems) {

        adapter = new ClientViewAdapter(getContext(), viewItems);

        recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        adapter.setSelectListener(new ClientViewAdapter.Listener() {
            @Override
            public void onClick(int position) {
                ClientViewItem viewItem = viewItems.get(position);

                viewOutput.viewDidSelectItem(viewItem);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        inflater.inflate(R.menu.menu_main, menu);
        itemDelete = menu.findItem(R.id.delete);
        itemSave = menu.findItem(R.id.save);
        itemEdit = menu.findItem(R.id.edit);
        itemAdd = menu.findItem(R.id.add);

        itemSave.setVisible(false);
        itemDelete.setVisible(false);
        itemEdit.setVisible(false);
        itemAdd.setVisible(false);

        super.onCreateOptionsMenu(menu, inflater);
    }
}

