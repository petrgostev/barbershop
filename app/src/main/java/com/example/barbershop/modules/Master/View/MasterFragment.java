package com.example.barbershop.modules.Master.View;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.bottomappbar.BottomAppBar;
import android.support.design.button.MaterialButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.barbershop.R;
import com.example.barbershop.modules.Master.Assembly.MasterAssembly;
import com.example.barbershop.modules.Master.Interactor.MasterViewItem;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;


public class MasterFragment extends Fragment implements MasterViewInterface {
    private static String FOR_EDIT_KEY = "FOR_EDIT_KEY";
    private Bundle bundle;

    MasterViewOutput viewOutput;

    private EditText surnameET;
    private EditText nameET;
    private EditText patronymicET;
    private EditText phoneET;
    private EditText emailET;
    private EditText passwordET;
    private EditText workModeET;

    private MaterialButton registrationButton;
    private MaterialButton exitButton;
    private MaterialButton addPhotoButton;
    private MaterialButton writeButton;
    private MaterialButton infoButton;

    private ImageButton myProfileButton;
    private TextView myProfileTv;

    private CircleImageView photoIV;
    private ProgressBar progressBar;

    private Button servicesBtn;

    private boolean isAdmin = false;
    private boolean isMaster = false;
    private boolean isEdit = false;
    private boolean isForCreate = false;
    private boolean isWrite = false;
    private boolean isForMyProfile = false;

    private Toolbar toolbar;

    private TextView title;

    MenuItem itemEdit;
    MenuItem itemAdd;
    MenuItem itemSave;
    MenuItem itemDelete;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MasterAssembly.configureModule(this);

        setHasOptionsMenu(true);
    }

    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_master, container, false);

        toolbar = Objects.requireNonNull(getActivity()).findViewById(R.id.toolbar);

        title = Objects.requireNonNull(getActivity()).findViewById(R.id.titleView);

        surnameET = view.findViewById(R.id.surname);
        nameET = view.findViewById(R.id.name);
        patronymicET = view.findViewById(R.id.patronymic);
        phoneET = view.findViewById(R.id.phone);
        emailET = view.findViewById(R.id.email);
        passwordET = view.findViewById(R.id.password);
        workModeET = view.findViewById(R.id.work_mode);

        registrationButton = view.findViewById(R.id.registration_button);
        exitButton = view.findViewById(R.id.exit_button);
        addPhotoButton = view.findViewById(R.id.add_photo_button);
        writeButton = view.findViewById(R.id.write_button);
        infoButton = view.findViewById(R.id.info_button);

        myProfileButton = Objects.requireNonNull(getActivity()).findViewById(R.id.my_profile_button);
        myProfileTv = Objects.requireNonNull(getActivity()).findViewById(R.id.my_profile_tv);

        servicesBtn = view.findViewById(R.id.services_button);
        servicesBtn.setVisibility(View.GONE);

        photoIV = view.findViewById(R.id.photo);
        progressBar = view.findViewById(R.id.progress_bar);

        progressBar.setVisibility(View.GONE);

        BottomAppBar bottomAppBar = Objects.requireNonNull(getActivity()).findViewById(R.id.bottom_app_bar);
        bottomAppBar.setVisibility(View.VISIBLE);

        registrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewOutput.viewDidTapRegistrationButton(surnameET.getText().toString(),
                        nameET.getText().toString(),
                        patronymicET.getText().toString(),
                        phoneET.getText().toString(),
                        emailET.getText().toString(),
                        passwordET.getText().toString(),
                        workModeET.getText().toString());
            }
        });

        workModeET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEdit) {
                    viewOutput.viewNeedsUpdateData(surnameET.getText().toString(),
                            nameET.getText().toString(),
                            patronymicET.getText().toString(),
                            phoneET.getText().toString(),
                            emailET.getText().toString(),
                            passwordET.getText().toString());

                    viewOutput.viewDidTapWorkMode(workModeET.getText().toString());
                }
            }
        });

        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewOutput.viewDidTapExitButton();
            }
        });

        addPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewOutput.viewDidTapAddPhotoButtonButton();
            }
        });

        photoIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEdit) {
                    viewOutput.viewDidTapReplacePhotoButton();
                }
            }
        });

        servicesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isEdit && !isForCreate) viewOutput.viewDidTapServicesCardViewForBrowse();
            }
        });

        infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               viewOutput.viewDidTapInfoButton();
            }
        });

        title.setText("Регистрация");

        return view;
    }

    public void setViewOutput(MasterViewOutput viewOutput) {
        this.viewOutput = viewOutput;
    }

    @Override
    public void onStart() {
        super.onStart();
        viewOutput.viewDidStarted();

        if (isForMyProfile) enableIcon(true);

    }

    @Override
    public void onPause() {
        super.onPause();
        enableIcon(false);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        bundle = outState;
        bundle.putBoolean(FOR_EDIT_KEY, isEdit);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (bundle != null) {
            isEdit = bundle.getBoolean(FOR_EDIT_KEY);
            setEditMode(isEdit, isForCreate);
        }
    }

    @Override
    public void showMasterViewItem(final MasterViewItem viewItem, boolean isForMyProfile, boolean isForCreate) {
        this.isForMyProfile = isForMyProfile;
        this.isForCreate = isForCreate;

        String titleStr = viewItem.name.isEmpty() ? "Профиль пользователя" : viewItem.fullName;
        title.setText(titleStr);

        toolbar.setVisibility(View.VISIBLE);
        registrationButton.setVisibility(View.GONE);

        isAdmin = viewItem.isAdmin;
        isMaster = viewItem.isMaster;

        infoButton.setVisibility(isForMyProfile ? View.GONE : View.VISIBLE);

        if (!viewItem.src.equals("")) {
            progressBar.setVisibility(View.VISIBLE);
            photoIV.setVisibility(View.VISIBLE);
            addPhotoButton.setVisibility(View.GONE);

            Picasso.with(photoIV.getContext())
                    .load(viewItem.src)
                    .into(photoIV);
        }

        if (!isForMyProfile) {
            addPhotoButton.setVisibility(View.GONE);
            exitButton.setVisibility(View.GONE);
            passwordET.setVisibility(View.GONE);
        }

        if (isForCreate || isEdit) {
            servicesBtn.setVisibility(View.GONE);

        } else {
            servicesBtn.setVisibility(View.VISIBLE);
        }

        workModeET.post(new Runnable() {
            @Override
            public void run() {
                surnameET.setText(viewItem.surname);
                nameET.setText(viewItem.name);
                phoneET.setText(viewItem.phone);
                emailET.setText(viewItem.email);
                passwordET.setText(viewItem.password);
                workModeET.setText(viewItem.workMode);
            }
        });
    }

    @Override
    public void setEditMode(boolean edit, boolean isForCreate) {
        this.isEdit = edit;
        this.isForCreate = isForCreate;

        surnameET.setEnabled(edit);
        nameET.setEnabled(edit);
        patronymicET.setEnabled(edit);
        phoneET.setEnabled(edit);
        emailET.setEnabled(edit);
        passwordET.setEnabled(edit);
        workModeET.setEnabled(edit);
        addPhotoButton.setEnabled(isEdit);

        addPhotoButton.setText(isEdit ? "Добавить фото" : "Нет фото");

        if (edit || isForCreate) {
            exitButton.setVisibility(View.GONE);
        }

        if (isWrite) {
            writeButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        inflater.inflate(R.menu.menu_main, menu);
        itemDelete = menu.findItem(R.id.delete);
        itemSave = menu.findItem(R.id.save);
        itemEdit = menu.findItem(R.id.edit);
        itemAdd = menu.findItem(R.id.add);

        itemEdit.setVisible(isForMyProfile);

        if (!isForCreate) {
            itemSave.setVisible(isEdit);

        } else {
            itemSave.setVisible(false);
        }

        itemAdd.setVisible(false);

        if (isAdmin && !isForMyProfile && !isForCreate) {
            itemDelete.setVisible(true);

        } else {
            itemDelete.setVisible(false);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.edit) {
            isEdit = true;

            itemSave.setVisible(true);

            if (isMaster && !isAdmin) {
                itemDelete.setVisible(false);
            } else {
                itemDelete.setVisible(true);
            }

            itemEdit.setVisible(false);

            setEditMode(true, isForCreate);
        }

        if (id == R.id.save) {
            itemSave.setVisible(false);
            itemDelete.setVisible(false);
            itemEdit.setVisible(true);

            setEditMode(false, isForCreate);

            viewOutput.viewDidTapSaveButton(surnameET.getText().toString(),
                    nameET.getText().toString(),
                    patronymicET.getText().toString(),
                    phoneET.getText().toString(),
                    emailET.getText().toString(),
                    passwordET.getText().toString(),
                    workModeET.getText().toString());
        }

        if (id == R.id.delete) {
            itemSave.setVisible(false);
            itemDelete.setVisible(false);

            if (!isAdmin) {
                itemEdit.setVisible(true);
            }

            setEditMode(false, isForCreate);

            viewOutput.viewDidTapDeleteButton();
        }

        return super.onOptionsItemSelected(item);
    }

    //Private

    private void enableIcon(boolean enable) {
        if (enable) {
            myProfileButton.setColorFilter(0xffff0000);
            myProfileTv.setTextColor(0xffff0000);

        } else {
            myProfileButton.setColorFilter(0xffffffff);
            myProfileTv.setTextColor(0xffffffff);
        }
    }
}
