package com.example.barbershop.modules.ClientWritesList.Assembly;

import android.os.Bundle;

import com.example.barbershop.Logic.ClientService.ClientService;
import com.example.barbershop.Logic.ClientService.ClientServiceInterface;
import com.example.barbershop.Logic.DataService.DataService;
import com.example.barbershop.Logic.DataService.DataServiceInterface;
import com.example.barbershop.Logic.MasterService.MasterService;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.modules.ClientWritesList.Interactor.ClientWritesListInteractor;
import com.example.barbershop.modules.ClientWritesList.Presenter.ClientWritesListPresenter;
import com.example.barbershop.modules.ClientWritesList.Router.ClientWritesListRouter;
import com.example.barbershop.modules.ClientWritesList.View.ClientWritesListFragment;

public class ClientWritesListAssembly {

    private static String ID_TOKEN_KEY = "ID_TOKEN_KEY";
    private static String OWNER_ID_KEY = "ID_KEY";

    public static ClientWritesListFragment MyWritesListModule(String ownerId, String idToken) {

        ClientWritesListFragment fragment = new ClientWritesListFragment();

        Bundle args = new Bundle();

        args.putString(OWNER_ID_KEY, ownerId);
        args.putString(ID_TOKEN_KEY, idToken);

        fragment.setArguments(args);
        return fragment;
    }

    public static void configureModule(ClientWritesListFragment fragment) {
        Bundle args = fragment.getArguments();

        String ownerId = "";
        String idToken = "";

        if (args != null) {
            ownerId = args.getString(OWNER_ID_KEY);
            idToken = args.getString(ID_TOKEN_KEY);
        }

        ClientServiceInterface clientService = ClientService.getInstance(fragment.getContext());
        MasterServiceInterface masterService = MasterService.getInstance(fragment.getContext());
        DataServiceInterface dataService = DataService.getInstance(fragment.getContext());

        ClientWritesListInteractor interactor = new ClientWritesListInteractor(ownerId, idToken, clientService, masterService, dataService);
        ClientWritesListPresenter presenter = new ClientWritesListPresenter();
        ClientWritesListRouter router = new ClientWritesListRouter(fragment.getActivity());

        fragment.setViewOutput(presenter);

        presenter.setRouter(router);
        presenter.setInteractor(interactor);
        presenter.setViewInterface(fragment);

        interactor.setOutput(presenter);
    }
}
