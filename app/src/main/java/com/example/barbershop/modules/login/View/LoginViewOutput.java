package com.example.barbershop.modules.login.View;


public interface LoginViewOutput {
    void didTapauthenticationButton(String login, String password);

    void didTapRegisterButton();
}
