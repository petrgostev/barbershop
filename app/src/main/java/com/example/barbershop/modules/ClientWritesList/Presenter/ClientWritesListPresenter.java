package com.example.barbershop.modules.ClientWritesList.Presenter;

import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;
import com.example.barbershop.modules.ClientWrite.Interactor.ClientWriteViewItem;
import com.example.barbershop.modules.ClientWritesList.Interactor.ClientWritesListInteractorInput;
import com.example.barbershop.modules.ClientWritesList.Interactor.ClientWritesListInteractorOutput;
import com.example.barbershop.modules.ClientWritesList.Router.ClientWritesListRouterInput;
import com.example.barbershop.modules.ClientWritesList.View.ClientWritesListViewInterface;
import com.example.barbershop.modules.ClientWritesList.View.ClientWritesListViewOutput;

import java.util.ArrayList;


public class ClientWritesListPresenter implements ClientWritesListViewOutput, ClientWritesListInteractorOutput {

    private ClientWritesListRouterInput router;
    private ClientWritesListInteractorInput interactor;
    private ClientWritesListViewInterface userInterface;

    public void setRouter(ClientWritesListRouterInput router) {
        this.router = router;
    }

    public void setInteractor(ClientWritesListInteractorInput interactor) {
        this.interactor = interactor;
    }

    public void setViewInterface(ClientWritesListViewInterface userInterface) {
        this.userInterface = userInterface;
    }

    //ClientWritesListViewOutput


    @Override
    public void viewDidStarted() {
        interactor.loadWrites();
    }

    @Override
    public void viewDidTapCreateNewWrite() {
        router.presentClientWriteFragment(interactor.getOwnerId(), interactor.getIdToken());
    }

    @Override
    public void viewDidTapDeleteButton(final ClientWriteViewItem viewItem) {
        String title = "Удалить запись на " + viewItem.date + " на " + viewItem.time + "?";
        router.showEditAlert(title, new EditCallBack() {
            @Override
            public void done(boolean ok) {
            if (ok) interactor.deleteWrite(viewItem.id, viewItem.masterId);
            }

        });
    }

    @Override
    public void viewDidTapMyProfile() {
        router.presentClientFragment(interactor.getOwnerId(), interactor.getIdToken());
    }

    @Override
    public void viewDidTapMyWrites() {
        router.presentClientWritesListFragment(interactor.getOwnerId(), interactor.getIdToken());
    }

    @Override
    public void viewDidTapMasters() {
        router.presentMastersListFragment(false, false, interactor.getIdToken());
    }

    @Override
    public void viewDidTapServices() {
        router.presentServicesBarberListFragment(null, interactor.getIdToken(), false, false);
    }

    //ClientWritesListInteractorOutput

    @Override
    public void interactorDidLoadViewItems(ArrayList<ClientWriteViewItem> viewItems) {
        userInterface.showViewItems(viewItems);
    }
}
