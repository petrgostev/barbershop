package com.example.barbershop.modules.ServicesBarberList.Router;


import com.example.barbershop.Logic.DataService.Objects.Callback.EditCallBack;
import com.example.barbershop.Logic.DataService.Objects.Callback.SelectedObjectCallback;

public interface ServicesBarberListRouterInput {

    void showServiceBarberModule(String id, String masterId, String idToken, boolean isAdmin, boolean isMaster,
                                 boolean isForCreate, boolean isBindToMaster);

    void closeModule();

    void presentServicesBarberForChoice(String masterId, boolean isBindToMaster, SelectedObjectCallback callback);

    void showInfoAlert(String title);

    void showEditAlert(String title, EditCallBack callBack);

    void presentClientWriteFragment(String ownerId, String serviceId, String idToken);
}
