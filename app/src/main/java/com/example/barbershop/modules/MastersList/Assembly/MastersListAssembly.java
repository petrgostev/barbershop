package com.example.barbershop.modules.MastersList.Assembly;

import android.os.Bundle;

import com.example.barbershop.Logic.DataService.Objects.Callback.SelectedObjectCallback;
import com.example.barbershop.Logic.MasterService.MasterService;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.modules.MastersList.Interactor.MastersListInteractor;
import com.example.barbershop.modules.MastersList.Presenter.MastersListPresenter;
import com.example.barbershop.modules.MastersList.Router.MastersListRouter;
import com.example.barbershop.modules.MastersList.View.MastersListFragment;


public class MastersListAssembly {

    private static String IS_ADMIN_KEY = "IS_ADMIN_KEY";
    private static String IS_MASTER_KEY = "IS_MASTER_KEY";
    private static String ID_TOKEN_KEY = "ID_TOKEN_KEY";

    public static MastersListFragment MastersListModule(boolean isAdmin, boolean isMaster, String idToken) {

        MastersListFragment fragment = new MastersListFragment();

        Bundle args = new Bundle();

        args.putBoolean(IS_ADMIN_KEY, isAdmin);
        args.putBoolean(IS_MASTER_KEY, isMaster);
        args.putString(ID_TOKEN_KEY, idToken);

        fragment.setArguments(args);
        return fragment;
    }

    public static MastersListFragment MastersListModuleForSelectMaster(SelectedObjectCallback callback) {
        MastersListFragment fragment = new MastersListFragment();
        fragment.callback = callback;
        return fragment;
    }

    public static void configureModule(MastersListFragment fragment) {
        Bundle args = fragment.getArguments();

        boolean isAdmin = false;
        boolean isMaster = false;
        String idToken = "";

        if (args != null) {
            isAdmin = args.getBoolean(IS_ADMIN_KEY);
            isMaster = args.getBoolean(IS_MASTER_KEY);
            idToken = args.getString(ID_TOKEN_KEY);
        }

        MasterServiceInterface masterService = MasterService.getInstance(fragment.getContext());
        MastersListInteractor interactor = new MastersListInteractor(idToken, masterService);
        MastersListPresenter presenter = new MastersListPresenter();
        MastersListRouter router = new MastersListRouter(fragment.getActivity());

        fragment.setViewOutput(presenter);

        presenter.setRouter(router);
        presenter.setInteractor(interactor);
        presenter.setViewInterface(fragment);
        presenter.selectedCallback = fragment.callback;

        interactor.setOutput(presenter);

        if (isAdmin && isMaster) {
            presenter.configureForAdmin();
        }

        if (!isAdmin && isMaster) {
            presenter.configureForMaster();
        }

        if (!isAdmin && !isMaster) {
            presenter.configureForClient();
        }
    }
}
