package com.example.barbershop.modules.ServiceBarber.Assembly;

import android.os.Bundle;

import com.example.barbershop.Logic.DataService.DataService;
import com.example.barbershop.Logic.DataService.DataServiceInterface;
import com.example.barbershop.Logic.MasterService.MasterService;
import com.example.barbershop.Logic.MasterService.MasterServiceInterface;
import com.example.barbershop.modules.ServiceBarber.Interactor.ServiceBarberInteractor;
import com.example.barbershop.modules.ServiceBarber.Presenter.ServiceBarberPresenter;
import com.example.barbershop.modules.ServiceBarber.Router.ServiceBarberRouter;
import com.example.barbershop.modules.ServiceBarber.View.ServiceBarberFragment;


public class ServiceBarberAssembly {

    private static String IS_ADMIN_KEY = "IS_ADMIN_KEY";
    private static String IS_MASTER_KEY = "IS_MASTER_KEY";
    private static String ID_KEY = "ID_KEY";
    private static String MASTER_ID_KEY = "MASTER_ID_KEY";
    private static String ID_TOKEN_KEY = "ID_TOKEN_KEY";

    public static ServiceBarberFragment serviceBarberModule(String id,String masterId,  String idToken, boolean isAdmin,
                                                           boolean isMaster, boolean isForCreate, boolean isBindToMaster) {
        ServiceBarberFragment fragment = new ServiceBarberFragment();

        Bundle args = new Bundle();

        String IS_FOR_CREATE_KEY = "IS_FOR_CREATE_KEY";
        String IS_BIND_TO_MASTER_KEY = "IS_BIND_TO_MASTER_KEY";

        args.putString(ID_KEY, id);
        args.putString(MASTER_ID_KEY, masterId);
        args.putBoolean(IS_FOR_CREATE_KEY, isForCreate);
        args.putBoolean(IS_ADMIN_KEY, isAdmin);
        args.putBoolean(IS_MASTER_KEY, isMaster);
        args.putBoolean(IS_BIND_TO_MASTER_KEY, isBindToMaster);
        args.putString(ID_TOKEN_KEY, idToken);

        fragment.setArguments(args);

        return fragment;
    }

    public static void configureModule(ServiceBarberFragment fragment) {
        Bundle args = fragment.getArguments();

        String id = null;
        String masterId = null;
        String idToken = null;
        boolean isAdmin = false;
        boolean isMaster = false;

        if (args != null) {
            id = args.getString(ID_KEY);
            masterId = args.getString(MASTER_ID_KEY);
            idToken = args.getString(ID_TOKEN_KEY);
            isAdmin = args.getBoolean(IS_ADMIN_KEY);
            isMaster = args.getBoolean(IS_MASTER_KEY);
        }

        MasterServiceInterface masterService = MasterService.getInstance(fragment.getContext());
        DataServiceInterface dataService = DataService.getInstance(fragment.getContext());

        ServiceBarberInteractor interactor = new ServiceBarberInteractor(id, masterId, idToken,isAdmin, isMaster, masterService, dataService);
        ServiceBarberPresenter presenter = new ServiceBarberPresenter();
        ServiceBarberRouter router = new ServiceBarberRouter(fragment.getActivity());

        fragment.setViewOutput(presenter);

        presenter.setRouter(router);
        presenter.setInteractor(interactor);
        presenter.setViewInterface(fragment);

        interactor.setOutput(presenter);

        if (id != null) {
            presenter.configureForShow();
        } else {
            presenter.configureForEdit();
        }
    }
}
