package com.example.barbershop.modules.ClientWritesList.Interactor;


public interface ClientWritesListInteractorInput {
    String getIdToken();

    String getOwnerId();

    void loadWrites();

    void deleteWrite(String id, String masterId);
}