package com.example.barbershop.modules.ClientWritesList.View;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.barbershop.R;
import com.example.barbershop.modules.ClientWrite.Interactor.ClientWriteViewItem;

import java.util.ArrayList;


public class ClientWriteAdapter extends RecyclerView.Adapter<ClientWriteAdapter.ViewHolder>{

    private ArrayList<ClientWriteViewItem> viewItems;

    private ClientWriteAdapter.Listener selectListener;

    public ClientWriteAdapter(Context context, ArrayList<ClientWriteViewItem> viewItems) {
        this.viewItems = viewItems;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        ViewHolder(CardView cardView) {
            super(cardView);
            this.cardView = cardView;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        CardView cv = (CardView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.client_write_card, viewGroup, false);

        return new ViewHolder(cv);
    }

    @Override
    public void onBindViewHolder(@NonNull ClientWriteAdapter.ViewHolder viewHolder, int position) {
        CardView cardView = viewHolder.cardView;

        configureView(cardView, position, viewHolder);
    }

    private void configureView(final CardView cardView, int position, final ViewHolder viewHolder) {
        final ClientWriteViewItem item = viewItems.get(position);

        final TextView dateTV = cardView.findViewById(R.id.date);
        final TextView timeTV = cardView.findViewById(R.id.time);
        final TextView serviceTV = cardView.findViewById(R.id.service);
        final TextView masterTV = cardView.findViewById(R.id.master);
        final TextView priceTV = cardView.findViewById(R.id.price);

        dateTV.setText(item.date);
        timeTV.setText(item.time);
        serviceTV.setText(item.service);
        masterTV.setText(item.master);
        priceTV.setText(item.price);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectListener != null) {
                    selectListener.onClick(viewHolder.getAdapterPosition());
                }
            }
        });

        cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                cardView.setBackgroundColor(Color.RED);
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return viewItems.size();
    }

    public interface Listener {
        void onClick(int position);
    }

    public void setSelectListener(Listener selectListener) {
        this.selectListener = selectListener;
    }
}
