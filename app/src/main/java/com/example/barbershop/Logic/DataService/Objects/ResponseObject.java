package com.example.barbershop.Logic.DataService.Objects;

import com.example.barbershop.Logic.ClientService.Objects.Client;
import com.example.barbershop.Logic.MasterService.Objects.Master;
import com.example.barbershop.Logic.MasterService.Objects.ServiceBarber;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


public class ResponseObject  {

    public Map<String, Master> masters;
    public Map<String, Client> clients;
    public String superUsers;
    public Map<String, Write> writes;
    public Map<String, ServiceBarber> servicesBarber;

    public ArrayList<Master> getMasters() {
        return new ArrayList<>(masters.values());
    }

    public ArrayList<Client> getClients() {
        return new ArrayList<> (clients.values());
    }

    public ArrayList<String> getSuperUsers() {
        List<String> superUsersList = Arrays.asList(superUsers.split(","));


        return new ArrayList<>(superUsersList);
    }

    public ArrayList<Write> getWrites() {
        return new ArrayList<> (writes.values());
    }

    public ArrayList<ServiceBarber> getServicesBarber() {
        return new ArrayList<> (servicesBarber.values());
    }
}
