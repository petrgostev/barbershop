package com.example.barbershop.Logic.MasterService;


public interface RefreshTokenCallback {
    void done(Error error, String refreshToken);
}
