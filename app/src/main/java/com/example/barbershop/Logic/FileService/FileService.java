package com.example.barbershop.Logic.FileService;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Path;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;


import com.example.barbershop.Logic.ClientService.ErrorExtractor;
import com.example.barbershop.Logic.FileService.Objects.Photo;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FileService implements FileServiceInterface {

    private final static String API_KEY = "AIzaSyBHoaAAFVpnobgsOW4d-w7Nzpu7HkHTOo0";
    private final static String EMAIL_KEY = "EMAIL_KEY";
    private final static String PASSWORD_KEY = "PASSWORD_KEY";
    private final static String BASE_URL_KEY = "https://firebasestorage.googleapis.com/v0/b/barbershop-2614a.appspot.com/o/";

    private String idToken;

    @SuppressLint("StaticFieldLeak")
    private static FileService ourInstance;

    private Context context;

    public static FileService getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new FileService(context);
        }

        return ourInstance;
    }

    private FileService(Context context) {
        this.context = context;
    }

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL_KEY)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private Retrofit retrofit_2 = new Retrofit.Builder()
            .baseUrl("https://console.firebase.google.com/project/barbershop-2614a/storage/barbershop-2614a.appspot.com/files/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @Override
    public void savePhoto(final String ownerId, byte[] jpegByte, boolean isMaster, final PhotoUrlCallback callback) {

        Date currentTime = Calendar.getInstance().getTime();
        final String imageName = "image" + String.valueOf(currentTime.getTime()) + ".jpg";

        final File image = new File(Environment.getExternalStorageDirectory(), imageName);

        try {
            FileUtils.writeByteArrayToFile(image, jpegByte);
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestBody requestFile =
                RequestBody.create(MediaType.parse("image/jpeg"), image);

        savePhotoToFirebase(ownerId, imageName, requestFile, new FileCallback() {
            @Override
            public void done(Error error, Photo photo) {
                if (error == null && photo != null) {
                    String url = BASE_URL_KEY + ownerId + "%2F" + photo.getNameForUrl();
                    callback.done(null, url, photo);
                } else {
                    callback.done(new Error("photo - null"), null, null);
                }
            }
        });
    }

    @Override
    public void deletePhoto(String ownerId, String imageName, final FileCallback callback) {
        deletePhotoForFirebase(ownerId, imageName, new FileCallback() {
            @Override
            public void done(Error error, Photo photo) {
                if (error == null) {
                    callback.done(null, null);

                } else {
                    callback.done(new Error("delete-error"), null);
                }
            }
        });
    }

    private void savePhotoToFirebase(String ownerId, String imageName, RequestBody requestFile, final FileCallback callback) {
        Call<Photo> call = createRequestForPutPhoto(ownerId, imageName, requestFile);

        call.enqueue(new Callback<Photo>() {
            @Override
            public void onResponse(Call<Photo> call, Response<Photo> response) {
                if (response.isSuccessful() && response.body() != null) {

                    callback.done(null, response.body());
                    Toast.makeText(context, "OK", Toast.LENGTH_SHORT).show();
                } else {
                    callback.done(new Error("photo - null"), null);
                }
            }

            @Override
            public void onFailure(Call<Photo> call, Throwable t) {
                Toast.makeText(context, "NO", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deletePhotoForFirebase(String ownerId, String imageName, final FileCallback callback) {
        Call<ResponseBody> call = createRequestForDeletePhoto(ownerId, imageName);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful() && response.body() == null) {
                    //if code 204
                    callback .done(null, null);
                    Toast.makeText(context, "Удалено", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, "NO", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Call<Photo> createRequestForPutPhoto(String ownerId, String imageName, RequestBody requestFile) {

        FileApi fileApi = retrofit.create(FileApi.class);

        return fileApi.upload(ownerId, imageName, requestFile);
    }

    private Call<ResponseBody> createRequestForDeletePhoto(String ownerId, String imageName) {

        FileApi fileApi = retrofit.create(FileApi.class);
        String request = ownerId + "/" + imageName ;

        return fileApi.deletePackagePhoto(imageName);
    }
}
