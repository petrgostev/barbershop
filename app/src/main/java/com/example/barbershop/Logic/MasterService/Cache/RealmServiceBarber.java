package com.example.barbershop.Logic.MasterService.Cache;

import com.example.barbershop.Logic.MasterService.Objects.ServiceBarber;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class RealmServiceBarber extends RealmObject {

    @PrimaryKey
    private String id;

    private String name;
    private double price;
    private double timeInterval;

    public ServiceBarber getServiceBarber() {
        return new ServiceBarber(id, name, price, timeInterval);
    }

    public void configureWithServiceBarber(ServiceBarber service) {
        this.id = service.getId();
        this.name = service.getName();
        this.price = service.getPrice();
        this.timeInterval = service.getTimeInterval();
    }
}
