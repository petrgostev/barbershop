package com.example.barbershop.Logic.DataService;

import com.example.barbershop.Logic.DataService.Objects.Callback.ServiceBarberCallback;
import com.example.barbershop.Logic.DataService.Objects.Callback.ServicesBarberCallback;
import com.example.barbershop.Logic.DataService.Objects.Callback.WriteCallback;
import com.example.barbershop.Logic.DataService.Objects.Callback.WritesCallback;
import com.example.barbershop.Logic.MasterService.Objects.ServiceBarber;
import com.example.barbershop.Logic.DataService.Objects.TimeInterval;
import com.example.barbershop.Logic.DataService.Objects.Write;

import java.util.ArrayList;


public interface DataServiceInterface {

    void saveWrite(Write write, String idToken, WriteCallback writeCallback);

    void getClientWrites(String ownerId, String idToken, WritesCallback writesCallback);

    void deleteWrite(String id, WriteCallback callback);

    void getWriteById(String id, WriteCallback callback);

    void getMasterWrites(String ownerId, String idToken, WritesCallback callback);

    void saveServiceBarber(ServiceBarber serviceBarber, ServiceBarberCallback serviceBarberCallback);

    void getServicesBarber(String idToken, ServicesBarberCallback servicesBarberCallback);

    void getServiceBarberById(String id, String idToken, ServiceBarberCallback callback);

    void updateServiceBarber(ServiceBarber serviceBarber, ServiceBarberCallback callback);

    void deleteServiceBarber(String id, ServiceBarberCallback callback);

    void getServicesBarberByIds(ArrayList<String> serviceBarberIds, ServicesBarberCallback callback);

    TimeInterval getTimeIntervalById(String timeIntervalId);

    void saveTimeIntervals(ArrayList<TimeInterval> timeIntervals);
}
