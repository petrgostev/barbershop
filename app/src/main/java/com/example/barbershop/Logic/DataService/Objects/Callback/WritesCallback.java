package com.example.barbershop.Logic.DataService.Objects.Callback;

import com.example.barbershop.Logic.DataService.Objects.Write;

import java.util.ArrayList;


public interface WritesCallback {
    void done(Error error, ArrayList<Write> writes);
}
