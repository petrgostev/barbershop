package com.example.barbershop.Logic.DataService.Objects.Callback;


public interface TimeCallback {
    void done(String timeInterval);
}
