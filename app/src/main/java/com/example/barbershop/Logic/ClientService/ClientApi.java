package com.example.barbershop.Logic.ClientService;

import com.example.barbershop.Logic.ClientService.Objects.Client;
import com.example.barbershop.Logic.UserService.Objects.ResponseUserForToken;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ClientApi {

    @Headers("Content-Type: application/json")
    @PUT("clients/{name}.json")
    Call<Client> clientCallForPutDB(@Body Client client,
                                    @Path("name") String id,
                                    @Query("idToken") String idToken);

    @Headers("Content-Type: application/json")
    @GET("clients.json")
    Call<HashMap<String, Client>> clientsCallForGetDB(@Query("idToken") String idToken);

    @Headers("Content-Type: application/json")
    @PUT("clients/{name}.json")
    Call<Client> clientCallForUpdateDB(@Body Client client,
                                       @Path("name") String id,
                                       @Query("idToken") String idToken);

    @Headers("Content-Type: application/json")
    @DELETE("clients/{id}.json")
    Call<Client> clientCallForRemoveDB(@Path("id") String id,
                                       @Query("idToken") String idToken);
}
