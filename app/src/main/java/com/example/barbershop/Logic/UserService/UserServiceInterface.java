package com.example.barbershop.Logic.UserService;

public interface UserServiceInterface {

    void authenticationSavedUser(final UserCallback callback);

    void authenticationUser(String login, String password, UserCallback callback);

    void registrationNewUser(boolean isAdmin, String email, String password, UserCallback callback);

    void logoutUser(UserCallback callback);

    void deleteUser(String idToken, UserCallback callback);

    void authenticationUserForAdmin(String email, String password, UserCallback callback);
}
