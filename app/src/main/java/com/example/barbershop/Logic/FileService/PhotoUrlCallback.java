package com.example.barbershop.Logic.FileService;


import com.example.barbershop.Logic.FileService.Objects.Photo;

public interface PhotoUrlCallback {
    void done(Error error, String url, Photo photo);
}
