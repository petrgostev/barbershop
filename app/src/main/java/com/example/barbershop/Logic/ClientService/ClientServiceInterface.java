package com.example.barbershop.Logic.ClientService;

import com.example.barbershop.Logic.ClientService.Objects.Callback.ClientCallback;
import com.example.barbershop.Logic.ClientService.Objects.Callback.ClientsCallback;
import com.example.barbershop.Logic.ClientService.Objects.Client;

public interface ClientServiceInterface {
    void registrationNewClient(String idToken, String id, String surname, String name,
                               String phone, String password, String email,
                               ClientCallback clientCallback);

    void getClientById(String id, String idToken, ClientCallback clientCallback);

    void updateClient(Client client, ClientCallback callback);

    void getClientsByName(String query, String idToken, ClientsCallback clientsCallback);

    void deleteClient(String id, ClientCallback callback);
}
