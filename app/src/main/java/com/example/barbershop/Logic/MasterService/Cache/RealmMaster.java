package com.example.barbershop.Logic.MasterService.Cache;

import com.example.barbershop.Logic.MasterService.Objects.ServiceBarber;
import com.example.barbershop.Logic.MasterService.Objects.Master;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class RealmMaster extends RealmObject {

    @PrimaryKey
    private String id;

    private String idToken;
    private String name;
    private String surname;
    private String phone;
    private String email;
    private String password;
    private String photoSrc;
    private String photoName;
    private Boolean isAdmin;

    private RealmList<RealmServiceBarber> services;

    public Master getMaster() {

        ArrayList<ServiceBarber> services = new ArrayList<>();

        for (RealmServiceBarber service : this.services) {
            services.add(service.getServiceBarber());
        }

        return new Master(idToken, id, name, surname, phone,
                email, password, photoSrc, photoName, services, isAdmin);
    }

    public void configureWithMaster(Master master) {

        if (this.id == null) {
            this.id = master.getId();
        }

        this.idToken = master.getIdToken();
        this.name = master.getName();
        this.surname = master.getSurname();
        this.phone = master.getPhone();
        this.email = master.getEmail();
        this.password = master.getPassword();
        this.photoSrc = master.getPhotoSrc();
        this.photoName = master.getPhotoName();
        this.isAdmin = master.isAdmin();

        this.services.clear();

        for (RealmServiceBarber service : services) {
            service.deleteFromRealm();
        }

        for (ServiceBarber service : master.services) {
            RealmServiceBarber realmServiceBarber =
                    getRealm().createObject(RealmServiceBarber.class);

            realmServiceBarber.configureWithServiceBarber(service);
            this.services.add(realmServiceBarber);
        }
    }
}
