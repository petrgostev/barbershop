package com.example.barbershop.Logic.MasterService;

import com.example.barbershop.Logic.UserService.Objects.ResponseUserForToken;
import com.example.barbershop.Logic.MasterService.Objects.Master;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface MasterApi {

    @Headers("Content-Type: application/json")
    @PUT("masters/{name}.json")
    Call<Master> masterCallForPutDB(@Body Master user,
                                    @Path("name") String id);

    @Headers("Content-Type: application/json")
    @PUT("masters/{name}.json")
    Call<Master> masterCallForUpdateDB(@Body Master master,
                                       @Path("name") String id,
                                       @Query("idToken") String idToken);

    @Headers("Content-Type: application/json")
    @GET("masters.json")
    Call<HashMap<String, Master>> mastersCallForGetDB(@Query("idToken") String idToken);

    @Headers("Content-Type: application/json")
    @GET("settings/super_admins.json")
    Call<String> superAdminsCallForGetDB(@Query("idToken") String idToken);

    @Headers("Content-Type: application/json")
    @DELETE("masters/{id}.json")
    Call<Master> masterCallForRemoveDB(@Path("id") String id,
                                       @Query("idToken") String idToken);
}
