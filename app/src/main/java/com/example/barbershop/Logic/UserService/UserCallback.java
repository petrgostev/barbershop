package com.example.barbershop.Logic.UserService;


public interface UserCallback {
    void done(Error error, String id, String idToken, boolean isAdmin, boolean isMaster);
}
