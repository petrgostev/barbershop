package com.example.barbershop.Logic.FileService.Objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;


public class Photo {
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("bucket")
    @Expose
    private String bucket;

    @SerializedName("generation")
    @Expose
    private String generation;

    @SerializedName("metageneration")
    @Expose
    private String metageneration;

    @SerializedName("contentType")
    @Expose
    private String contentType;
    @SerializedName("timeCreated")
    @Expose
    private String timeCreated;

    @SerializedName("updated")
    @Expose
    private String updated;

    @SerializedName("storageClass")
    @Expose
    private String storageClass;

    @SerializedName("size")
    @Expose
    private String size;

    @SerializedName("md5Hash")
    @Expose
    private String md5Hash;

    @SerializedName("contentEncoding")
    @Expose
    private String contentEncoding;

    @SerializedName("contentDisposition")
    @Expose
    private String contentDisposition;

    @SerializedName("crc32c")
    @Expose
    private String crc32c;

    @SerializedName("etag")
    @Expose
    private String etag;

    @SerializedName("downloadTokens")
    @Expose
    private String downloadTokens;

    public String getName() {
        return name;
    }

    public String getNameForUrl() {
        String[] names = getName().split("/");
        return names[1] + "?alt=media&token=" + getDownloadTokens();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getGeneration() {
        return generation;
    }

    public void setGeneration(String generation) {
        this.generation = generation;
    }

    public String getMetageneration() {
        return metageneration;
    }

    public void setMetageneration(String metageneration) {
        this.metageneration = metageneration;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(String timeCreated) {
        this.timeCreated = timeCreated;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getStorageClass() {
        return storageClass;
    }

    public void setStorageClass(String storageClass) {
        this.storageClass = storageClass;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getMd5Hash() {
        return md5Hash;
    }

    public void setMd5Hash(String md5Hash) {
        this.md5Hash = md5Hash;
    }

    public String getContentEncoding() {
        return contentEncoding;
    }

    public void setContentEncoding(String contentEncoding) {
        this.contentEncoding = contentEncoding;
    }

    public String getContentDisposition() {
        return contentDisposition;
    }

    public void setContentDisposition(String contentDisposition) {
        this.contentDisposition = contentDisposition;
    }

    public String getCrc32c() {
        return crc32c;
    }

    public void setCrc32c(String crc32c) {
        this.crc32c = crc32c;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public String getDownloadTokens() {
        return downloadTokens;
    }

    public void setDownloadTokens(String downloadTokens) {
        this.downloadTokens = downloadTokens;
    }
}
