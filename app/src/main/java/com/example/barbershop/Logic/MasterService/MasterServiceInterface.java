package com.example.barbershop.Logic.MasterService;


import com.example.barbershop.Logic.MasterService.Objects.Master;


import io.realm.RealmList;

public interface MasterServiceInterface {

    void registrationNewMaster(String idToken, String id, String surname, String name,
                               String patronymic, String phone, String email, String password, MasterCallback callback);

    void getMasterById(String id, String idToken, MasterCallback callback);

    void deleteMaster(String id, String idToken, MasterCallback callback);

    void updateMaster(Master master, MasterCallback masterCallback);

    void getMasters(String idToken, MastersCallback callback);

    RealmList<Master> getLoadedMasters();
}
