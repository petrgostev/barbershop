package com.example.barbershop.Logic.DataService.Objects.Callback;


public interface EditCallBack {
    void done(boolean ok);
}
