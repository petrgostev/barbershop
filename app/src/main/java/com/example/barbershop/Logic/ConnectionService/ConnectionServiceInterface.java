package com.example.barbershop.Logic.ConnectionService;


public interface ConnectionServiceInterface {

    void addDelegate(ConnectionServiceOutput delegate);

    void removeDelegate(ConnectionServiceOutput delegate);

}
