package com.example.barbershop.Logic.ClientService;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.example.barbershop.Logic.ClientService.Objects.Callback.ClientCallback;
import com.example.barbershop.Logic.ClientService.Objects.Client;
import com.example.barbershop.Logic.ClientService.Objects.Callback.ClientsCallback;
import com.example.barbershop.Logic.ConnectionService.ConnectionService;
import com.example.barbershop.Logic.ConnectionService.ConnectionServiceInterface;
import com.example.barbershop.Logic.ConnectionService.ConnectionServiceOutput;
import com.example.barbershop.Logic.DataService.Objects.ResponseObject;
import com.example.barbershop.Logic.UserService.Objects.ResponseUserForToken;

import java.util.ArrayList;
import java.util.HashMap;

import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ClientService implements ClientServiceInterface, ConnectionServiceOutput {

    private final static String API_KEY = "AIzaSyBHoaAAFVpnobgsOW4d-w7Nzpu7HkHTOo0";
    private String idToken;

    private ArrayList<Client> clients;

    @SuppressLint("StaticFieldLeak")
    private static ClientService ourInstance;

    public static ClientService getInstance(Context getContext) {

        if (ourInstance == null) {
            ourInstance = new ClientService(getContext);
        }
        return ourInstance;
    }

    private ClientService(Context context) {
        ConnectionServiceInterface connectionService = new ConnectionService();
        connectionService.addDelegate(this);
    }

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://www.googleapis.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private Retrofit retrofitBD = new Retrofit.Builder()
            .baseUrl("https://barbershop-2614a.firebaseio.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    //ConnectionServiceOutput

    @Override
    public void isInternet(boolean isConnection) {
        if (isConnection) {
            Log.d("QQQQQQQ", "true");
        }
    }

    @Override
    public void registrationNewClient(String idToken, String id, String surname, String name,
                                      String phone, String password, String email, ClientCallback callback) {
        Client client = new Client();

        client.setId(id);
        client.setSurname(surname);
        client.setName(name);
        client.setPhone(phone);
        client.setEmail(email);
        client.setPassword(password);

        this.idToken = idToken;

        addClientToDataBase(client);

        callback.done(null, client);
    }

    @Override
    public void getClientById(final String id, String idToken, final ClientCallback callback) {
        this.idToken = idToken;

        getClientsForDataBase(new ClientsCallback() {
            @Override
            public void done(Error error, ArrayList<Client> clients) {
                if (error == null && clients != null) {
                    for (Client client : clients) {

                        if (client.getId().equals(id)) {
                            callback.done(null, client);

                        } else {
                            callback.done(new Error("client = null"), null);
                        }
                    }
                } else {
                    callback.done(new Error("client = null"), null);
                }
            }
        });
    }

    @Override
    public void getClientsByName(final String query, String idToken, final ClientsCallback callback) {
        this.idToken = idToken;

        getClientsForDataBase(new ClientsCallback() {
            @Override
            public void done(Error error, ArrayList<Client> clients) {

                if (error == null && clients != null) {
                    ArrayList<Client> clientsForChoice = new ArrayList<>();

                    for (Client client : clients) {

                        if (client.getSurname().equals(query) || client.getPhone().equals(query)) {
                            clientsForChoice.add(client);
                        }
                    }
                    callback.done(null, clientsForChoice);

                } else {
                    callback.done(new Error("client = null"), null);
                }
            }
        });
    }

    @Override
    public void updateClient(Client client, final ClientCallback callback) {
        Call<Client> callUpdate = createRequestForUpdateClientDB(client, client.getId());

        callUpdate.enqueue(new Callback<Client>() {
            @Override
            public void onResponse(Call<Client> call, Response<Client> response) {
                if (response.isSuccessful() && response.body() != null) {
                    callback.done(null, response.body());

                } else {
                    callback.done(new Error("Пользователь не обновился!"), null);
                }
            }

            @Override
            public void onFailure(Call<Client> call, Throwable t) {
                callback.done(new Error("Пользователь не обновился!"), null);
            }
        });
    }

    @Override
    public void deleteClient(String id, final ClientCallback callback) {

        Call<Client> call = createRequestForRemoveClientDB(id);

        assert call != null;
        call.enqueue(new Callback<Client>() {
            @Override
            public void onResponse(Call<Client> call, Response<Client> response) {
                if (response.isSuccessful()) {
                    callback.done(null, null);

                } else {
                    callback.done(new Error("Пользователь не удален!"), null);
                }
            }

            @Override
            public void onFailure(Call<Client> call, Throwable t) {
                Log.d("CCCCCC", t.getLocalizedMessage());
            }
        });
    }

    //Private

    private void addClientToDataBase(final Client client) {

        Call<Client> call = createRequestForPutClientDB(client);

        call.enqueue(new Callback<Client>() {
            @Override
            public void onResponse(Call<Client> call, Response<Client> response) {
                if (response.isSuccessful() && response.body() != null) {

                    if (clients == null) {
                        clients = new ArrayList<>();
                    }

                    clients.add(client);
                }
            }

            @Override
            public void onFailure(Call<Client> call, Throwable t) {
                Log.d("CCCCCC", t.getLocalizedMessage());
            }
        });
    }

    private void getClientsForDataBase(final ClientsCallback callback) {

        if (clients != null) {
            callback.done(null, clients);
            return;
        }

        getClientsFromFirebaseDB(new ClientsCallback() {
            @Override
            public void done(Error error, ArrayList<Client> clients) {
                if (error == null && clients != null) {
                    callback.done(null, clients);

                } else {
                    callback.done(new Error("clients - null"), null);
                }
            }
        });
    }

    private Call<Client> createRequestForRemoveClientDB(String id) {
        if (!id.isEmpty()) {
            ClientApi clientApi = retrofitBD.create(ClientApi.class);

            return clientApi.clientCallForRemoveDB(id, idToken);
        }

        return null;
    }

    private Call<Client> createRequestForPutClientDB(Client client) {

        ClientApi clientApi = retrofitBD.create(ClientApi.class);

        return clientApi.clientCallForPutDB(client, client.getId(), idToken);
    }

    private Call<HashMap<String, Client>> createRequestForGetClientDB() {
        ClientApi clientApi = retrofitBD.create(ClientApi.class);

        return clientApi.clientsCallForGetDB(idToken);
    }

    private Call<Client> createRequestForUpdateClientDB(Client client, String id) {
        ClientApi clientApi = retrofitBD.create(ClientApi.class);

        return clientApi.clientCallForUpdateDB(client, id, idToken);
    }

//    private Call<Client> createRequestForDeleteClientDB(Client client, String id) {
//        ClientApi clientApi = retrofitBD.create(ClientApi.class);
//
//        return clientApi.clientCallForDeleteDB(client, id, idToken);
//    }

    public void getClientsFromFirebaseDB(final ClientsCallback callback) {

        Call<HashMap<String, Client>> call = createRequestForGetClientDB();

        call.enqueue(new Callback<HashMap<String, Client>>() {
            @Override
            public void onResponse(Call<HashMap<String, Client>> call, Response<HashMap<String, Client>> response) {
                if (response.isSuccessful() && response.body() != null) {

                    clients = new ArrayList<>();

                    ResponseObject responseObject = new ResponseObject();
                    responseObject.clients = response.body();
                    clients.addAll(responseObject.getClients());

                    callback.done(null, clients);
                }
            }

            @Override
            public void onFailure(Call<HashMap<String, Client>> call, Throwable t) {
                Log.d("LLLLLLL", t.getLocalizedMessage());
            }
        });
    }
}
