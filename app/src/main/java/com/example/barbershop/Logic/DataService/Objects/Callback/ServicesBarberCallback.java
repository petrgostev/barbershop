package com.example.barbershop.Logic.DataService.Objects.Callback;

import com.example.barbershop.Logic.MasterService.Objects.ServiceBarber;

import java.util.ArrayList;


public interface ServicesBarberCallback {
    void done(Error error, ArrayList<ServiceBarber> servicesBarber);
}
