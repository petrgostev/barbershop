package com.example.barbershop.Logic.DataService.Objects;


public class TimeInterval {

    private double longTimeInterval;
    private double startTimeInterval;
    private double endTimeInterval;
    private double startTimeWrite;
    private double endTimeWrite;
    private String id;

    public String getId() {
        return id;
    }

    public double getLongTimeInterval() {
        return longTimeInterval;
    }

    public double getStartTimeInterval() {
        return startTimeInterval;
    }

    public double getEndTimeInterval() {
        return endTimeInterval;
    }

    public double getStartTimeWrite() {
        return startTimeWrite;
    }

    public double getEndTimeWrite() {
        return endTimeWrite;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLongTimeInterval(double longTimeInterval) {
        this.longTimeInterval = longTimeInterval;
    }

    public void setStartTimeInterval(double startTimeInterval) {
        this.startTimeInterval = startTimeInterval;
    }

    public void setEndTimeInterval(double endTimeInterval) {
        this.endTimeInterval = endTimeInterval;
    }

    public void setStartTimeWrite(double startTimeWrite) {
        this.startTimeWrite = startTimeWrite;
    }

    public void setEndTimeWrite(double endTimeWrite) {
        this.endTimeWrite = endTimeWrite;
    }
}
