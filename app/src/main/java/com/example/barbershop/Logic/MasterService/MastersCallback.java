package com.example.barbershop.Logic.MasterService;

import com.example.barbershop.Logic.MasterService.Objects.Master;

import io.realm.RealmList;


public interface MastersCallback {
    void done(Error error, RealmList<Master> masters, boolean isAdmin);
}
