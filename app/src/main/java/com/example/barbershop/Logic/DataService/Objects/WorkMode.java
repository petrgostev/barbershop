package com.example.barbershop.Logic.DataService.Objects;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import static java.lang.Math.ceil;


public class WorkMode {

    private Date date;
    private ArrayList<TimeInterval> timeIntervals;
    private double startWork;
    private double endWork;
    private double startTimeout;
    private double endTimeout;
    private String weekend;

    public String getWeekend() {
        return weekend != null ? weekend : "";
    }

    public double getStartWork() {
        return startWork;
    }

    public double getEndWork() {
        return endWork;
    }

    public double getStartTimeout() {
        return startTimeout;
    }

    public double getEndTimeout() {
        return endTimeout;
    }

    public double getLengthWorkingDay() {
        return endWork - startWork - (endTimeout - startTimeout);
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setWeekend(String weekend) {
        this.weekend = weekend;
    }

    public void setStartWork(double startWork) {
        this.startWork = startWork;
    }

    public void setEndWork(double endWork) {
        this.endWork = endWork;
    }

    public void setStartTimeout(double startTimeout) {
        this.startTimeout = startTimeout;
    }

    public void setEndTimeout(double endTimeout) {
        this.endTimeout = endTimeout;
    }

    public ArrayList<TimeInterval> getTimeIntervals(ArrayList<Write> existentWrite) {
        double countTimeIntervals = getLengthWorkingDay() * 2 + 2;

        createTimeIntervals(startWork, countTimeIntervals);

        Iterator<TimeInterval> timeIntervalIterator = timeIntervals.iterator();//создаем итератор
        while (timeIntervalIterator.hasNext()) {//до тех пор, пока в списке есть элементы

            TimeInterval timeInterval = timeIntervalIterator.next();//получаем следующий элемент
            if (timeInterval.getStartTimeInterval() >= startTimeout && timeInterval.getStartTimeInterval() < endTimeout) {
                timeIntervalIterator.remove();
            }

            for (Write write : existentWrite) {
                if (timeInterval.getStartTimeInterval() >= write.getTimeStart() &&
                        timeInterval.getStartTimeInterval() < write.getTimeEnd()) {
                    timeIntervalIterator.remove();
                }
            }
        }

        return timeIntervals;
    }

    public ArrayList<TimeInterval> getTimeIntervalsForService(double timeStart, double countTimeIntervals) {
        createTimeIntervals(timeStart, countTimeIntervals);

        return timeIntervals;
    }

    public TimeInterval getTimeOutInterval() {
        TimeInterval timeOutInterval = new TimeInterval();

        timeOutInterval.setStartTimeInterval(startTimeout);
        timeOutInterval.setEndTimeInterval(endTimeout);

        return timeOutInterval;
    }

    private void createTimeIntervals(double timeStart, double countTimeIntervals) {
        timeIntervals = new ArrayList<>();

         double timeEnd = timeStart + 0.30;

        if (timeEnd % 1 > 0.31) {
            timeEnd = ceil(timeEnd);//к целому числу
        }

        for (int i = 0; i < countTimeIntervals; i++) {
            TimeInterval timeInterval = new TimeInterval();

            double idDouble = Math.random() * 3;

            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
            String pattern = "#00.00000";
            DecimalFormat decimalFormat = new DecimalFormat(pattern, otherSymbols);

            String id = decimalFormat.format(idDouble);

            timeInterval.setId(id);
            timeInterval.setStartTimeInterval(timeStart);
            timeInterval.setEndTimeInterval(timeEnd);

            timeIntervals.add(timeInterval);

            timeStart = timeEnd;
            timeEnd = timeIntervals.get(i).getStartTimeInterval() + 1;
        }
    }

    private double getLengthWorkingDayUpTimeout() {
        return startTimeout - startWork;
    }

    private double getLengthWorkingDayOnTimeout() {
        return endWork - endTimeout;
    }

    public String getWorkMode() {
        return String.valueOf(startWork) + String.valueOf(startTimeout) + String.valueOf(endTimeout + endWork);
    }
}
