package com.example.barbershop.Logic.MasterService.Objects;

import com.google.gson.annotations.SerializedName;


public class ServiceBarber {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("price")
    private double price;

    @SerializedName("timeInterval")
    private double timeInterval;

    public ServiceBarber(String id, String name, double price, double timeInterval) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.timeInterval = timeInterval;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name != null ? name : "";
    }

    public double getPrice() {
        return price != 0 ? price : 0.0;
    }

    public double getTimeInterval() {
        return timeInterval != 0 ? timeInterval : 0.0;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setTimeInterval(double timeInterval) {
        this.timeInterval = timeInterval;
    }
}
