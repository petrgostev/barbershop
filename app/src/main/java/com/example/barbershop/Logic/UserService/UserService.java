package com.example.barbershop.Logic.UserService;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.example.barbershop.Logic.ClientService.Objects.Callback.ClientCallback;
import com.example.barbershop.Logic.ClientService.ClientService;
import com.example.barbershop.Logic.ClientService.ErrorExtractor;
import com.example.barbershop.Logic.ClientService.Objects.Client;
import com.example.barbershop.Logic.ClientService.Objects.Callback.ClientsCallback;
import com.example.barbershop.Logic.MasterService.MasterCallback;
import com.example.barbershop.Logic.MasterService.MasterRolesCallback;
import com.example.barbershop.Logic.MasterService.MasterService;
import com.example.barbershop.Logic.MasterService.Objects.Master;
import com.example.barbershop.Logic.UserService.Objects.ResponseUserForToken;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import io.realm.Realm;
import io.realm.RealmList;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.MODE_PRIVATE;


public class UserService implements UserServiceInterface {
    private final static String API_KEY = "AIzaSyBHoaAAFVpnobgsOW4d-w7Nzpu7HkHTOo0";
    private final static String EMAIL_KEY = "EMAIL_KEY";
    private final static String PASSWORD_KEY = "PASSWORD_KEY";

    private String idToken;

    @SuppressLint("StaticFieldLeak")
    private static UserService ourInstance;

    private Context context;

    private boolean isMaster = false;
    private boolean isAdmin = false;
    private SharedPreferences preferences;

    private MasterService masterService;
    private ClientService clientService;

    public static UserService getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new UserService(context);
        }

        return ourInstance;
    }

    private UserService(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences("UserService", MODE_PRIVATE);
        masterService = MasterService.getInstance(context);
        clientService = ClientService.getInstance(context);
    }

    final OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(10, TimeUnit.SECONDS)
            .connectTimeout(15, TimeUnit.SECONDS)
            .build();

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://www.googleapis.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build();

    @Override
    public void authenticationSavedUser(final UserCallback callback) {

        String savedEmail = getSavedEmail();
        String savedPassword = getSavedPassword();

        if (savedEmail == null || getSavedPassword() == null) {
            callback.done(new Error("Email and/or Password don't saved"), null, null, isAdmin, isMaster);
            return;
        }

        authenticationUser(savedEmail, savedPassword, callback);
    }

    @Override
    public void authenticationUserForAdmin(final String email, final String password, final UserCallback callback) {
        if (email == null || password == null) {
            callback.done(new Error("Email and/or Password can't be null"), null, null, isAdmin, isMaster);
            return;
        }

        Call<ResponseUserForToken> call = createRequestForAuthenticationUser(email, password);

        if (call == null) {
            callback.done(new Error("Call can't be null. Retrofit API for ResponseUserForToken is not defined"),
                    null, null, isAdmin, isMaster);
            return;
        }

        call.enqueue(new Callback<ResponseUserForToken>() {
            @Override
            public void onResponse(Call<ResponseUserForToken> call, final Response<ResponseUserForToken> response) {

                if (response.isSuccessful() && response.body() != null) {
                    String idToken = response.body().getIdToken();
                    final String localId = response.body().getLocalId();

                    if (idToken == null || localId == null || idToken.isEmpty() || localId.isEmpty()) {
                        callback.done(new Error("Response fields are null or empty"), null, null, isAdmin, isMaster);
                        return;
                    }

                    callback.done(null, localId, idToken, isAdmin, isMaster);

                } else {
                    callback.done(ErrorExtractor.getError(response.code()), null, null, isAdmin, isMaster);
                }
            }

            @Override
            public void onFailure(Call<ResponseUserForToken> call, Throwable t) {
                Toast.makeText(context, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                preferences.edit().clear().apply();
                callback.done(new Error(t.getLocalizedMessage()), null, null, isAdmin, isMaster);
            }
        });
    }

    @Override
    public void authenticationUser(final @Nullable String email, final @Nullable String password, final UserCallback callback) {

        if (email == null || password == null) {
            callback.done(new Error("Email and/or Password can't be null"), null, null, isAdmin, isMaster);
            return;
        }

        Call<ResponseUserForToken> call = createRequestForAuthenticationUser(email, password);

        if (call == null) {
            callback.done(new Error("Call can't be null. Retrofit API for ResponseUserForToken is not defined"),
                    null, null, isAdmin, isMaster);
            return;
        }

        call.enqueue(new Callback<ResponseUserForToken>() {
            @Override
            public void onResponse(Call<ResponseUserForToken> call, final Response<ResponseUserForToken> response) {

                if (response.isSuccessful() && response.body() != null) {
                    idToken = response.body().getIdToken();
                    final String localId = response.body().getLocalId();

                    if (idToken == null || localId == null || idToken.isEmpty() || localId.isEmpty()) {
                        callback.done(new Error("Response fields are null or empty"), null, null, isAdmin, isMaster);
                        return;
                    }

                    saveCurrentUserToAppPreference(email, password);

                    getCurrentMasterFromServerDB(localId, new MasterCallback() {
                        @Override
                        public void done(Error error, Master master) {

                            if (error == null && master != null) {
                                isMaster = true;
                                callback.done(null, localId, idToken, isAdmin, isMaster);

                            } else {
                                getCurrentClientFromServerDB(email, password, new ClientCallback() {
                                    @Override
                                    public void done(Error error, Client client) {

                                        if (error == null && client != null) {
                                            isMaster = false;
                                            callback.done(null, localId, idToken, isAdmin, isMaster);

                                        } else {
                                            callback.done(new Error("User - null"), null, null, isAdmin, isMaster);
                                        }
                                    }
                                });
                            }
                        }
                    });

                } else {
                    callback.done(ErrorExtractor.getError(response.code()), null, null, isAdmin, isMaster);
                }
            }

            @Override
            public void onFailure(Call<ResponseUserForToken> call, Throwable t) {
                Toast.makeText(context, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                preferences.edit().clear().apply();
                callback.done(new Error(t.getLocalizedMessage()), null, null, isAdmin, isMaster);
            }
        });
    }

    @Override
    public void registrationNewUser(final boolean isAdmin,
                                    final String email,
                                    final String password,
                                    final UserCallback callback) {
        Call<ResponseUserForToken> call = createRequestForRegistrationUser(email, password);

        assert call != null;
        call.enqueue(new Callback<ResponseUserForToken>() {
            @Override
            public void onResponse(Call<ResponseUserForToken> call, Response<ResponseUserForToken> response) {
                if (response.isSuccessful() && response.body() != null) {
                    idToken = response.body().getIdToken();

                    if (!isAdmin) {
                        saveCurrentUserToAppPreference(email, password);
                    }

                    callback.done(null, response.body().getLocalId(), idToken, isAdmin, isMaster);

                } else {
                    callback.done(ErrorExtractor.getError(response.code()), null, null, isAdmin, isMaster);
                }
            }

            @Override
            public void onFailure(Call<ResponseUserForToken> call, Throwable t) {
                Toast.makeText(context, "Отсутствует соединение. Повторите попытку позднее!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void deleteUser(final String idToken, final UserCallback callback) {
        Call<ResponseUserForToken> call = createRequestForRemoveUser(idToken);

        assert call != null;
        call.enqueue(new Callback<ResponseUserForToken>() {
            @Override
            public void onResponse(Call<ResponseUserForToken> call, Response<ResponseUserForToken> response) {
                if (response.isSuccessful()) {
                    callback.done(null, null, idToken, isAdmin, isMaster);

                } else {
                    callback.done(new Error("Пользователь не удален!"), null, null, isAdmin, isMaster);
                }
            }

            @Override
            public void onFailure(Call<ResponseUserForToken> call, Throwable t) {
                Log.d("CCCCCC", t.getLocalizedMessage());
            }
        });
    }

    @Override
    public void logoutUser(UserCallback callback) {
        preferences.edit().clear().apply();
        callback.done(null, null, null, false, false);
    }

    //Private

    private Call<ResponseUserForToken> createRequestForAuthenticationUser(String email, String
            password) {
        if (!email.isEmpty()) {
            UserApi userApi = retrofit.create(UserApi.class);

            return userApi.userCallForAuthentication(email, password, API_KEY, true);
        }

        return null;
    }

    private Call<ResponseUserForToken> createRequestForRegistrationUser(String email, String
            password) {
        if (!email.isEmpty()) {
            UserApi masterApi = retrofit.create(UserApi.class);

            return masterApi.userCallForRegister(email, password, API_KEY, true);
        }

        return null;
    }

    private Call<ResponseUserForToken> createRequestForRemoveUser(String idToken) {
        if (!idToken.isEmpty()) {
            UserApi userApi = retrofit.create(UserApi.class);

            return userApi.userCallForRemove(idToken, API_KEY);
        }

        return null;
    }

    private void getCurrentMasterFromServerDB(String id, final MasterCallback callback) {

        masterService.getCurrentMasterFromServerDB(id, new MasterCallback() {
            @Override
            public void done(Error error, final Master master) {

                if (error == null && master != null) {

                    masterService.checkMasterRoles(master, new MasterRolesCallback() {
                        @Override
                        public void done(Error error, boolean isAdmin) {
                            if (error == null) {
                                UserService.this.isAdmin = isAdmin;

                                callback.done(null, master);
                            }
                        }
                    });

                } else {
                    callback.done(new Error("currentMaster=null"), null);
                }
            }
        });
    }

    private void getCurrentClientFromServerDB(final String email, final String password,
                                              final ClientCallback callback) {

        clientService.getClientsFromFirebaseDB(new ClientsCallback() {
            @Override
            public void done(Error error, ArrayList<Client> clients) {
                if (error == null) {

                    for (Client client : clients) {

                        if (client.getEmail().equals(email) && client.getPassword().equals(password)) {
                            callback.done(null, client);
                        }
                    }
                } else {
                    callback.done(new Error("currentClient = null"), null);
                }
            }
        });
    }

    private void saveCurrentUserToAppPreference(String email, String password) {
        preferences.edit().putString(EMAIL_KEY, email).apply();
        preferences.edit().putString(PASSWORD_KEY, password).apply();
    }

    private @Nullable
    String getSavedEmail() {
        String email = preferences.getString(EMAIL_KEY, "");

        if (email == null || email.isEmpty()) {
            return null;
        }

        return email;
    }

    private @Nullable
    String getSavedPassword() {
        String password = preferences.getString(PASSWORD_KEY, "");

        if (password == null || password.isEmpty()) {
            return null;
        }

        return password;
    }
}
