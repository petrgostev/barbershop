package com.example.barbershop.Logic.DataService;

import com.example.barbershop.Logic.MasterService.Objects.ServiceBarber;
import com.example.barbershop.Logic.DataService.Objects.Write;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface DataApi {

    //Writes

    @Headers("Content-Type: application/json")
    @PUT("writes/{id}.json")
    Call<Write> writeCallForPutDB(@Body Write write,
                                  @Path("id") String id,
                                  @Query("idToken") String idToken);

    @Headers("Content-Type: application/json")
    @GET("writes.json?orderBy=\"clientId\"&print=pretty")
    Call<HashMap<String, Write>> writesCallForGetClientWritesDB(@Query("idToken") String idToken,
                                                                @Query("equalTo") String equalTo);

    @Headers("Content-Type: application/json")
    @GET("writes.json?orderBy=\"masterId\"&print=pretty")
    Call<HashMap<String, Write>> writesCallForGetMasterWritesDB(@Query("idToken") String idToken,
                                                                @Query("equalTo") String equalTo);

    @Headers("Content-Type: application/json")
    @DELETE("writes/{id}.json")
    Call<Write> callForDeleteWriteFromDB(@Path("id") String id,
                                  @Query("idToken") String idToken);

    //ServiceBarber

    @Headers("Content-Type: application/json")
    @PUT("servicesBarber/{id}.json")
    Call<ServiceBarber> callForPutServiceBarberDB(@Body ServiceBarber serviceBarber,
                                  @Path("id") String id,
                                  @Query("idToken") String idToken);

    @Headers("Content-Type: application/json")
    @GET("servicesBarber.json")
    Call<HashMap<String, ServiceBarber>> callForGetServicesBarberDB(@Query("idToken") String idToken);

    @Headers("Content-Type: application/json")
    @DELETE("servicesBarber/{id}.json")
    Call<ServiceBarber> callForDeleteServiceBarberFromDB(@Path("id") String id,
                                         @Query("idToken") String idToken);
}
