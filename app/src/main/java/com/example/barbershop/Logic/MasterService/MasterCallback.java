package com.example.barbershop.Logic.MasterService;

import com.example.barbershop.Logic.MasterService.Objects.Master;


public interface MasterCallback {
    void done(Error error, Master master);
}
