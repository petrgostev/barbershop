package com.example.barbershop.Logic.UserService;

import com.example.barbershop.Logic.UserService.Objects.ResponseUserForToken;

import retrofit2.Call;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface UserApi {
    @Headers("Content-Type: application/json")
    @POST("identitytoolkit/v3/relyingparty/verifyPassword")
    Call<ResponseUserForToken> userCallForAuthentication(@Query("email") String email,
                                                         @Query("password") String password,
                                                         @Query("key") String key,
                                                         @Query("returnSecureToken") boolean isToken);

    @Headers("Content-Type: application/json")
    @POST("identitytoolkit/v3/relyingparty/signupNewUser")
    Call<ResponseUserForToken> userCallForRegister(@Query("email") String email,
                                                   @Query("password") String password,
                                                   @Query("key") String key,
                                                   @Query("returnSecureToken") boolean isToken);

    @Headers("Content-Type: application/json")
    @POST("identitytoolkit/v3/relyingparty/deleteAccount")
    Call<ResponseUserForToken> userCallForRemove(@Query("idToken") String idToken,
                                                   @Query("key") String key);
}
