package com.example.barbershop.Logic.DataService.Objects.Callback;

import com.example.barbershop.Logic.MasterService.Objects.ServiceBarber;


public interface ServiceBarberCallback {
    void done(Error error, ServiceBarber serviceBarber);
}
