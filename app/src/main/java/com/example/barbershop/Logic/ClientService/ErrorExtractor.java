package com.example.barbershop.Logic.ClientService;


public class ErrorExtractor {
    public static Error getError(int error) {
         Error errorString = new Error("");

        switch (error) {
            case 400:
                errorString = new Error("Неверный пароль или email!");
                break;
            case 404:
                errorString = new Error("Страница не найдена!");
                break;
            case 500:
                errorString = new Error("Ошибка сервера!");
                break;
        }

        return errorString;
    }
}
