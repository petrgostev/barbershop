package com.example.barbershop.Logic.MasterService.Objects;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class Master {

    @SerializedName("idToken")
    private String idToken;

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("surname")
    private String surname;

    @SerializedName("phone")
    private String phone;

    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String password;

    @SerializedName("photoSrc")
    private String photoSrc;

    @SerializedName("photoName")
    private String photoName;

//new fields

    @SerializedName("services")
    public ArrayList<ServiceBarber> services;

    private boolean isAdmin = false;

    public Master(String idToken, String id, String name, String surname, String phone,
                  String email, String password, String photoSrc, String photoName,
                  ArrayList<ServiceBarber> services, boolean isAdmin) {
        this.idToken = idToken;
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.photoSrc = photoSrc;
        this.photoName = photoName;
        this.services = services;
        this.isAdmin = isAdmin;
    }

    public String getIdToken() {
        return idToken;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public String getPhotoSrc() {
        return photoSrc == null ? "" : photoSrc;
    }

    public String getPhotoName() {
        return photoName == null ? "" : photoName;
    }

    public String getId() {
        return id == null ? "" : id;
    }

    public String getName() {
        return name == null ? "" : name;
    }

    public String getFullName() {
        return getName() + " " + getSurname();
    }

    public String getSurname() {
        return surname == null ? "" : surname;
    }

    public String getPhone() {
        return phone == null ? "" : phone;
    }

    public String getEmail() {
        return email == null ? "" : email;
    }

    public String getPassword() {
        return password == null ? "" : password;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public void setPhotoSrc(String photoSrc) {
        this.photoSrc = photoSrc;
    }

    public void setPhotoName(String photoName) {
        this.photoName = photoName;
    }

    public void removePhoto() {
        photoSrc = "";
        photoName = "";
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
