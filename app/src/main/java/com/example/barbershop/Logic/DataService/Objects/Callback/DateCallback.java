package com.example.barbershop.Logic.DataService.Objects.Callback;

import java.util.Date;


public interface DateCallback {
    void done(Date date);
}
