package com.example.barbershop.Logic.MasterService;


public interface MasterRolesCallback {
    void done(Error error, boolean isAdmin);
}
