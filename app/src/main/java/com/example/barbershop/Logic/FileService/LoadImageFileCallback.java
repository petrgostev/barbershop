package com.example.barbershop.Logic.FileService;

import java.io.File;


public interface LoadImageFileCallback {
    void imageFileDidLoad(File imageFile);
}
