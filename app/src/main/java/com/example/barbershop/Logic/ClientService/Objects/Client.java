package com.example.barbershop.Logic.ClientService.Objects;

import com.example.barbershop.Logic.MasterService.Objects.Master;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class Client {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("surname")
    private String surname;

    @SerializedName("patronymic")
    private String patronymic;

    @SerializedName("phone")
    private String phone;

    @SerializedName("login")
    private String login;

    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String password;

    @SerializedName("myMasters")
    private ArrayList<Master> myMasters;

    @SerializedName("writeIds")
    private ArrayList<String> writeIds;

    @SerializedName("photoSrc")
    private String photoSrc;

    @SerializedName("photoName")
    private String photoName;

    public String getPhotoSrc() {
        return photoSrc == null ? "" : photoSrc;
    }

    public String getPhotoName() {
        return photoName == null ? "" : photoName;
    }

    public String getId() {
        return id == null ? "" : id;
    }

    public String getName() {
        return name == null ? "" : name;
    }

    public String getFullName() {
        return getName() + " " + getSurname();
    }

    public String getSurname() {
        return surname == null ? "" : surname;
    }

    public String getPatronymic() {
        return patronymic == null ? "" : patronymic;
    }

    public String getPhone() {
        return phone == null ? "" : phone;
    }

    public String getLogin() {
        return login == null ? "" : login;
    }

    public String getEmail() {
        return email == null ? "" : email;
    }

    public String getPassword() {
        return password == null ? "" : password;
    }

    public ArrayList<Master> getMyMasters() {
        return myMasters;
    }

    public ArrayList<String> getWriteIds() {
        return writeIds;
    }

    public void setPhotoSrc(String photoSrc) {
        this.photoSrc = photoSrc;
    }

    public void removePhoto() {
        photoSrc = "";
        photoName = "";
    }

    public void setPhotoName(String photoName) {
        this.photoName = photoName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setMyMasters(ArrayList<Master> myMasters) {
        this.myMasters = myMasters;
    }

    public void addWriteId(String id) {

        if (getWriteIds() == null) writeIds = new ArrayList<>();

        writeIds.add(id);
    }

    public void deleteWriteId(String id) {
        writeIds.remove(id);
    }
}
