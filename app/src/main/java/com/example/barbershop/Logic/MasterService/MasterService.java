package com.example.barbershop.Logic.MasterService;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.example.barbershop.Logic.ClientService.ErrorExtractor;
import com.example.barbershop.Logic.MasterService.Objects.Master;
import com.example.barbershop.Logic.DataService.Objects.ResponseObject;

import java.util.ArrayList;
import java.util.HashMap;

import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MasterService implements MasterServiceInterface {
    private final static String API_KEY = "AIzaSyBHoaAAFVpnobgsOW4d-w7Nzpu7HkHTOo0";

    private String idToken;

    @SuppressLint("StaticFieldLeak")
    private static MasterService ourInstance;


    private RealmList<Master> masters;
    private Master currentMaster;
    private boolean isAdmin = false;

    public static MasterService getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new MasterService(context);
        }

        return ourInstance;
    }

    private MasterService(Context context) {
    }

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://www.googleapis.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private Retrofit retrofitBD = new Retrofit.Builder()
            .baseUrl("https://barbershop-2614a.firebaseio.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @Override
    public void registrationNewMaster(String idToken, String id, String surname, String name, String patronymic,
                                      String phone, String email, String password, final MasterCallback callback) {

        Master master = new Master(idToken, id, name, surname, phone, email, password,
                null,  null, null, false);

        addMasterToDataBase(master, new MasterCallback() {
            @Override
            public void done(Error error, Master master) {
                if (error == null) {
                    callback.done(null, master);

                } else {
                    callback.done(new Error("master save - error"), null);
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void deleteMaster(final String id, String idToken, final MasterCallback callback) {

        Call<Master> callRemove = createRequestForRemoveMaster(id, idToken); //Запрос на удаление

        assert callRemove != null;
        callRemove.enqueue(new Callback<Master>() {
            @Override
            public void onResponse(Call<Master> call, Response<Master> response) {

                if (response.isSuccessful()) {

                    for (Master master : masters) {

                        if (master.getId().equals(id)) {
                            masters.remove(master);

                            callback.done(null, null);
                            return;
                        }
                    }
                } else {
                    callback.done(ErrorExtractor.getError(response.code()), null);
                }
            }

            @Override
            public void onFailure(Call<Master> call, Throwable t) {

            }
        });
    }

    @Override
    public void getMasterById(final String id, String idToken, final MasterCallback callback) {
        this.idToken = idToken;

        getMastersForDataBase(new MastersCallback() {
            @Override
            public void done(Error error, RealmList<Master> masters, boolean isAdmin) {
                if (error == null && masters != null) {

                    for (final Master master : masters) {
                        if (master.getId().equals(id)) {

                            callback.done(null, master);
                        }
                    }
                } else {
                    callback.done(new Error("master=null"), null);
                }
            }
        });
    }

    @Override
    public void getMasters(String idToken, final MastersCallback callback) {
        this.idToken = idToken;

        getMastersForDataBase(new MastersCallback() {
            @Override
            public void done(Error error, RealmList<Master> masters, boolean isAdmin) {
                if (error == null && masters != null) {
                    callback.done(null, masters, isAdmin);

                } else {
                    callback.done(new Error("masters=null"), null, isAdmin);
                }
            }
        });
    }

    @Override
    public RealmList<Master> getLoadedMasters() {
        return masters;
    }

    @Override
    public void updateMaster(final Master master, final MasterCallback callback) {
        Call<Master> callUpdate = createRequestForUpdateMasterDB(master, master.getId());

        callUpdate.enqueue(new Callback<Master>() {
            @Override
            public void onResponse(@NonNull Call<Master> call, @NonNull Response<Master> response) {
                if (response.isSuccessful() && response.body() != null) {
                    callback.done(null, response.body());

                } else {
                    callback.done(new Error("Пользователь не обновился!"), null);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Master> call, @NonNull Throwable t) {
                callback.done(new Error("Пользователь не обновился!"), null);
            }
        });
    }

    //Private

    private Call<Master> createRequestForRemoveMaster(String id, String idToken) {
        if (!id.isEmpty()) {
            MasterApi masterApi = retrofitBD.create(MasterApi.class);

            return masterApi.masterCallForRemoveDB(id, idToken);
        }

        return null;
    }

    private Call<Master> createRequestForPutMasterDB(Master master) {

        MasterApi masterApi = retrofitBD.create(MasterApi.class);

        return masterApi.masterCallForPutDB(master, master.getId());
    }

    private Call<Master> createRequestForUpdateMasterDB(Master master, String id) {
        MasterApi masterApi = retrofitBD.create(MasterApi.class);

        return masterApi.masterCallForUpdateDB(master, id, idToken);
    }

    private Call<HashMap<String, Master>> createRequestForGetMasterDB() {
        MasterApi masterApi = retrofitBD.create(MasterApi.class);

        return masterApi.mastersCallForGetDB(idToken);
    }

    private Call<String> createRequestForGetSuperAdminsDB() {
        MasterApi masterApi = retrofitBD.create(MasterApi.class);

        return masterApi.superAdminsCallForGetDB(idToken);
    }

    private void addMasterToDataBase(final Master master, final MasterCallback callback) {

        Call<Master> call = createRequestForPutMasterDB(master);

        call.enqueue(new Callback<Master>() {
            @Override
            public void onResponse(@NonNull Call<Master> call, @NonNull Response<Master> response) {
                if (response.isSuccessful() && response.body() != null) {

                    if (masters == null) {
                        masters = new RealmList<>();
                    }

                    masters.add(response.body());
                    callback.done(null, response.body());

                } else {
                    callback.done(new Error("master save - error"), null);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Master> call, @NonNull Throwable t) {

            }
        });
    }


    public void getMastersForDataBase(final MastersCallback callback) {

        if (masters != null) {
            callback.done(null, masters, isAdmin);
            return;
        }

        getMastersFromFirebaseDB(new MastersCallback() {
            @Override
            public void done(Error error, RealmList<Master> masters, boolean isAdmin) {
                if (error == null && masters != null) {

                    callback.done(null, masters, isAdmin);

                } else {
                    callback.done(new Error("masters=null"), null, isAdmin);
                }
            }
        });
    }

    private void getMastersFromFirebaseDB(final MastersCallback callback) {

        Call<HashMap<String, Master>> call = createRequestForGetMasterDB();

        call.enqueue(new Callback<HashMap<String, Master>>() {
            @Override
            public void onResponse(@NonNull Call<HashMap<String, Master>> call, @NonNull Response<HashMap<String, Master>> response) {
                if (response.isSuccessful() && response.body() != null) {

                    masters = new RealmList<>();

                    ResponseObject responseObject = new ResponseObject();
                    responseObject.masters = response.body();
                    masters.addAll(responseObject.getMasters());


                    callback.done(null, masters, isAdmin);
                } else {
                    callback.done(new Error("response.body = null"), null, isAdmin);
                }
            }

            @Override
            public void onFailure(@NonNull Call<HashMap<String, Master>> call, @NonNull Throwable t) {
                Log.d("Masters failure", t.getLocalizedMessage());
            }
        });
    }

    private void getSuperAdminsFromFirebaseDB(final SuperAdminsCallback callback) {

        Call<String> call = createRequestForGetSuperAdminsDB();

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                if (response.isSuccessful() && response.body() != null) {

                    ResponseObject responseObject = new ResponseObject();

                    responseObject.superUsers = response.body();

                    callback.done(null, responseObject.getSuperUsers());
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {

            }
        });
    }

    public void checkMasterRoles(final Master master, final MasterRolesCallback callback) {

        getSuperAdminsFromFirebaseDB(new SuperAdminsCallback() {
            @Override
            public void done(Error error, ArrayList<String> emailSuperAdmins) {
                if (error == null && emailSuperAdmins != null) {

                    for (String emailSuperAdmin : emailSuperAdmins) {

                        if (master.getEmail().equals(emailSuperAdmin)) {
                            isAdmin = true;

                            callback.done(null, isAdmin);
                            return;

                        } else {
                            callback.done(null, isAdmin);
                        }
                    }
                } else {
                    callback.done(new Error("email - null"), isAdmin);
                }
            }
        });
    }

    public void getCurrentMasterFromServerDB(final String id, final MasterCallback callback) {

        getMastersForDataBase(new MastersCallback() {
            @Override
            public void done(Error error, RealmList<Master> masters, boolean isAdmin) {
                if (error == null) {

                    for (Master master : masters) {

                        if (master.getId().equals(id)) {
                            MasterService.this.isAdmin = isAdmin;

                            callback.done(null, master);

                        } else {
                            callback.done(new Error("currentMaster = null"), null);
                        }
                    }
                } else {
                    callback.done(new Error("currentMaster = null"), null);
                }
            }
        });
    }
}
