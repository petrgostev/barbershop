package com.example.barbershop.Logic.DataService.Objects.Callback;

import com.example.barbershop.Logic.DataService.Objects.WorkMode;


public interface WorkModeCallback {
    void done(WorkMode workMode);
}
