package com.example.barbershop.Logic.ClientService.Objects.Callback;

import com.example.barbershop.Logic.ClientService.Objects.Client;


public interface ClientCallback {
    void done(Error error, Client client);
}
