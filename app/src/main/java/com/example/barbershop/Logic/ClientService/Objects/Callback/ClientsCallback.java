package com.example.barbershop.Logic.ClientService.Objects.Callback;

import com.example.barbershop.Logic.ClientService.Objects.Client;

import java.util.ArrayList;


public interface ClientsCallback {
    void done(Error error, ArrayList<Client> clients);
}
