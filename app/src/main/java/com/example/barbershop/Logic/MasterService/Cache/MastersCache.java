package com.example.barbershop.Logic.MasterService.Cache;

import com.example.barbershop.Logic.Database.Database;
import com.example.barbershop.Logic.MasterService.MasterService;
import com.example.barbershop.Logic.MasterService.Objects.Master;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

public class MastersCache {

    private static MastersCache instance;

    private Realm realm = Database.main();
    private RealmList<Master> masters;

    public static MastersCache getInstance() {
        if (instance == null) {
            instance = new MastersCache();
        }

        return instance;
    }

    public void addMaster(Master master) {
        realm.beginTransaction();
        RealmMaster realmMaster = realm.createObject(RealmMaster.class);
        realmMaster.configureWithMaster(master);
        realm.commitTransaction();

        masters.add(master);
    }

    public void removeMaster(Master master) {
        realm.beginTransaction();
        RealmMaster realmMaster = realm.where(RealmMaster.class).equalTo("id", master.getId()).findFirst();

        if (realmMaster != null) {
            realmMaster.deleteFromRealm();
        }

        realm.commitTransaction();

        masters.remove(master);
    }

    public void updateMaster(Master master) {
        realm.beginTransaction();
        RealmMaster realmMaster = realm.createObject(RealmMaster.class);
        realmMaster.configureWithMaster(master);
        realm.commitTransaction();

        masters.add(master);
    }

    private MastersCache() {
        loadMasters();
    }

    private void loadMasters() {
        RealmResults<RealmMaster> results =
                realm.where(RealmMaster.class).findAll();

        for (RealmMaster master : results) {
            masters.add(master.getMaster());
        }
    }
}
