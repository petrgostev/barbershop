package com.example.barbershop.Logic.DataService.Objects.Callback;


public interface SelectedObjectCallback {
    void didSelect(String objectId);
}
