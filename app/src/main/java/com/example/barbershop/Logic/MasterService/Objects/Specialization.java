package com.example.barbershop.Logic.MasterService.Objects;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;


public class Specialization {

    @SerializedName("id")
    private Integer id;

    @SerializedName("name")
    private String name;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //    CHILDRENS_HAIRDRESSER, MENES_HAIRDRESSER, FEMALE_HAIRDRESSER
}
