package com.example.barbershop.Logic.MasterService;

import com.example.barbershop.Logic.UserService.Objects.ResponseUserForToken;


public interface ResponseMasterCallback {
    void done(Error error, ResponseUserForToken responseUser);

}
