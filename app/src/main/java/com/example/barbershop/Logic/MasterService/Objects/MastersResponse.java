package com.example.barbershop.Logic.MasterService.Objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class MastersResponse {
    @SerializedName("masters")
    @Expose
    private ArrayList<Master> masters;

    public ArrayList<Master> getUsers() {
        return masters;
    }
}
