package com.example.barbershop.Logic.FileService;

import com.example.barbershop.Logic.FileService.Objects.Photo;

public interface FileServiceInterface {
    void savePhoto(String ownerId, byte[] jpegByte, boolean isMaster, PhotoUrlCallback callback);

    void deletePhoto(String ownerId, String imageName, FileCallback callback);
}
