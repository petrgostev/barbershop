package com.example.barbershop.Logic.MasterService;

import java.util.ArrayList;


public interface SuperAdminsCallback {
    void done(Error error, ArrayList<String> emailSuperAdmins);
}
