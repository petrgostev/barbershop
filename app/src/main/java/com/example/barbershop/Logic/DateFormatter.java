package com.example.barbershop.Logic;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatter {

    public static String stringFromDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy, EEEE", new Locale("ru"));
        return dateFormat.format(date);
    }
}
