package com.example.barbershop.Logic.FileService;

import com.example.barbershop.Logic.FileService.Objects.Photo;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface FileApi {

    //    @Headers("Content-Type: image/jpeg")
    @Multipart
    @POST("{ownerId}%2F{name}")
    Call<Photo> upload(@Path("ownerId") String ownerId,
                       @Path("name") String name,
                       @Part("requestFile") RequestBody requestFile);

    @Headers("Content-Type: folder/name")
    @DELETE("{name}")
    Call<ResponseBody> deletePackagePhoto(@Path("name") String ownerId);
}
