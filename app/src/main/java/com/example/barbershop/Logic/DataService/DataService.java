package com.example.barbershop.Logic.DataService;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.example.barbershop.Logic.DataService.Objects.Callback.ServiceBarberCallback;
import com.example.barbershop.Logic.DataService.Objects.Callback.ServicesBarberCallback;
import com.example.barbershop.Logic.DataService.Objects.Callback.WriteCallback;
import com.example.barbershop.Logic.DataService.Objects.Callback.WritesCallback;
import com.example.barbershop.Logic.MasterService.Objects.ServiceBarber;
import com.example.barbershop.Logic.DataService.Objects.TimeInterval;
import com.example.barbershop.Logic.DataService.Objects.Write;
import com.example.barbershop.Logic.DataService.Objects.ResponseObject;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class DataService implements DataServiceInterface {

    @SuppressLint("StaticFieldLeak")
    private static DataService ourInstance;

    private Context context;

    private String idToken;

    private ArrayList<Write> writes;
    private ArrayList<ServiceBarber> servicesBarber;
    private ArrayList<TimeInterval> timeIntervals;

    public static DataService getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new DataService(context);
        }

        return ourInstance;
    }

    private DataService(Context context) {
        this.context = context;
        this.timeIntervals = new ArrayList<>();
    }

    private Retrofit retrofitBD = new Retrofit.Builder()
            .baseUrl("https://barbershop-2614a.firebaseio.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @Override
    public void saveWrite(Write write, String idToken, final WriteCallback callback) {
        this.idToken = idToken;

        addWriteToDataBase(write, new WriteCallback() {
            @Override
            public void done(Error error, Write write) {
                if (error == null && write != null) callback.done(null, write);
                else callback.done(new Error("write-null"), null);
            }
        });
    }

    @Override
    public void getClientWrites(String ownerId, String idToken, final WritesCallback callback) {
        this.idToken = idToken;

        Call<HashMap<String, Write>> call = createRequestForGetClientWritesDB(ownerId);

        call.enqueue(new Callback<HashMap<String, Write>>() {
            @Override
            public void onResponse(Call<HashMap<String, Write>> call, Response<HashMap<String, Write>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    writes = new ArrayList<>();

                    ResponseObject responseObject = new ResponseObject();
                    responseObject.writes = response.body();
                    writes.addAll(responseObject.getWrites());

                    callback.done(null, writes);

                } else {
                    callback.done(new Error("writes - null"), null);
                }
            }

            @Override
            public void onFailure(Call<HashMap<String, Write>> call, Throwable t) {
                Log.d("Barber", t.getMessage());
            }
        });
    }

    @Override
    public void getMasterWrites(String ownerId, String idToken, final WritesCallback callback) {
        this.idToken = idToken;

        Call<HashMap<String, Write>> call = createRequestForGetMasterWritesDB(ownerId);

        call.enqueue(new Callback<HashMap<String, Write>>() {
            @Override
            public void onResponse(Call<HashMap<String, Write>> call, Response<HashMap<String, Write>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    writes = new ArrayList<>();

                    ResponseObject responseObject = new ResponseObject();
                    responseObject.writes = response.body();
                    writes.addAll(responseObject.getWrites());

                    callback.done(null, writes);

                } else {
                    callback.done(new Error("writes - null"), null);
                }
            }

            @Override
            public void onFailure(Call<HashMap<String, Write>> call, Throwable t) {
                Log.d("Barber", t.getMessage());
            }
        });
    }

    @Override
    public void getWriteById(String id, WriteCallback callback) {

        if (writes != null) {

            for (Write write : writes) {
                if (write.getId().equals(id)) {
                    callback.done(null, write);
                    return;
                }
            }
        } else {
//            getW
        }
    }

    @Override
    public void deleteWrite(final String id, final WriteCallback callback) {


        deleteWriteFromDataBase(id, new WriteCallback() {
            @Override
            public void done(Error error, Write write) {
                if (error == null) {
                    callback.done(null, null);

//                    if (writes != null) {
//
//                        for (Write writeLocal : writes) {
//
//                            if (writeLocal.getId().equals(id)) {
//                                writes.remove(write);
//                            }
//                        }
//                    }

                } else {
                    callback.done(new Error("write delete - error"), null);
                }
            }
        });
    }

    @Override
    public void saveServiceBarber(ServiceBarber serviceBarber, final ServiceBarberCallback callback) {
//        this.idToken = idToken;

        addServiceBarberToDataBase(serviceBarber, new ServiceBarberCallback() {
            @Override
            public void done(Error error, ServiceBarber serviceBarber) {
                if (error == null && serviceBarber != null) {

                    callback.done(null, serviceBarber);

                } else {
                    callback.done(new Error("serviceBarber-null"), null);
                }
            }
        });
    }

    @Override
    public void updateServiceBarber(ServiceBarber serviceBarber, final ServiceBarberCallback callback) {

        updateServiceBarberFromDataBase(serviceBarber, new ServiceBarberCallback() {
            @Override
            public void done(Error error, ServiceBarber serviceBarber) {

                if (error == null && serviceBarber != null) {
                    callback.done(null, serviceBarber);

                } else {
                    callback.done(new Error("UpdateServiceBarberDB - error"), null);
                }
            }
        });
    }

    @Override
    public void deleteServiceBarber(String id, final ServiceBarberCallback callback) {

        deleteServiceBarberFromDataBase(id, new ServicesBarberCallback() {
            @Override
            public void done(Error error, ArrayList<ServiceBarber> servicesBarber) {
                if (error == null) {
                    callback.done(null, null);

                } else {
                    callback.done(new Error("DeleteServiceBarber - error"), null);
                }
            }
        });
    }

    @Override
    public void getServicesBarber(String idToken, final ServicesBarberCallback callback) {
        this.idToken = idToken;

        getServicesBarberFromDataBase(new ServicesBarberCallback() {
            @Override
            public void done(Error error, ArrayList<ServiceBarber> servicesBarber) {
                if (error == null && servicesBarber != null) {
                    callback.done(null, servicesBarber);

                } else {
                    callback.done(new Error("servicesBarber from DB - error"), null);
                }
            }
        });
    }

    @Override
    public void getServicesBarberByIds(final ArrayList<String> serviceBarberIds, final ServicesBarberCallback callback) {

        getServicesBarberFromDataBase(new ServicesBarberCallback() {
            @Override
            public void done(Error error, ArrayList<ServiceBarber> servicesBarber) {
                if (error == null && servicesBarber != null) {
                    ArrayList<ServiceBarber> servicesBarberByIds = new ArrayList<>();

                    for (ServiceBarber serviceBarber : servicesBarber) {

                        for (String serviceBarberId : serviceBarberIds) {

                            if (serviceBarber.getId().equals(serviceBarberId)) {
                                servicesBarberByIds.add(serviceBarber);
                            }
                        }
                    }
                    callback.done(null, servicesBarberByIds);

                } else {
                    callback.done(new Error("servicesBarberByIds - null"), null);
                }
            }
        });

    }

    @Override
    public void getServiceBarberById(final String id, String idToken, final ServiceBarberCallback callback) {
        this.idToken = idToken;

        getServicesBarberFromDataBase(new ServicesBarberCallback() {
            @Override
            public void done(Error error, ArrayList<ServiceBarber> servicesBarber) {
                if (error == null && servicesBarber != null) {

                    for (ServiceBarber serviceBarber : servicesBarber) {
                        if (serviceBarber.getId().equals(id)) {

                            callback.done(null, serviceBarber);
                            return;
                        }
                    }

                } else {
                    callback.done(new Error("servicesBarber from DB - error"), null);
                }
            }
        });
    }

    //TimeInterval

    @Override
    public void saveTimeIntervals(ArrayList<TimeInterval> intervals) {
        timeIntervals.addAll(intervals);
    }

    @Override
    public TimeInterval getTimeIntervalById(String timeIntervalId) {

        for (TimeInterval timeInterval : timeIntervals) {

            if (timeInterval.getId().equals(timeIntervalId)) {
                return timeInterval;
            }
        }

        return null;
    }

    //Private

    private void addServiceBarberToDataBase(ServiceBarber serviceBarber, final ServiceBarberCallback callback) {

        Call<ServiceBarber> call = createRequestForPutServiceBarberDB(serviceBarber);

        call.enqueue(new Callback<ServiceBarber>() {
            @Override
            public void onResponse(Call<ServiceBarber> call, Response<ServiceBarber> response) {
                if (response.isSuccessful() && response.body() != null) {

                    if (servicesBarber == null) {
                        servicesBarber = new ArrayList<>();
                    }

                    servicesBarber.add(response.body());
                    callback.done(null, response.body());

                } else {
                    callback.done(new Error("ServiceBarber for BD - error"), null);
                }
            }

            @Override
            public void onFailure(Call<ServiceBarber> call, Throwable t) {
                Log.d("Barber", t.getMessage());
            }
        });
    }

    private void updateServiceBarberFromDataBase(ServiceBarber serviceBarber, final ServiceBarberCallback callback) {

        for (ServiceBarber service : servicesBarber) {

            if (service.getId().equals(serviceBarber.getId())) {
                servicesBarber.remove(service);
                servicesBarber.add(serviceBarber);
            }

        }

        Call<ServiceBarber> call = createRequestForUpdateServiceBarberDB(serviceBarber);

        call.enqueue(new Callback<ServiceBarber>() {
            @Override
            public void onResponse(Call<ServiceBarber> call, Response<ServiceBarber> response) {
                if (response.isSuccessful() && response.body() != null) {

                    callback.done(null, response.body());

                    for (ServiceBarber service : servicesBarber) {

                        if (service.getId().equals(response.body().getId())) {
                            servicesBarber.remove(service);
                            servicesBarber.add(response.body());

                            return;
                        }
                    }
                } else {
                    callback.done(new Error("UpdateServiceBarberDB - error"), null);
                }
            }

            @Override
            public void onFailure(Call<ServiceBarber> call, Throwable t) {
                Log.d("Barber", t.getMessage());
            }
        });
    }

    private void deleteServiceBarberFromDataBase(final String id, final ServicesBarberCallback callback) {

        Call<ServiceBarber> call = createRequestForDeleteServiceBarberDB(id);

        call.enqueue(new Callback<ServiceBarber>() {
            @Override
            public void onResponse(Call<ServiceBarber> call, Response<ServiceBarber> response) {
                if (response.isSuccessful()) {

                    for (ServiceBarber service : servicesBarber) {

                        if (service.getId().equals(id)) {
                            servicesBarber.remove(service);
                            callback.done(null, null);
                            return;
                        }
                    }
                } else {
                    callback.done(new Error("DeleteServiceBarber - error"), null);
                }
            }

            @Override
            public void onFailure(Call<ServiceBarber> call, Throwable t) {
                Log.d("Barber", t.getMessage());
            }
        });
    }

    private void getServicesBarberFromDataBase(final ServicesBarberCallback callback) {

        if (servicesBarber != null) {
            callback.done(null, servicesBarber);
            return;
        }

        Call<HashMap<String, ServiceBarber>> call = createRequestForGetServicesBarberDB();

        call.enqueue(new Callback<HashMap<String, ServiceBarber>>() {
            @Override
            public void onResponse(Call<HashMap<String, ServiceBarber>> call, Response<HashMap<String, ServiceBarber>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    servicesBarber = new ArrayList<>();

                    ResponseObject responseObject = new ResponseObject();
                    responseObject.servicesBarber = response.body();
                    servicesBarber.addAll(responseObject.getServicesBarber());

                    callback.done(null, servicesBarber);

                } else {
                    callback.done(new Error("servicesBarber from DB - error"), null);
                }
            }

            @Override
            public void onFailure(Call<HashMap<String, ServiceBarber>> call, Throwable t) {
                Log.d("Barber", t.getMessage());
            }
        });
    }

    private void addWriteToDataBase(final Write write, final WriteCallback callback) {
        Call<Write> call = createRequestForPutWriteDB(write);

        call.enqueue(new Callback<Write>() {
            @Override
            public void onResponse(Call<Write> call, Response<Write> response) {
                if (response.isSuccessful() && response.body() != null) {

                    callback.done(null, response.body());

                } else callback.done(new Error("write to bd - error"), null);
            }

            @Override
            public void onFailure(Call<Write> call, Throwable t) {
                Log.d("Barber", t.getMessage());
            }
        });
    }

    private void deleteWriteFromDataBase(String id, final WriteCallback callback) {
        Call<Write> call = createRequestForDeleteWriteFromDB(id);

        call.enqueue(new Callback<Write>() {
            @Override
            public void onResponse(Call<Write> call, Response<Write> response) {
                if (response.isSuccessful()) {
                    callback.done(null, null);

                } else {
                    callback.done(new Error("write delete - error"), null);
                }
            }

            @Override
            public void onFailure(Call<Write> call, Throwable t) {
                Log.d("Barber", t.getMessage());
            }
        });
    }

    private Call<Write> createRequestForPutWriteDB(Write write) {
        DataApi dataApi = retrofitBD.create(DataApi.class);

        return dataApi.writeCallForPutDB(write, write.getId(), idToken);
    }

    private Call<ServiceBarber> createRequestForPutServiceBarberDB(ServiceBarber serviceBarber) {
        DataApi dataApi = retrofitBD.create(DataApi.class);

        return dataApi.callForPutServiceBarberDB(serviceBarber, serviceBarber.getId(), idToken);
    }

    private Call<ServiceBarber> createRequestForUpdateServiceBarberDB(ServiceBarber serviceBarber) {
        DataApi dataApi = retrofitBD.create(DataApi.class);

        return dataApi.callForPutServiceBarberDB(serviceBarber, serviceBarber.getId(), idToken);
    }

    private Call<ServiceBarber> createRequestForDeleteServiceBarberDB(String id) {
        DataApi dataApi = retrofitBD.create(DataApi.class);

        return dataApi.callForDeleteServiceBarberFromDB(id, idToken);
    }

    private Call<HashMap<String, ServiceBarber>> createRequestForGetServicesBarberDB() {
        DataApi dataApi = retrofitBD.create(DataApi.class);

        return dataApi.callForGetServicesBarberDB(idToken);
    }

    private Call<HashMap<String, Write>> createRequestForGetClientWritesDB(String ownerId) {
        DataApi dataApi = retrofitBD.create(DataApi.class);

        String equalTo = "\"" + ownerId + "\"";
        return dataApi.writesCallForGetClientWritesDB(idToken, equalTo);
    }

    private Call<HashMap<String, Write>> createRequestForGetMasterWritesDB(String ownerId) {
        DataApi dataApi = retrofitBD.create(DataApi.class);

        String equalTo = "\"" + ownerId + "\"";
        return dataApi.writesCallForGetMasterWritesDB(idToken, equalTo);
    }

    private Call<Write> createRequestForDeleteWriteFromDB(String id) {
        DataApi dataApi = retrofitBD.create(DataApi.class);

        return dataApi.callForDeleteWriteFromDB(id, idToken);
    }
}
