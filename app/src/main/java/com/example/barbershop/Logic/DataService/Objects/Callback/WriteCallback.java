package com.example.barbershop.Logic.DataService.Objects.Callback;

import com.example.barbershop.Logic.DataService.Objects.Write;


public interface WriteCallback {
    void done(Error error, Write write);
}
