package com.example.barbershop.Logic.FileService;

import com.example.barbershop.Logic.FileService.Objects.Photo;


public interface FileCallback {
    void done(Error error, Photo photo);
}
