package com.example.barbershop.Logic.DataService.Objects;

import com.example.barbershop.Logic.MasterService.Objects.Master;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;


public class Write {

    @SerializedName("id")
    private String id;

    @SerializedName("masterId")
    private String masterId;

    @SerializedName("clientId")
    private String clientId;

    @SerializedName("date")
    private Date date;

    @SerializedName("serviceBarberId")
    private String serviceBarberId;

    @SerializedName("lengthTimeInterval")
    private double lengthTimeInterval;

    @SerializedName("timeStart")
    private double timeStart;

    @SerializedName("timeEnd")
    private double timeEnd;

    @SerializedName("prise")
    private double prise;

    @SerializedName("masterName")
    private String masterName;

    @SerializedName("clientName")
    private String clientName;

    public String getId() {
        return id != null ? id : "";
    }

    public String getMasterId() {
        return masterId != null ? masterId : "";
    }

    public String getClientId() {
        return clientId != null ? clientId : "";
    }

    public String getServiceBarberId() {
        return serviceBarberId != null ? serviceBarberId : "";
    }

    public Date getDate() {
        return date;
    }

    public double getLengthTimeInterval() {
        return lengthTimeInterval != 0 ? lengthTimeInterval : 0.0;
    }

    public double getTimeStart() {
        return timeStart != 0 ? timeStart : 0.0;
    }

    public double getTimeEnd() {
        return timeEnd != 0 ? timeEnd : 0.0;
    }

    public double getPrise() {
        return prise != 0 ? prise : 0.0;
    }

    public String getMasterName() {
        return masterName != null ? masterName : "";
    }

    public String getClientName() {
        return clientName != null ? clientName : "";
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setServiceBarberId(String serviceBarberId) {
        this.serviceBarberId = serviceBarberId;
    }

    public void removeServiceBarber() {
        serviceBarberId = "";
        lengthTimeInterval = 0;
        timeStart = 0;
        timeEnd = 0;

    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setLengthTimeInterval(double lengthTimeInterval) {
        this.lengthTimeInterval = lengthTimeInterval;
    }

    public void setTimeStart(double timeStart) {
        this.timeStart = timeStart;
    }

    public void setTimeEnd(double timeEnd) {
        this.timeEnd = timeEnd;
    }

    public void setPrise(double prise) {
        this.prise = prise;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }
}
