package com.example.barbershop.Logic.ConnectionService;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;


public class ConnectionService extends BroadcastReceiver implements ConnectionServiceInterface {

    private List<ConnectionServiceOutput> delegates = new ArrayList<>();

    @Override
    public void addDelegate(ConnectionServiceOutput delegate) {
        delegates.add(delegate);
    }

    @Override
    public void removeDelegate(ConnectionServiceOutput delegate) {
        delegates.add(delegate);
    }

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {
        final ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        final android.net.NetworkInfo wifi = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        final android.net.NetworkInfo mobile = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (wifi.isAvailable() || mobile.isAvailable()) {
            Log.d("Network Available ", "Flag No 1");
        }
    }
}
