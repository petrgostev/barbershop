package com.example.barbershop.Logic.Database;


import io.realm.Realm;
import io.realm.RealmConfiguration;

public class Database {

    public static Realm main() {
        return Realm.getInstance(mainConfiguration());
    }

    private static RealmConfiguration mainConfiguration() {
        return new RealmConfiguration.Builder()
                .name("mainConfiguration.realm")
                .build();
    }
}
