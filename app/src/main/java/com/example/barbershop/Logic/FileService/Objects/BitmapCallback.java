package com.example.barbershop.Logic.FileService.Objects;

import android.graphics.Bitmap;


public interface BitmapCallback {
    void done(Error error, Bitmap bitmap);
}
