package com.example.barbershop;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.bottomappbar.BottomAppBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.barbershop.Logic.FileService.Objects.BitmapCallback;
import com.example.barbershop.modules.Loader.Assembly.LoaderAssembly;
import com.example.barbershop.modules.Photo.Assembly.PhotoAssembly;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;


public class MainActivity extends AppCompatActivity {
    public static final int REQUEST_IMAGE_CAPTURE = 1041;

    private Toolbar toolbar;
    private Bitmap bitmap;

    public Uri lastImageUri;

    public String ownerId;
    public String idToken;
    public boolean isMaster;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomAppBar bottomAppBar = findViewById(R.id.bottom_app_bar);
        setSupportActionBar(bottomAppBar);
        bottomAppBar.setVisibility(View.GONE);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        FrameLayout frame = findViewById(R.id.frame);
        TextView titleView = findViewById(R.id.titleView);

        presentLoader();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == REQUEST_IMAGE_CAPTURE) {

            bitmap = null;

            compressBitmap(new BitmapCallback() {
                @Override
                public void done(Error error, Bitmap bitmap) {
                    if (error == null && bitmap != null) {
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                        Fragment fragment = PhotoAssembly.photoModuleForCreate(ownerId, idToken, bitmap, isMaster);

                        assert fragment != null;
                        fragmentTransaction.replace(R.id.frame, fragment, null);
                        fragmentTransaction.addToBackStack(fragment.getTag());
                        fragmentTransaction.commit();
                    }
                }
            });
        }
    }

    public void presentLoader() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frame, LoaderAssembly.loaderModule(), null);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    private void compressBitmap(final BitmapCallback callback) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    InputStream is = getContentResolver().openInputStream(lastImageUri);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeStream(is, null, options);
                    assert is != null;
                    is.close();

                Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
                DisplayMetrics metrics = new DisplayMetrics();
                display.getMetrics(metrics);

                    int origWidth = options.outWidth; //исходная ширина
                    int origHeight = options.outHeight; //исходная высота
                    int bytesPerPixel = 2; //соответствует RGB_555 конфигурации
                    int maxSize = 480 * 800 * bytesPerPixel; //Максимально разрешенный размер Bitmap
                    int desiredWidth = metrics.widthPixels; //Нужная ширина
                    int desiredHeight = metrics.heightPixels; //Нужная высота
                    int desiredSize = desiredWidth * desiredHeight * bytesPerPixel; //Максимально разрешенный размер Bitmap для заданных width х height
                    if (desiredSize < maxSize) maxSize = desiredSize;
                    int scale = 1; //кратность уменьшения
                    int origSize = origWidth * origHeight * bytesPerPixel;
                    //высчитываем кратность уменьшения
                    if (origWidth > origHeight) {
                        scale = Math.round((float) origHeight / (float) desiredHeight);
                    } else {
                        scale = Math.round((float) origWidth / (float) desiredWidth);

                        options = new BitmapFactory.Options();
                        options.inSampleSize = scale;
                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                        options.inJustDecodeBounds = false;

                        is = getContentResolver().openInputStream(lastImageUri);
                        bitmap = BitmapFactory.decodeStream(is, null, options);

                        callback.done(null, bitmap);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
}
